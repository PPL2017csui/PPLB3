let monthList = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
module.exports = {
    toUserFormat(date) {
        let chunks = date.split('-');
        return chunks['2'] + ' ' + monthList[parseInt(chunks['1'])] + ' ' + chunks['0'];
    },
    retrieveDate(dateISO) {
        let date = dateISO.toISOString().split('T')[0];
        return this.toMysqlFormat(date);
    },
    dateToCalendar(dateISO) {
        let date = dateISO ? dateISO.toISOString().split('T')[0] : null;
        return date ? this.toCalendarFormat(date) : null;
    },
    toCalendarFormat(date) {
        let chunks = date.split('-');
        return chunks['1'] + '/' + chunks['2'] + '/' + chunks['0'];
    },
    dateToUser(dateISO) {
        let date = dateISO ? dateISO.toISOString().split('T')[0] : null;
        return date ? this.toUserFormat(date) : '-';
    },
    toMysqlFormat(date) {
        let chunks = date.split('-');
        return chunks['2'] + '-' + chunks['1'] + '-' + chunks['0'];
    },
    getBirthdayDate(date) {
        cur_year = (new Date()).getFullYear();
        tgl = ("0" + date.getDate()).slice(-2);
        month = ("0" + (date.getMonth() + 1)).slice(-2);
        return cur_year + '-' + month + '-' + tgl;
    },
    retrieveDateReverse(date) {
        tgl = ("0" + date.getDate()).slice(-2);
        month = ("0" + (date.getMonth() + 1)).slice(-2);
        return date.getFullYear() + '-' + month + '-' + tgl;
    },
    getWeekdaysInMonth(month, year) {
        days = (new Date(year, month + 1, 0)).getDate();
        weekdays = 0;
        for(i = 0; i < days; i++) {
            if (this.isWeekday(year, month, i + 1)) weekdays++;
        }
        return weekdays;
    },
    isWeekday(year, month, day) {
        var day = new Date(year, month, day).getDay();
        return day !=0 && day !=6;
    }

};