var bcrypt = require('bcrypt')

const saltRounds = 10;

module.exports = {
    hash: function(password) {
        return bcrypt.hashSync(password, saltRounds);
    },
    validate: function(password, hash) {
        return bcrypt.compareSync(password, hash);
    }
};