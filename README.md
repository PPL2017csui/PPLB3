# Autopersonalia
[![coverage report](https://gitlab.com/PPL2017csui/PPLB3/badges/develop/coverage.svg)](https://gitlab.com/PPL2017csui/PPLB3/commits/develop) [![build status](https://gitlab.com/PPL2017csui/PPLB3/badges/develop/build.svg)](https://gitlab.com/PPL2017csui/PPLB3/commits/develop)

Autopersonalia is a HR Management system.

### Team Member

* Brigita Maria W
* Gentur Waskito Triwinasis
* Kustiawanto Halim
* Muthy Afifah
* Riscel Eliel F
 
### Project Link

You can access our project [here](http://auto-personalia-development.herokuapp.com "Autopersonalia").

### Tech

This project use geek power such as:

* [Twitter Bootstrap] - Front-end
* [Node.js] - Back-end
* [Express] - fast node.js network app framework [@tjholowaychuk]

### Installation

Dillinger requires [Node.js](https://nodejs.org/) v4+ to run.

Clone this repository.
```sh
$ git clone https://gitlab.com/PPL2017csui/PPLB3.git
```

Install the dependencies.

```sh
$ cd PPLB3
$ npm install
```

Configure environment variable.

```sh
$ vim .env
```

Add the following code to the file/

```sh
DATABASE_URL={YOUR_DATABASE_URL}
MYSQL_USERNAME={YOUR_DATABASE_USERNAME}
MYSQL_PASSWORD={YOUR_DATABASE_PASSWORD}
PORT={PORT_TO_RUN_APP}
```
Run the app.

```sh
$ npm start
```

### Project Error Code

The list of error code in this project.

| Error Code | Description |
| :---: |:---|
| Error #400 | Bad Request |
| Error #403 | Forbidden |
| Error #404 | Not Found |
| Error #500 | Internal Server Error |
| Error #502 | Bad Gateway |
| Error #601 | Atasan tidak ditemukan |
| Error #602 | Data atasan yang dimasukkan salah |
| Error #603 | Terjadi duplikasi data jabatan |
| Error #604 | Field sudah pernah dipakai |
| Error #605 | Data employee yang dimasukkan salah |
| Error #606 | Tidak ada data divisi |
| Error #607 | Data divisi yang dimasukkan salah |
| Error #608 | Divisi tidak ditemukan |
| Error #609 | Divisi sudah pernah dipakai |
| Error #610 | Sistem hanya dapat menerima file CSV |
| Error #611 | Terdapat duplikasi data |
| Error #612 | Tidak ada data cuti |
| Error #613 | Terjadi kesalahan dalam sistem |
| Error #614 | Tidak ada pengajuan dengan id |
| Error #615 | Jadwal pengajuan yang diajukan beririsan dengan pengajuan yang lain |
| Error #616 | Data cuti yang dimasukkan tidak benar |
| Error #617 | Tidak ada karyawan dengan id |
| Error #618 | Tidak bisa melihat data karyawan |
| Error #619 | Tidak bisa mengubah karyawan |
| Error #620 | Tidak bisa menghapus karyawan |
| Error #621  | Token salah |
| Error #622  | Pengubahan password gagal |

License
----

MIT


**Free Software, Hell Yeah!**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [express]: <http://expressjs.com>
