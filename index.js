require('dotenv').config();
(require('./models/relations')());
let attendance = require('./handlers/attendance');
let bodyParser = require('body-parser');
let dashboard = require('./handlers/dashboard');
let division = require('./handlers/division');
let employee = require('./handlers/employee');
let express = require('express');
let exphbs = require('express-handlebars');
let flash = require('express-flash');
let job = require('./handlers/job');
let model = require('./models');
let multer = require('multer');
let session = require('express-session');
let Sequelize = require("sequelize");
let Umzug = require('umzug');
let upload = multer({ dest: 'uploads/' });
var login = require('./handlers/login');
let dayoff_accept = require('./handlers/dayoff-accept');
var password = require('./handlers/password');
var register = require('./handlers/register');
let dayoff = require('./handlers/dayoff');

let app = express();

app.engine('handlebars', exphbs({
    defaultLayout: 'main'
}));
app.set('view engine', 'handlebars');

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(flash());
app.use(express.static('public'));
app.use(
    session({
        cookie: {
            maxAge: 3600000
        },
        secret: 'woot',
        resave: false,
        saveUninitialized: false
    })
);

app.use(function(req, res, next) {
    if (req.session && req.session.user) {
        model.Employee.findOne({ where: { email: req.session.user.email } })
            .then(function(user) {
                model.Job.findOne({ where: { id: user.id_job_title } })
                    .then(function(userjob) {
                        model.Division.findOne({ where: { id: userjob.id_division } })
                            .then(function(userdivisi) {
                                if (user) {
                                    req.user = user;
                                    delete req.user.password;
                                    req.session.user = user;
                                    res.locals.user = user;
                                    res.locals.company = userdivisi.id_company;
                                    res.locals.division = userdivisi.id;
                                }
                                next();
                            }).catch(function(err) {
                                console.log(err);
                                delete req.user;
                                delete req.session.user;
                                delete res.locals.user;
                                delete res.locals.company;
                                next();
                            })
                    }).catch(function(err) {
                        console.log(err);
                        delete req.user;
                        delete req.session.user;
                        delete res.locals.user;
                        delete res.locals.company;
                        next();
                    })
            }).catch(function(err) {
                console.log(err);
                delete req.user;
                delete req.session.user;
                delete res.locals.user;
                delete res.locals.company;
                next();
            })
    } else {
        next();
    }
});
app.get('/', function(req, res) {
    res.render('index');
});
app.get('/login', login.viewLogin);
app.post('/login', login.authenticate);
app.get('/logout', function(req, res) {
    req.session.destroy(function() {
        res.redirect('/');
    });
});

app.get('/dashboard', requireLogin, function(req, res) {
    if (req.user.is_admin) dashboard.renderAdminDashboard(req, res);
    else dashboard.renderNonAdminDashboard(req, res);
});
app.post('/dashboard', requireLogin, dayoff_accept.addAnnualDayoff);
app.get('/dayoff/view', requireLogin, dayoff_accept.viewListDayoff);
app.post('/dayoff/view', requireLogin, dayoff_accept.changeDayoff);
app.get('/dayoff/detail/:id', requireLogin, dayoff_accept.viewDetailDayoff);
app.post('/dayoff/detail/:id', requireLogin, dayoff_accept.changeDayoff);
app.get('/dayoff/add', requireLogin, dayoff_accept.viewAddAnnualDayoff);
app.post('/dayoff/add', requireLogin, dayoff_accept.addAnnualDayoff);
app.get('/employee', requireLogin, employee.viewListEmployee);
app.get('/employee/detail/:id', requireLogin, employee.viewDetailEmployee);
app.get('/employee/create', requireLogin, employee.viewCreateEmployee);
app.post('/employee/create', requireLogin, employee.saveCreateEmployee);
app.get('/employee/edit/:id', requireLogin, employee.viewUpdateEmployee);
app.post('/employee/edit/:id', requireLogin, employee.saveUpdateEmployee);
app.get('/employee/delete/:id', requireLogin, employee.deleteEmployee);
app.get('/attendance', requireLogin, attendance.viewAttendance);
app.post('/attendance', upload.single('csv'), requireLogin, attendance.saveAttendance);
app.get('/attendance/api/list-division', division.getDivision);
app.get('/job', requireLogin, job.viewListJob);
app.get('/job/create', requireLogin, job.viewCreateJob);
app.post('/job/create', requireLogin, job.saveCreateJob);
app.get('/job/api/list-job-title', job.getJobTitle);
app.get('/division', requireLogin, division.viewListDivision);
app.get('/division/create', requireLogin, division.viewCreateDivision);
app.post('/division/create', requireLogin, division.saveCreateDivision);
app.get('/set-password', password.viewSetPassword);
app.post('/set-password', password.savePassword);
app.get('/register', register.viewRegister);
app.post('/register', register.submitRegister);
app.get('/download/csv', attendance.downloadCSV);
app.get('/dayoff', requireLogin, dayoff.viewDayOffStatus);
app.get('/dayoff/create', requireLogin, dayoff.viewCreateDayOff);
app.post('/dayoff/create', requireLogin, dayoff.saveCreateDayOff);
app.get('/dayoff/api/list-dates', dayoff.getDates);

app.get('*', (req, res) => {
    res.render('404');
});

let umzug = new Umzug({
    storage: 'sequelize',
    storageOptions: {
        sequelize: model.sequelize,
        tableName: '_migration'
    },
    logging: false,
    upName: 'up',
    downName: 'down',
    migrations: {
        params: [model.sequelize.getQueryInterface(), Sequelize],
        path: 'migrations',
        pattern: /^.*\.js$/
    }
});

umzug.up().then(function(migrations, err) {
    if (err) {
        console.log(err);
    } else {
        console.log("migrations ok");
        app.listen(process.env.PORT, function() {
            console.log('listening on port ' + process.env.PORT);
        });
    }
});

function requireLogin(req, res, next) {
    // req.user = {is_admin: false, id: 6};
    // res.locals = {user: {id: 6}};

    // next();
    if (!req.user) {
        res.redirect('/login');
    } else {
        next();
    }
}