var async = require('async');

module.exports = {
    up: function(migration, Sequelize) {
        return async.series([
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',1,'2017-03-08 08:00:00','2017-03-08 18:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',2,'2017-03-08 08:00:00','2017-03-08 17:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',3,'2017-03-08 08:30:00','2017-03-08 17:03:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',4,'2017-03-08 09:00:00','2017-03-08 17:16:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',5,'2017-03-08 07:55:00','2017-03-08 17:10:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',6,'2017-03-08 08:07:00','2017-03-08 18:15:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',7,'2017-03-08 08:00:00','2017-03-08 17:30:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',8,'2017-03-08 08:00:00','2017-03-08 17:50:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',9,'2017-03-08 08:30:00','2017-03-08 17:38:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',10,'2017-03-08 09:00:00','2017-03-08 19:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',11,'2017-03-08 10:00:00','2017-03-08 18:10:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',12,'2017-03-08 08:00:00','2017-03-08 18:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',13,'2017-03-08 08:00:00','2017-03-08 17:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',14,'2017-03-08 08:30:00','2017-03-08 17:03:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',15,'2017-03-08 09:00:00','2017-03-08 17:16:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',16,'2017-03-08 07:55:00','2017-03-08 17:10:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',17,'2017-03-08 08:07:00','2017-03-08 18:15:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',18,'2017-03-08 03:36:00','2017-03-08 17:30:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',19,'2017-03-08 08:00:00','2017-03-08 17:50:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',20,'2017-03-08 08:30:00','2017-03-08 17:38:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',21,'2017-03-08 09:00:00','2017-03-08 19:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',22,'2017-03-08 08:00:00','2017-03-08 18:10:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-08',23,'2017-03-08 08:11:00','2017-03-08 18:15:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',1,'2017-03-09 09:00:00','2017-03-09 17:16:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',2,'2017-03-09 07:55:00','2017-03-09 17:10:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',3,'2017-03-09 08:07:00','2017-03-09 18:15:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',4,'2017-03-09 08:30:00','2017-03-09 17:03:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',5,'2017-03-09 09:00:00','2017-03-09 17:16:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',6,'2017-03-09 07:55:00','2017-03-09 17:10:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',7,'2017-03-09 08:07:00','2017-03-09 18:15:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',8,'2017-03-09 08:36:00','2017-03-09 17:30:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',9,'2017-03-09 08:00:00','2017-03-09 17:50:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',10,'2017-03-09 08:00:00','2017-03-09 18:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',11,'2017-03-09 08:00:00','2017-03-09 17:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',12,'2017-03-09 08:30:00','2017-03-09 17:03:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',13,'2017-03-09 08:30:00','2017-03-09 17:38:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',14,'2017-03-09 09:00:00','2017-03-09 19:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',15,'2017-03-09 08:00:00','2017-03-09 17:30:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',16,'2017-03-09 08:00:00','2017-03-09 17:50:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',17,'2017-03-09 08:30:00','2017-03-09 17:38:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',18,'2017-03-09 09:00:00','2017-03-09 19:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',19,'2017-03-09 10:00:00','2017-03-09 18:10:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',20,'2017-03-09 08:00:00','2017-03-09 18:10:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',21,'2017-03-09 08:11:00','2017-03-09 18:15:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',22,'2017-03-09 08:00:00','2017-03-09 18:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-09',23,'2017-03-09 07:58:00','2017-03-09 17:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',1,'2017-03-10 08:06:00','2017-03-10 16:51:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',2,'2017-03-10 10:00:00','2017-03-10 17:13:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',3,'2017-03-10 09:28:00','2017-03-10 19:04:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',4,'2017-03-10 08:26:00','2017-03-10 18:47:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',5,'2017-03-10 08:30:00','2017-03-10 18:48:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',6,'2017-03-10 07:55:00','2017-03-10 16:48:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',7,'2017-03-10 08:36:00','2017-03-10 17:20:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',8,'2017-03-10 08:28:00','2017-03-10 18:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',9,'2017-03-10 10:00:00','2017-03-10 17:40:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',10,'2017-03-10 09:30:00','2017-03-10 17:21:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',11,'2017-03-10 08:07:00','2017-03-10 16:26:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',12,'2017-03-10 08:50:00','2017-03-10 17:54:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',13,'2017-03-10 09:42:00','2017-03-10 19:13:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',14,'2017-03-10 09:26:00','2017-03-10 16:54:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',15,'2017-03-10 09:08:00','2017-03-10 19:12:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',16,'2017-03-10 08:00:00','2017-03-10 16:24:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',17,'2017-03-10 08:00:00','2017-03-10 18:37:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',18,'2017-03-10 08:30:00','2017-03-10 16:35:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',19,'2017-03-10 08:58:00','2017-03-10 17:34:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',20,'2017-03-10 08:52:00','2017-03-10 17:12:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',21,'2017-03-10 08:00:00','2017-03-10 19:19:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',22,'2017-03-10 08:02:00','2017-03-10 18:22:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-10',23,'2017-03-10 08:16:00','2017-03-10 18:41:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',1,'2017-03-11 08:56:00','2017-03-11 19:30:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',2,'2017-03-11 09:12:00','2017-03-11 17:18:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',3,'2017-03-11 08:54:00','2017-03-11 18:32:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',4,'2017-03-11 08:30:00','2017-03-11 16:55:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',5,'2017-03-11 09:38:00','2017-03-11 18:57:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',6,'2017-03-11 08:00:00','2017-03-11 16:28:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',7,'2017-03-11 08:07:00','2017-03-11 16:39:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',8,'2017-03-11 09:00:00','2017-03-11 17:32:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',9,'2017-03-11 07:55:00','2017-03-11 17:47:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',10,'2017-03-11 08:00:00','2017-03-11 18:15:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',11,'2017-03-11 09:10:00','2017-03-11 19:23:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',12,'2017-03-11 09:00:00','2017-03-11 19:29:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',13,'2017-03-11 09:00:00','2017-03-11 18:11:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',14,'2017-03-11 08:06:00','2017-03-11 16:27:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',15,'2017-03-11 08:03:00','2017-03-11 18:16:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',16,'2017-03-11 08:55:00','2017-03-11 17:20:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',17,'2017-03-11 08:07:00','2017-03-11 16:57:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',18,'2017-03-11 09:28:00','2017-03-11 16:33:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',19,'2017-03-11 07:32:00','2017-03-11 19:01:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',20,'2017-03-11 08:52:00','2017-03-11 17:34:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',21,'2017-03-11 07:35:00','2017-03-11 19:15:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',22,'2017-03-11 09:25:00','2017-03-11 18:57:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-11',23,'2017-03-11 08:39:00','2017-03-11 17:19:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',1,'2017-03-12 09:10:00','2017-03-12 18:42:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',2,'2017-03-12 08:49:00','2017-03-12 18:04:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',3,'2017-03-12 08:24:00','2017-03-12 16:31:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',4,'2017-03-12 08:10:00','2017-03-12 19:12:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',5,'2017-03-12 08:17:00','2017-03-12 17:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',6,'2017-03-12 07:52:00','2017-03-12 19:04:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',7,'2017-03-12 07:37:00','2017-03-12 18:13:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',8,'2017-03-12 08:08:00','2017-03-12 17:01:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',9,'2017-03-12 08:05:00','2017-03-12 17:49:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',10,'2017-03-12 09:05:00','2017-03-12 17:44:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',11,'2017-03-12 07:36:00','2017-03-12 16:40:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',12,'2017-03-12 08:38:00','2017-03-12 16:23:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',13,'2017-03-12 08:37:00','2017-03-12 18:12:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',14,'2017-03-12 09:07:00','2017-03-12 18:54:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',15,'2017-03-12 08:25:00','2017-03-12 18:50:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',16,'2017-03-12 09:03:00','2017-03-12 16:44:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',17,'2017-03-12 08:44:00','2017-03-12 17:26:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',18,'2017-03-12 08:27:00','2017-03-12 17:29:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',19,'2017-03-12 08:11:00','2017-03-12 17:16:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',20,'2017-03-12 08:51:00','2017-03-12 19:03:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',21,'2017-03-12 08:42:00','2017-03-12 19:05:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',22,'2017-03-12 07:40:00','2017-03-12 19:01:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-12',23,'2017-03-12 07:35:00','2017-03-12 17:33:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',1,'2017-03-13 08:36:00','2017-03-13 16:37:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',2,'2017-03-13 07:34:00','2017-03-13 19:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',3,'2017-03-13 09:12:00','2017-03-13 16:39:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',4,'2017-03-13 08:55:00','2017-03-13 17:09:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',5,'2017-03-13 08:54:00','2017-03-13 18:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',6,'2017-03-13 09:06:00','2017-03-13 18:35:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',7,'2017-03-13 09:20:00','2017-03-13 16:26:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',8,'2017-03-13 07:50:00','2017-03-13 17:31:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',9,'2017-03-13 08:53:00','2017-03-13 18:38:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',10,'2017-03-13 08:23:00','2017-03-13 18:31:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',11,'2017-03-13 08:01:00','2017-03-13 19:17:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',12,'2017-03-13 08:59:00','2017-03-13 17:51:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',13,'2017-03-13 07:32:00','2017-03-13 16:57:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',14,'2017-03-13 09:14:00','2017-03-13 16:54:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',15,'2017-03-13 08:12:00','2017-03-13 18:30:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',16,'2017-03-13 08:46:00','2017-03-13 16:56:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',17,'2017-03-13 08:50:00','2017-03-13 16:36:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',18,'2017-03-13 07:47:00','2017-03-13 19:14:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',19,'2017-03-13 08:13:00','2017-03-13 18:29:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',20,'2017-03-13 07:48:00','2017-03-13 17:59:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',21,'2017-03-13 08:06:00','2017-03-13 18:11:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',22,'2017-03-13 08:31:00','2017-03-13 18:34:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-13',23,'2017-03-13 08:02:00','2017-03-13 17:57:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',1,'2017-03-14 08:39:00','2017-03-14 18:25:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',2,'2017-03-14 08:33:00','2017-03-14 17:07:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',3,'2017-03-14 07:49:00','2017-03-14 17:08:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',4,'2017-03-14 07:41:00','2017-03-14 19:29:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',5,'2017-03-14 09:00:00','2017-03-14 17:41:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',6,'2017-03-14 08:40:00','2017-03-14 17:24:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',7,'2017-03-14 08:09:00','2017-03-14 19:10:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',8,'2017-03-14 08:48:00','2017-03-14 16:49:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',9,'2017-03-14 08:57:00','2017-03-14 16:47:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',10,'2017-03-14 08:34:00','2017-03-14 18:20:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',11,'2017-03-14 07:30:00','2017-03-14 17:22:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',12,'2017-03-14 08:26:00','2017-03-14 16:53:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',13,'2017-03-14 07:31:00','2017-03-14 19:02:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',14,'2017-03-14 07:56:00','2017-03-14 17:52:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',15,'2017-03-14 07:38:00','2017-03-14 17:50:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',16,'2017-03-14 09:04:00','2017-03-14 16:22:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',17,'2017-03-14 08:18:00','2017-03-14 17:37:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',18,'2017-03-14 08:20:00','2017-03-14 19:25:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',19,'2017-03-14 07:46:00','2017-03-14 17:05:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',20,'2017-03-14 09:17:00','2017-03-14 17:30:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',21,'2017-03-14 07:53:00','2017-03-14 17:58:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',22,'2017-03-14 08:45:00','2017-03-14 17:10:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-14',23,'2017-03-14 08:30:00','2017-03-14 17:13:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',1,'2017-03-15 07:57:00','2017-03-15 18:07:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',2,'2017-03-15 07:59:00','2017-03-15 19:08:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',3,'2017-03-15 08:21:00','2017-03-15 19:30:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',4,'2017-03-15 08:04:00','2017-03-15 18:17:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',5,'2017-03-15 09:19:00','2017-03-15 17:40:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',6,'2017-03-15 08:58:00','2017-03-15 16:28:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',7,'2017-03-15 08:15:00','2017-03-15 19:06:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',8,'2017-03-15 09:15:00','2017-03-15 18:55:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',9,'2017-03-15 07:58:00','2017-03-15 16:38:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',10,'2017-03-15 08:47:00','2017-03-15 16:27:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',11,'2017-03-15 08:19:00','2017-03-15 17:20:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',12,'2017-03-15 08:28:00','2017-03-15 19:27:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',13,'2017-03-15 07:33:00','2017-03-15 18:22:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',14,'2017-03-15 09:13:00','2017-03-15 16:59:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',15,'2017-03-15 07:39:00','2017-03-15 19:21:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',16,'2017-03-15 07:51:00','2017-03-15 17:15:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',17,'2017-03-15 07:55:00','2017-03-15 17:14:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',18,'2017-03-15 09:18:00','2017-03-15 18:49:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',19,'2017-03-15 08:41:00','2017-03-15 19:26:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',20,'2017-03-15 09:08:00','2017-03-15 18:28:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',21,'2017-03-15 08:52:00','2017-03-15 18:18:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',22,'2017-03-15 08:00:00','2017-03-15 16:55:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-15',23,'2017-03-15 09:16:00','2017-03-15 16:48:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',1,'2017-03-16 09:11:00','2017-03-16 17:55:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',2,'2017-03-16 07:44:00','2017-03-16 16:24:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',3,'2017-03-16 08:07:00','2017-03-16 18:32:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',4,'2017-03-16 08:14:00','2017-03-16 16:21:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',5,'2017-03-16 08:35:00','2017-03-16 19:18:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',6,'2017-03-16 09:02:00','2017-03-16 18:44:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',7,'2017-03-16 08:03:00','2017-03-16 19:15:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',8,'2017-03-16 08:32:00','2017-03-16 18:52:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',9,'2017-03-16 08:28:00','2017-03-16 18:44:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',10,'2017-03-16 08:23:00','2017-03-16 19:07:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',11,'2017-03-16 07:33:00','2017-03-16 19:29:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',12,'2017-03-16 08:18:00','2017-03-16 17:31:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',13,'2017-03-16 09:00:00','2017-03-16 17:52:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',14,'2017-03-16 07:50:00','2017-03-16 18:21:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',15,'2017-03-16 09:08:00','2017-03-16 19:30:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',16,'2017-03-16 08:31:00','2017-03-16 17:43:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',17,'2017-03-16 08:08:00','2017-03-16 19:22:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',18,'2017-03-16 08:34:00','2017-03-16 17:46:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',19,'2017-03-16 07:45:00','2017-03-16 17:20:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',20,'2017-03-16 07:51:00','2017-03-16 17:10:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',21,'2017-03-16 07:38:00','2017-03-16 18:10:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',22,'2017-03-16 07:44:00','2017-03-16 18:32:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-16',23,'2017-03-16 09:11:00','2017-03-16 16:45:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',1,'2017-03-17 08:03:00','2017-03-17 18:58:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',2,'2017-03-17 08:29:00','2017-03-17 19:15:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',3,'2017-03-17 08:42:00','2017-03-17 17:21:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',4,'2017-03-17 08:46:00','2017-03-17 17:16:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',5,'2017-03-17 09:18:00','2017-03-17 17:24:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',6,'2017-03-17 08:01:00','2017-03-17 18:34:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',7,'2017-03-17 08:17:00','2017-03-17 18:05:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',8,'2017-03-17 08:06:00','2017-03-17 18:53:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',9,'2017-03-17 09:04:00','2017-03-17 17:28:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',10,'2017-03-17 07:31:00','2017-03-17 16:46:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',11,'2017-03-17 09:03:00','2017-03-17 16:25:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',12,'2017-03-17 08:40:00','2017-03-17 19:10:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',13,'2017-03-17 08:02:00','2017-03-17 17:18:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',14,'2017-03-17 08:09:00','2017-03-17 18:07:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',15,'2017-03-17 09:17:00','2017-03-17 19:04:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',16,'2017-03-17 07:39:00','2017-03-17 16:31:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',17,'2017-03-17 07:43:00','2017-03-17 18:17:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',18,'2017-03-17 07:37:00','2017-03-17 16:29:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',19,'2017-03-17 07:30:00','2017-03-17 16:37:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',20,'2017-03-17 07:56:00','2017-03-17 17:15:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',21,'2017-03-17 08:53:00','2017-03-17 18:43:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',22,'2017-03-17 08:41:00','2017-03-17 16:48:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-17',23,'2017-03-17 08:48:00','2017-03-17 17:06:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',1,'2017-03-18 09:10:00','2017-03-18 18:50:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',2,'2017-03-18 07:58:00','2017-03-18 17:29:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',3,'2017-03-18 08:11:00','2017-03-18 16:40:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',4,'2017-03-18 09:16:00','2017-03-18 17:54:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',5,'2017-03-18 07:34:00','2017-03-18 16:41:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',6,'2017-03-18 08:36:00','2017-03-18 16:50:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',7,'2017-03-18 09:07:00','2017-03-18 16:22:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',8,'2017-03-18 09:13:00','2017-03-18 18:04:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',9,'2017-03-18 08:32:00','2017-03-18 19:26:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',10,'2017-03-18 08:05:00','2017-03-18 17:38:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',11,'2017-03-18 07:52:00','2017-03-18 18:14:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',12,'2017-03-18 09:06:00','2017-03-18 19:12:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',13,'2017-03-18 08:54:00','2017-03-18 19:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',14,'2017-03-18 07:55:00','2017-03-18 19:01:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',15,'2017-03-18 08:47:00','2017-03-18 17:42:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',16,'2017-03-18 08:25:00','2017-03-18 19:13:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',17,'2017-03-18 08:10:00','2017-03-18 19:16:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',18,'2017-03-18 07:32:00','2017-03-18 16:26:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',19,'2017-03-18 07:47:00','2017-03-18 17:27:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',20,'2017-03-18 08:58:00','2017-03-18 17:56:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',21,'2017-03-18 08:19:00','2017-03-18 17:02:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',22,'2017-03-18 08:43:00','2017-03-18 18:54:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-18',23,'2017-03-18 07:46:00','2017-03-18 16:42:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',1,'2017-03-19 08:45:00','2017-03-19 17:59:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',2,'2017-03-19 08:20:00','2017-03-19 19:08:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',3,'2017-03-19 07:36:00','2017-03-19 18:16:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',4,'2017-03-19 08:24:00','2017-03-19 18:57:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',5,'2017-03-19 08:37:00','2017-03-19 16:20:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',6,'2017-03-19 08:16:00','2017-03-19 18:28:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',7,'2017-03-19 07:49:00','2017-03-19 16:21:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',8,'2017-03-19 08:30:00','2017-03-19 18:30:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',9,'2017-03-19 07:35:00','2017-03-19 17:53:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',10,'2017-03-19 08:33:00','2017-03-19 19:24:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',11,'2017-03-19 09:20:00','2017-03-19 17:12:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',12,'2017-03-19 08:56:00','2017-03-19 17:45:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',13,'2017-03-19 09:05:00','2017-03-19 18:38:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',14,'2017-03-19 09:09:00','2017-03-19 17:49:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',15,'2017-03-19 08:22:00','2017-03-19 18:49:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',16,'2017-03-19 08:14:00','2017-03-19 17:30:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',17,'2017-03-19 08:44:00','2017-03-19 18:15:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',18,'2017-03-19 07:59:00','2017-03-19 18:51:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',19,'2017-03-19 08:00:00','2017-03-19 17:22:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',20,'2017-03-19 07:42:00','2017-03-19 17:57:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',21,'2017-03-19 08:49:00','2017-03-19 17:51:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',22,'2017-03-19 08:50:00','2017-03-19 18:25:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-19',23,'2017-03-19 08:51:00','2017-03-19 18:20:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',1,'2017-03-20 08:07:00','2017-03-20 17:47:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',2,'2017-03-20 09:02:00','2017-03-20 18:45:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',3,'2017-03-20 08:27:00','2017-03-20 16:39:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',4,'2017-03-20 08:57:00','2017-03-20 18:11:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',5,'2017-03-20 08:13:00','2017-03-20 18:39:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',6,'2017-03-20 09:01:00','2017-03-20 18:47:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',7,'2017-03-20 08:59:00','2017-03-20 16:33:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',8,'2017-03-20 08:38:00','2017-03-20 16:56:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',9,'2017-03-20 08:26:00','2017-03-20 16:59:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',10,'2017-03-20 09:12:00','2017-03-20 16:23:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',11,'2017-03-20 08:39:00','2017-03-20 18:37:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',12,'2017-03-20 09:19:00','2017-03-20 18:41:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',13,'2017-03-20 07:48:00','2017-03-20 19:28:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',14,'2017-03-20 07:53:00','2017-03-20 16:38:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',15,'2017-03-20 07:41:00','2017-03-20 18:22:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',16,'2017-03-20 07:57:00','2017-03-20 17:34:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',17,'2017-03-20 08:01:00','2017-03-20 19:23:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',18,'2017-03-20 07:39:00','2017-03-20 17:16:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',19,'2017-03-20 07:43:00','2017-03-20 17:02:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',20,'2017-03-20 08:45:00','2017-03-20 18:33:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',21,'2017-03-20 09:14:00','2017-03-20 19:24:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',22,'2017-03-20 08:47:00','2017-03-20 17:36:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-20',23,'2017-03-20 08:50:00','2017-03-20 16:25:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',1,'2017-03-21 08:41:00','2017-03-21 18:15:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',2,'2017-03-21 07:36:00','2017-03-21 16:57:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',3,'2017-03-21 08:21:00','2017-03-21 16:54:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',4,'2017-03-21 09:13:00','2017-03-21 16:31:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',5,'2017-03-21 08:39:00','2017-03-21 17:22:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',6,'2017-03-21 07:40:00','2017-03-21 18:58:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',7,'2017-03-21 07:38:00','2017-03-21 16:36:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',8,'2017-03-21 09:10:00','2017-03-21 17:43:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',9,'2017-03-21 08:04:00','2017-03-21 18:19:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',10,'2017-03-21 07:52:00','2017-03-21 17:07:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',11,'2017-03-21 09:04:00','2017-03-21 18:20:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',12,'2017-03-21 07:44:00','2017-03-21 18:17:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',13,'2017-03-21 07:53:00','2017-03-21 19:03:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',14,'2017-03-21 07:32:00','2017-03-21 17:31:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',15,'2017-03-21 07:33:00','2017-03-21 18:50:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',16,'2017-03-21 09:01:00','2017-03-21 16:55:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',17,'2017-03-21 08:35:00','2017-03-21 19:15:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',18,'2017-03-21 08:07:00','2017-03-21 19:13:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',19,'2017-03-21 08:52:00','2017-03-21 18:39:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',20,'2017-03-21 08:26:00','2017-03-21 18:56:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',21,'2017-03-21 08:38:00','2017-03-21 19:12:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',22,'2017-03-21 07:56:00','2017-03-21 18:24:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-21',23,'2017-03-21 07:58:00','2017-03-21 17:56:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',1,'2017-03-22 08:33:00','2017-03-22 16:37:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',2,'2017-03-22 08:17:00','2017-03-22 17:23:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',3,'2017-03-22 08:15:00','2017-03-22 18:31:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',4,'2017-03-22 07:31:00','2017-03-22 16:53:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',5,'2017-03-22 08:40:00','2017-03-22 19:26:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',6,'2017-03-22 09:05:00','2017-03-22 18:23:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',7,'2017-03-22 08:57:00','2017-03-22 16:51:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',8,'2017-03-22 08:03:00','2017-03-22 17:38:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',9,'2017-03-22 07:54:00','2017-03-22 16:35:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',10,'2017-03-22 08:22:00','2017-03-22 19:04:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',11,'2017-03-22 08:05:00','2017-03-22 18:55:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',12,'2017-03-22 08:18:00','2017-03-22 18:09:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',13,'2017-03-22 08:13:00','2017-03-22 16:38:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',14,'2017-03-22 08:54:00','2017-03-22 18:25:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',15,'2017-03-22 08:14:00','2017-03-22 18:32:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',16,'2017-03-22 08:28:00','2017-03-22 17:25:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',17,'2017-03-22 07:42:00','2017-03-22 16:27:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',18,'2017-03-22 08:30:00','2017-03-22 18:02:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',19,'2017-03-22 08:23:00','2017-03-22 18:12:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',20,'2017-03-22 08:51:00','2017-03-22 18:01:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',21,'2017-03-22 07:59:00','2017-03-22 17:32:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',22,'2017-03-22 08:42:00','2017-03-22 17:05:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-22',23,'2017-03-22 07:50:00','2017-03-22 18:45:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',1,'2017-03-23 08:31:00','2017-03-23 17:28:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',2,'2017-03-23 08:12:00','2017-03-23 19:25:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',3,'2017-03-23 09:15:00','2017-03-23 16:34:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',4,'2017-03-23 09:03:00','2017-03-23 17:20:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',5,'2017-03-23 09:19:00','2017-03-23 19:16:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',6,'2017-03-23 08:02:00','2017-03-23 18:59:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',7,'2017-03-23 07:55:00','2017-03-23 18:47:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',8,'2017-03-23 08:44:00','2017-03-23 16:39:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',9,'2017-03-23 09:17:00','2017-03-23 17:46:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',10,'2017-03-23 07:57:00','2017-03-23 17:21:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',11,'2017-03-23 08:29:00','2017-03-23 17:51:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',12,'2017-03-23 07:51:00','2017-03-23 17:14:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',13,'2017-03-23 09:09:00','2017-03-23 17:52:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',14,'2017-03-23 08:32:00','2017-03-23 17:00:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',15,'2017-03-23 08:19:00','2017-03-23 17:24:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',16,'2017-03-23 07:47:00','2017-03-23 18:40:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',17,'2017-03-23 08:59:00','2017-03-23 16:23:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',18,'2017-03-23 08:27:00','2017-03-23 18:38:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',19,'2017-03-23 08:24:00','2017-03-23 19:18:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',20,'2017-03-23 08:37:00','2017-03-23 16:56:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',21,'2017-03-23 08:56:00','2017-03-23 17:10:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',22,'2017-03-23 07:41:00','2017-03-23 16:47:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-23',23,'2017-03-23 07:46:00','2017-03-23 18:03:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',1,'2017-03-24 07:48:00','2017-03-24 19:11:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',2,'2017-03-24 07:30:00','2017-03-24 19:20:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',3,'2017-03-24 08:20:00','2017-03-24 19:08:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',4,'2017-03-24 09:18:00','2017-03-24 18:54:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',5,'2017-03-24 08:10:00','2017-03-24 19:22:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',6,'2017-03-24 09:07:00','2017-03-24 17:53:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',7,'2017-03-24 07:37:00','2017-03-24 19:07:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',8,'2017-03-24 08:48:00','2017-03-24 17:19:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',9,'2017-03-24 08:49:00','2017-03-24 19:02:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',10,'2017-03-24 08:08:00','2017-03-24 16:49:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',11,'2017-03-24 07:49:00','2017-03-24 18:29:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',12,'2017-03-24 08:06:00','2017-03-24 18:22:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',13,'2017-03-24 08:34:00','2017-03-24 18:11:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',14,'2017-03-24 09:11:00','2017-03-24 18:37:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',15,'2017-03-24 08:09:00','2017-03-24 18:08:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',16,'2017-03-24 08:11:00','2017-03-24 16:24:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',17,'2017-03-24 08:43:00','2017-03-24 18:52:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',18,'2017-03-24 08:46:00','2017-03-24 17:45:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',19,'2017-03-24 08:58:00','2017-03-24 18:13:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',20,'2017-03-24 09:00:00','2017-03-24 18:18:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',21,'2017-03-24 08:36:00','2017-03-24 19:09:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',22,'2017-03-24 07:34:00','2017-03-24 16:30:00')");
                cb() },
            function(cb) { migration.sequelize.query("INSERT INTO attendances(absent_date,id_employee,`in`,`out`) VALUES ('2017-03-24',23,'2017-03-24 08:16:00','2017-03-24 17:54:00')");
                cb() }
        ]);
    },
    down: function(QueryInterface) {
        return QueryInterface.dropTable("attendances");
    }
};