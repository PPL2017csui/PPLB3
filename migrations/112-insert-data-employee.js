var async = require('async');
var password = require('password-hash-and-salt');
var Promise = require('bluebird');

module.exports = {
    up: function(migration, Sequelize) {
        hash = Promise.promisify(password('pplcermaticeria').hash);
        return hash().then((pplcermaticeria) => {
            return async.series([
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (1,'genturwt@gmail.com','" + pplcermaticeria + "',1,'Gentur W.','2016-06-03','Palembang','1996-05-17',1,5000000,1,NULL,'male','08918182311','0218112239','114375683344','Jl. Saharjo no 2, Jakarta Selatan.')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (2,'bwiputri@gmail.com','" + pplcermaticeria + "',2,'Brigita Maria W.','2016-07-03','Jakarta','1996-04-19',1,6000000,8,NULL,'female','08152934031','0218349233','123859324211','Jl. Mangga Besar IVR no 10. Jakarta')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (3,'jason@gmail.com','" + pplcermaticeria + "',3,'Alexander Jason','2016-10-20','Jakarta','1990-06-09',0,5500000,2,NULL,'male','08938382300','0218298563','334375683389','JL. Tebet Raya No. 84, Tebet, Jakarta Selatan')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (4,'jael@gmail.com','" + pplcermaticeria + "',4,'Jael Ancela K','2016-04-21','Jakarta','1995-04-21',0,4500000,4,NULL,'female','08154934092','0218394857','155323900998','Jl. Metro Pondok Indah Kav. IV, Kebayoran Lama, Jakarta Selatan')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (5,'otniel@gmail.com','" + pplcermaticeria + "',5,'Otniel K.','2016-08-27','Jakarta','1992-08-27',0,8000000,6,NULL,'male','08958582393','0218394533','114433234554','Jl. Hos Cokroaminoto, No. 84, Menteng Jakarta Pusat.')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (6,'risceleliel23@gmail.com','" + pplcermaticeria + "',6,'Riscel Eliel F. I','2016-08-03','Jakarta','1996-08-23',1,7000000,9,NULL,'female','08156934096','0218439485','163859360861','Jl. Ahmad Dahlan/ Jl. Bacang I No.2 Jakarta Selatan')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (7,'muthyafi@gmail.com','" + pplcermaticeria + "',7,'Muthy Afifah','2016-10-04','Jakarta','1996-10-14',1,5000000,9,NULL,'female','08978782390','0213845860','155465657474','Jl. Alam Segar 3 No. 8, Pondok Indah Jakarta Selatan')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (8,'kustiawanto.halim@gmail.com','" + pplcermaticeria + "',8,'Kustiawanto Halim','2016-06-15','Payakumbuh','1996-03-02',1,7500000,9,NULL,'male','089888823921','0218298568','384875688889','Jl. Kebon Jeruk Raya No. 44 (depan SMPN 75) Jakarta Barat.')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (9,'angga@gmail.com','" + pplcermaticeria + "',9,'Angga','2016-08-03','Jakarta','1997-05-17',0,8000000,5,NULL,'male','08998982383','0218992239','232992222320','Jl. Raya Pasar Minggu Jakarta Selatan, Sebelah Makam Pahlawan Kalibata Jakarta Selatan')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (10,'komang@gmail.com','" + pplcermaticeria + "','10','Komang','2016-09-03','Jakarta','1997-04-19',0,4500000,3,NULL,'female','08910810823','0218102082','112228208232','Jl. Arya Putra, Kedaung Ciputat Tangerang')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (11,'rika@gmail.com','" + pplcermaticeria + "','11','Rika Mariska','2016-10-20','Jakarta','1990-07-09',0,8000000,7,NULL,'female','08911811823','0218298561','111141175688','Jl Wolter Monginsidi Raya 125 Jakarta')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (12,'harper.macias@gmail.com','" + pplcermaticeria + "','12','Harper Macias','2014-10-08','Peutie','1990-09-09',0,7000000,3,NULL,'female','086257355980','0216799975','1627040552599','Jl. Buaran Raya Blok D No. 1 Duren Sawit Jakarta Timur')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (13,'nell.howe@gmail.com','" + pplcermaticeria + "','13','Nell Howe','2016-09-11','Augusta','1983-03-28',0,5000000,5,NULL,'male','082804639425','0213218210','1615101784399','Gg Belimbing 12 RT 006/05 Jakarta')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (14,'warren.berger@gmail.com','" + pplcermaticeria + "','14','Warren Berger','2014-02-07','Épinal','1989-05-03',0,6000000,7,NULL,'male','083336466895','0216523619','1614111762299','Jl. Tebet Barat 1 No. 24 (Samping SMA 26) Jakarta Selatan')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (15,'phyllis.barrera@gmail.com','" + pplcermaticeria + "','15','Phyllis Barrera','2016-09-09','Calgary','1983-03-14',0,5500000,3,NULL,'male','086766569562','0213932336','1673102292699','Jl Gn Sahari Raya 65 Jakarta')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (16,'nina.ballard@gmail.com','" + pplcermaticeria + "','16','Nina Ballard','2014-07-26','Blankenfelde-Mahlow','1983-05-28',0,4500000,5,NULL,'female','082549350266','0214448272','1642041364399','Jl. Pangeran tubagus Angke Blok D9A No.2 Jakarta Barat')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (17,'michelle.meadows@gmail.com','" + pplcermaticeria + "','17','Michelle Meadows','2016-11-12','Maastricht','1981-01-27',0,8000000,7,NULL,'female','083715245724','0211287715','1667121035999','Green Bay Pluit Apartment Tower E-GF No. 23')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (18,'kelly.griffin@gmail.com','" + pplcermaticeria + "',1,'Kelly Griffin','2014-03-22','Candela','1989-09-10',0,7000000,10,NULL,'female','088379622365','0216239047','1639082132999','Jl. Palapa Raya No. 2, Pasar Minggu, Jakarta Selatan')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (19,'idola.fitzpatrick@gmail.com','" + pplcermaticeria + "',2,'Idola Fitzpatrick','2015-10-01','Bridgnorth','1990-08-28',0,5000000,11,NULL,'male','085971476236','0211785878','1644010266699','JL.Kapten Tendean no:121 (seberang sekolah tarakanita) Jakarta Selatan')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (20,'jared.giles@gmail.com','" + pplcermaticeria + "',3,'Jared Giles','2016-06-25','Angol','1994-06-18',0,6000000,12,NULL,'male','082988946236','0219655556','1615092554099','Jl. Cipinang Kebembem Rawa Mangun Jakarta')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (21,'reed.roach@gmail.com','" + pplcermaticeria + "',4,'Reed Roach','2014-11-15','Pemberton','1994-08-28',1,5500000,13,NULL,'male','089768360297','0212454326','1650082480599','Jl Kramat Pela III 1 RT 006/07 Jakarta')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (22,'madeline.pitts@gmail.com','" + pplcermaticeria + "',5,'Madeline Pitts','2014-06-07','Shaki','1985-03-10',0,4500000,14,NULL,'female','089952166569','0212733026','1609030245599','Jl Pulo Asem Tmr VII 11 RT 007/02 Jakarta')");
                    cb()
                },
                function(cb) {
                    migration.sequelize.query("INSERT INTO employees(id,email,password,employee_no,name,join_date,place_of_birth,date_of_birth,is_admin,salary,id_job_title,retire_date,gender,phone_no,telp_no,nik,address) VALUES (23,'axel.aguirre@gmail.com','" + pplcermaticeria + "',6,'Axel Aguirre','2016-08-28','Pemuco','1984-01-06',0,8000000,12,NULL,'male','083137869289','0214674798','1662042819199','Jl Agung Niaga V Bl G-5/41 Jakarta')");
                    cb()
                }
            ]);
        });
    },
    down: function(QueryInterface) {
        return QueryInterface.dropTable("employees");
    }
};