module.exports = {
    up: function(QueryInterface, Sequelize) {
        return QueryInterface.createTable("divisions", {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            id_company: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: { model: 'companies', key: 'id' },
                onUpdate: 'cascade',
                onDelete: 'cascade'
            },
            division_name: {
                type: Sequelize.STRING
            }
        }, {}).then(() => {
            QueryInterface.sequelize.query('' +
                'alter table `divisions`' +
                'add unique key `division_company_unique` (`id_company`, `division_name`)');
        });
    },
    down: function(QueryInterface) {
        return QueryInterface.dropTable("divisions");
    }
};