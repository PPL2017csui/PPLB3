module.exports = {
    up: function(QueryInterface, Sequelize) {
        return QueryInterface.createTable("companies", {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            company_name: {
                type: Sequelize.STRING,
                unique: true,
                allowNull: false
            },
            address: {
                type: Sequelize.STRING,
            },
            description: {
                type: Sequelize.STRING,
                allowNull: false
            }
        }, {});
    },
    down: function(QueryInterface) {
        return QueryInterface.dropTable("companies");
    }
};