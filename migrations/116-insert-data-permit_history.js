var async = require('async');

module.exports = {
    up: function(migration, Sequelize) {
        return async.series([
            function(cb) {
                migration.sequelize.query("INSERT INTO permit_histories(id,start_date,end_date,id_employee,approval_status,approval_date,superior_note,request_date,employee_note,expire_date,dayoff_type,value) VALUES (1,'2017-06-09','2017-06-09',1,'disetujui',NULL,NULL,'2017-04-28',NULL,'2017-06-11','sakit',10)");
                cb()
            },
            function(cb) {
                migration.sequelize.query("INSERT INTO permit_histories(id,start_date,end_date,id_employee,approval_status,approval_date,superior_note,request_date,employee_note,expire_date,dayoff_type,value) VALUES (2,'2017-06-21','2017-06-21',2,'disetujui',NULL,NULL,'2017-04-28',NULL,'2017-06-21','sakit',14)");
                cb()
            },
            function(cb) {
                migration.sequelize.query("INSERT INTO permit_histories(id,start_date,end_date,id_employee,approval_status,approval_date,superior_note,request_date,employee_note,expire_date,dayoff_type,value) VALUES (3,'2017-06-15','2017-06-15',3,'disetujui',NULL,NULL,'2017-04-28',NULL,'2017-06-18','sakit',9)");
                cb()
            },
            function(cb) {
                migration.sequelize.query("INSERT INTO permit_histories(id,start_date,end_date,id_employee,approval_status,approval_date,superior_note,request_date,employee_note,expire_date,dayoff_type,value) VALUES (4,'2017-06-15','2017-06-15',23,'disetujui',NULL,NULL,'2017-04-28',NULL,'2017-06-18','sakit',9)");
                cb()
            },
            function(cb) {
                migration.sequelize.query("INSERT INTO permit_histories(id,start_date,end_date,id_employee,approval_status,approval_date,superior_note,request_date,employee_note,expire_date,dayoff_type,value) VALUES (5,'2017-07-12','2017-07-21',2,'disetujui',NULL,NULL,'2017-04-28','Izin karena flu','2017-06-21','sakit',-9)");
                cb()
            },
            function(cb) {
                migration.sequelize.query("INSERT INTO permit_histories(id,start_date,end_date,id_employee,approval_status,approval_date,superior_note,request_date,employee_note,expire_date,dayoff_type,value) VALUES (6,'2017-07-28','2017-07-28',2,'menunggu persetujuan',NULL,NULL,'2017-04-28','Libur ke kota','2017-06-21','cuti',-1)");
                cb()
            },
            function(cb) {
                migration.sequelize.query("INSERT INTO permit_histories(id,start_date,end_date,id_employee,approval_status,approval_date,superior_note,request_date,employee_note,expire_date,dayoff_type,value) VALUES (7,'2017-08-09','2017-08-09',2,'ditolak',NULL,NULL,'2017-09-28','Mau libur aja','2017-10-21','izin',-1)");
                cb()
            }
        ]);
    },
    down: function(QueryInterface) {
        return QueryInterface.dropTable("permit_histories");
    }
};