module.exports = {
    up: function(QueryInterface, Sequelize) {
        return QueryInterface.createTable("employees_salary_and_benefit", {
            id_salary_benefit: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                references: { model: 'salaries_and_benefits', key: 'id' },
                onUpdate: 'cascade',
                onDelete: 'cascade'
            },
            id_employee: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                references: { model: 'employees', key: 'id' },
                onUpdate: 'cascade',
                onDelete: 'cascade'
            },
            nominal: {
                type: Sequelize.INTEGER,
                allowNull: false
            }
        }, {});
    },
    down: function(QueryInterface) {
        return QueryInterface.dropTable("employees_salary_and_benefit");
    }
};