var async = require('async');

module.exports = {
    up: function(migration, Sequelize) {
        return async.series([
            function(cb) { migration.sequelize.query("INSERT INTO jobs(id,job_title,supervisor,id_division) VALUES (1,'Owner',NULL,1)"); cb()},
            function(cb) { migration.sequelize.query("INSERT INTO jobs(id,job_title,supervisor,id_division) VALUES (2,'Head of Marketing',1,2)"); cb()},
            function(cb) { migration.sequelize.query("INSERT INTO jobs(id,job_title,supervisor,id_division) VALUES (3,'Staff Marketing',2,2)"); cb()},
            function(cb) { migration.sequelize.query("INSERT INTO jobs(id,job_title,supervisor,id_division) VALUES (4,'Head of Development',1,3)"); cb()},
            function(cb) { migration.sequelize.query("INSERT INTO jobs(id,job_title,supervisor,id_division) VALUES (5,'Staff Development',4,3)"); cb()},
            function(cb) { migration.sequelize.query("INSERT INTO jobs(id,job_title,supervisor,id_division) VALUES (6,'Head of Technician',1,4)"); cb()},
            function(cb) { migration.sequelize.query("INSERT INTO jobs(id,job_title,supervisor,id_division) VALUES (7,'Technician',6,4)"); cb()},
            function(cb) { migration.sequelize.query("INSERT INTO jobs(id,job_title,supervisor,id_division) VALUES (8,'Head of Human Resource',1,6)"); cb()},
            function(cb) { migration.sequelize.query("INSERT INTO jobs(id,job_title,supervisor,id_division) VALUES (9,'staff of Human Resource',8,6)"); cb()},
            function(cb) { migration.sequelize.query("INSERT INTO jobs(id,job_title,supervisor,id_division) VALUES (10,'Founder',NULL,7)"); cb()},
            function(cb) { migration.sequelize.query("INSERT INTO jobs(id,job_title,supervisor,id_division) VALUES (11,'Head of Technician',10,8)"); cb()},
            function(cb) { migration.sequelize.query("INSERT INTO jobs(id,job_title,supervisor,id_division) VALUES (12,'Technician',11,8)"); cb()},
            function(cb) { migration.sequelize.query("INSERT INTO jobs(id,job_title,supervisor,id_division) VALUES (13,'Accounting',10,9)"); cb()},
            function(cb) { migration.sequelize.query("INSERT INTO jobs(id,job_title,supervisor,id_division) VALUES (14,'Secretary',10,9)"); cb()}
        ]);
    },
    down: function(QueryInterface) {
        return QueryInterface.dropTable("jobs");
    }
};