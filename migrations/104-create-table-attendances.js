module.exports = {
    up: function(QueryInterface, Sequelize) {
        return QueryInterface.createTable("attendances", {
            absent_date: {
                type: Sequelize.DATEONLY,
                primaryKey: true,
            },
            id_employee: {
                type: Sequelize.INTEGER,
                allowNull: false,
                primaryKey: true,
                references: { model: 'employees', key: 'id' },
                onUpdate: 'cascade',
                onDelete: 'cascade'
            },
            in: Sequelize.TIME,
            out: Sequelize.TIME
        }, {});
    },
    down: function(QueryInterface) {
        return QueryInterface.dropTable("attendances");
    }
};