module.exports = {
    up: function(QueryInterface, Sequelize) {
        return QueryInterface.createTable("salaries_and_benefits", {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            id_company: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: { model: 'companies', key: 'id' },
                onUpdate: 'cascade',
                onDelete: 'cascade'
            },
            salary_benefit_name: {
                type: Sequelize.STRING,
                allowNull: false
            }
        }, {});
    },
    down: function(QueryInterface) {
        return QueryInterface.dropTable("salaries_and_benefits");
    }
};