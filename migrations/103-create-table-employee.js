module.exports = {
    up: function(QueryInterface, Sequelize) {
        return QueryInterface.createTable("employees", {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            email: {
                type: Sequelize.STRING,
                unique: true,
                allowNull: false
            },
            password: {
                type: Sequelize.TEXT,
                allowNull: true
            },
            employee_no: {
                type: Sequelize.STRING,
                allowNull: false
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            join_date: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.NOW,
                allowNull: false
            },
            place_of_birth: {
                type: Sequelize.STRING,
                allowNull: true
            },
            date_of_birth: {
                type: Sequelize.DATEONLY,
                allowNull: false

            },
            is_admin: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
                allowNull: false
            },
            salary: {
                type: Sequelize.INTEGER,
                allowNull: true
            },
            id_job_title: {
                type: Sequelize.INTEGER,
                references: { model: 'jobs', key: 'id' },
                onUpdate: 'cascade',
                onDelete: 'cascade'
            },
            retire_date: Sequelize.DATE,
            gender: {
                type: Sequelize.ENUM,
                values: ['male', 'female']
            },
            phone_no: {
                type: Sequelize.STRING,
                unique: true,
                allowNull: true
            },
            telp_no: {
                type: Sequelize.STRING,
                allowNull: true
            },
            nik: {
                type: Sequelize.STRING,
                unique: true,
                allowNull: false
            },
            address: {
                type: Sequelize.STRING,
                allowNull: true
            },
            token: {
                type: Sequelize.TEXT,
                allowNull: true
            }
        }, {})
    },
    down: function(QueryInterface) {
        return QueryInterface.dropTable("employees");
    }
};