var async = require('async');

module.exports = {
    up: function(migration, Sequelize) {
        return async.series([
            function(cb) { migration.sequelize.query("INSERT INTO divisions(id,id_company,division_name) VALUES (1,1,'Board of Management')"); cb(); },
            function(cb) { migration.sequelize.query("INSERT INTO divisions(id,id_company,division_name) VALUES (2,1,'Marketing')"); cb(); },
            function(cb) { migration.sequelize.query("INSERT INTO divisions(id,id_company,division_name) VALUES (3,1,'Development')"); cb(); },
            function(cb) { migration.sequelize.query("INSERT INTO divisions(id,id_company,division_name) VALUES (4,1,'Technical')"); cb(); },
            function(cb) { migration.sequelize.query("INSERT INTO divisions(id,id_company,division_name) VALUES (5,1,'Finance')"); cb(); },
            function(cb) { migration.sequelize.query("INSERT INTO divisions(id,id_company,division_name) VALUES (6,1,'Human Resource')"); cb(); },
            function(cb) { migration.sequelize.query("INSERT INTO divisions(id,id_company,division_name) VALUES (7,2,'Board of Management')"); cb(); },
            function(cb) { migration.sequelize.query("INSERT INTO divisions(id,id_company,division_name) VALUES (8,2,'Technical')"); cb(); },
            function(cb) { migration.sequelize.query("INSERT INTO divisions(id,id_company,division_name) VALUES (9,2,'Administration')"); cb(); }
        ]);
    },
    down: function(QueryInterface) {
        return QueryInterface.dropTable("divisions");
    }
};