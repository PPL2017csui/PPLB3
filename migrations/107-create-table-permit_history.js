module.exports = {
    up: function(QueryInterface, Sequelize) {
        return QueryInterface.createTable("permit_histories", {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            start_date: {
                type: Sequelize.DATEONLY,
                allowNull: false,
                unique: 'compositeIndex'
            },
            end_date: {
                type: Sequelize.DATEONLY,
                allowNull: false,
                unique: 'compositeIndex'
            },
            id_employee: {
                type: Sequelize.INTEGER,
                allowNull: false,
                unique: 'compositeIndex',
                references: { model: 'employees', key: 'id' },
                onUpdate: 'cascade',
                onDelete: 'cascade'
            },
            approval_status: {
                type: Sequelize.ENUM,
                values: ['disetujui', 'menunggu persetujuan', 'ditolak'],
                allowNull: false,
                defaultValue: 'menunggu persetujuan'
            },
            approval_date: {
                type: Sequelize.DATEONLY,
                allowNull: true
            },
            superior_note: {
                type: Sequelize.STRING,
                allowNull: true
            },
            request_date: {
                type: Sequelize.DATEONLY,
                defaultValue: Sequelize.NOW,
                allowNull: false
            },
            employee_note: {
                type: Sequelize.STRING,
                allowNull: true
            },
            expire_date: {
                type: Sequelize.DATEONLY,
                allowNull: false
            },
            dayoff_type: {
                type: Sequelize.ENUM,
                values: ['sakit', 'izin', 'cuti'],
                allowNull: false
            },
            value: {
                type: Sequelize.INTEGER,
            },
        }, {})
    },
    down: function(QueryInterface) {
        return QueryInterface.dropTable("permit_histories");
    }
};