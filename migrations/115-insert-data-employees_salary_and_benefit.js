var async = require('async');

module.exports = {
    up: function(migration, Sequelize) {
        return async.series([
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (1,1,'20000000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (1,2,'10000000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (1,3,'15000000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (1,4,'15000000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (1,5,'9200000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (1,6,'5200000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (1,7,'5700000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (1,8,'7400000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (1,9,'6300000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (1,10,'7000000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (1,11,'6000000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (1,12,'9800000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (1,13,'5400000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (1,14,'4800000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (1,15,'3900000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (1,16,'7400000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (2,17,'3500000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (3,18,'6300000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (3,19,'4600000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (3,20,'9900000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (3,21,'9700000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (3,22,'4400000')") ; cb()},
            function(cb) { migration.sequelize.query("INSERT INTO employees_salary_and_benefit(id_salary_benefit,id_employee,nominal) VALUES (3,23,'4500000')") ; cb()}

        ]);
    },
    down: function(QueryInterface) {
        return QueryInterface.dropTable("employees_salary_and_benefit");
    }
};