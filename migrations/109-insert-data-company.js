var async = require('async');

module.exports = {
    up: function(migration, Sequelize) {
        return async.series([
            function(cb) { migration.sequelize.query("INSERT INTO companies(id,company_name,address,description) VALUES (1,'PT Sukses','Pondok Cina, Depok.','PT Sukses didirikan tahun 2010. Kami bergerak dibidang infrastruktur.')"); cb(); },
            function(cb) { migration.sequelize.query("INSERT INTO companies(id,company_name,address,description) VALUES (2,'PT Abadi','Cempaka Putih, Jakarta.','PT Abadi didirikan tahun 2000. Kami berkomitmen untuk selalu memberikan pelayanan terbaik kepada customer')"); cb(); }
        ]);
    },
    down: function(QueryInterface) {
        return QueryInterface.dropTable("companies");
    }
};