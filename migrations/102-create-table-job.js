module.exports = {
    up: function(QueryInterface, Sequelize) {
        return QueryInterface.createTable("jobs", {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            job_title: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: 'jobAndDiv',
            },
            supervisor: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: { model: 'jobs', key: 'id' },
                onUpdate: 'cascade',
                onDelete: 'cascade',
            },
            id_division: {
                type: Sequelize.INTEGER,
                references: { model: 'divisions', key: 'id' },
                onUpdate: 'cascade',
                onDelete: 'cascade',
                unique: 'jobAndDiv',
            }
        }, {}).then(() => {
            QueryInterface.sequelize.query("" +
                "alter table `jobs`" +
                "add unique key `job_and_div` (`job_title`, `id_division`)"
            );
        });
    },
    down: function(QueryInterface) {
        return QueryInterface.dropTable("jobs");
    }
};