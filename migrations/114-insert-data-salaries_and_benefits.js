var async = require('async');

module.exports = {
    up: function(migration, Sequelize) {
        return async.series([
            function(cb) { migration.sequelize.query("INSERT INTO salaries_and_benefits(id,id_company,salary_benefit_name) VALUES (1,1,'Gaji Pokok')"); cb()},
            function(cb) { migration.sequelize.query("INSERT INTO salaries_and_benefits(id,id_company,salary_benefit_name) VALUES (2,1,'Part Time')"); cb()},
            function(cb) { migration.sequelize.query("INSERT INTO salaries_and_benefits(id,id_company,salary_benefit_name) VALUES (3,2,'Gaji Pokok')"); cb()},
        ]);
    },
    down: function(QueryInterface) {
        return QueryInterface.dropTable("salaries_and_benefits");
    }
};