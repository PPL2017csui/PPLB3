require('dotenv').config();
var assert = require('chai').assert;
var expect = require('chai').expect;
var sinon = require('sinon');
var underTest = require('../handlers/division');
var Division = require('../models').Division;
var Promise = require('bluebird');
var Sequelize = require('sequelize');
var sequelize = new Sequelize();

describe('division', function() {
    var divisionFindStub;
    var divisionCreateStub;

    describe('on view create divisions', function(done) {
        describe('should show render view', function(done) {
            it('flash should contain message', function(done) {
                var req = {
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                            done();
                        } catch (err) {}
                    }
                };
                var res = {
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'division-create');
                        } catch (err) {}
                    }
                };
                underTest.viewCreateDivision(req, res, function() {});
            });
            it('res should render division-create view', function(done) {
                var req = {
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                            done();
                        } catch (err) {}
                    }
                };
                var res = {
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'division-create');
                        } catch (err) {}
                    }
                };
                underTest.viewCreateDivision(req, res, function() {});
            });
        });
    });

    describe('on view list of divisions', function(done) {
        describe('should show list of divisions', function(done) {
            beforeEach(function(done) {
                divisionFindStub = sinon.stub(Division, 'findAll', function() {
                    return Promise.resolve();
                });
                underTest.viewListDivision = Promise.promisify(underTest.viewListDivision);
                done();
            })
            afterEach(function(done) {
                divisionFindStub.restore();
                done();
            })
            it('res should render division-view', function(done) {
                var req = {
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                        } catch (err) {}
                    }
                };
                var res = {
                    locals: {
                        company: '1'
                    },
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'division-view');
                            done();
                        } catch (err) {}
                    }
                };
                var id_company = 1;
                underTest.viewListDivision(req, res, function() {});
            });
        });
        describe('on data not available, should show error', function(done) {
            beforeEach(function(done) {
                divisionFindStub = sinon.stub(Division, 'findAll', function() {
                    return Promise.reject(new Error('test-error'));
                });
                underTest.viewListDivision = Promise.promisify(underTest.viewListDivision);
                done();
            })
            afterEach(function(done) {
                divisionFindStub.restore();
                done();
            })
            it('check for flash', function(done) {
                var req = {
                    flash: function(args) {
                        return ['bri'];
                    }
                };
                var res = {
                    locals: {
                        company: '1'
                    },
                    render: function(args, opts) {
                        try {
                            assert.equal(args, 'division-view');
                            assert.deepEqual(opts, { message: ['bri'], perusahaan: true });
                            done();
                        } catch (err) {}
                    }
                };
                underTest.viewListDivision(req, res, function() {});
            });
            it('res should redirect to division', function(done) {
                var req = {
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                        } catch (err) {}
                    }
                };
                var res = {
                    locals: {
                        company: '1'
                    },
                    render: function(args) {
                        try {
                            assert.equal(args, 'division-view');
                            done();
                        } catch (err) {}
                    }
                };
                underTest.viewListDivision(req, res, function() {});
            });
        });
    });
    describe('on success creating division', function() {
        var id_company;
        var division_name;
        beforeEach(function(done) {
            divisionCreateStub = sinon.stub(Division, 'create', function(done) {
                return Promise.resolve();
            })
            underTest.saveCreateDivision = Promise.promisify(underTest.saveCreateDivision);
            done();
        });
        afterEach(function(done) {
            divisionCreateStub.restore();
            done();
        });
        it('should create division - check req', function(done) {
            var req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Divisi berhasil dibuat');
                        done();
                    } catch (err) {}
                },
                body: {
                    division_name: 'Marketing'
                }
            };
            var res = {
                locals: {
                    company: '1'
                },
                redirect: function(args) {
                    try {
                        assert.equal(args, '/division/create');
                    } catch (err) {}
                }
            };
            underTest.saveCreateDivision(req, res, function() {});
        });
        it('should create division - check res', function(done) {
            var req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Divisi berhasil dibuat');
                    } catch (err) {}
                },
                body: {
                    division_name: 'Marketing'
                }
            };
            var res = {
                locals: {
                    company: '1'
                },
                redirect: function(args) {
                    try {
                        assert.equal(args, '/division/create');
                        done();
                    } catch (err) {}
                }
            };
            underTest.saveCreateDivision(req, res, function() {});
        });
    });

    describe('on failed create division', function(done) {
        var id_company;
        var division_name;
        beforeEach(function(done) {
            divisionCreateStub = sinon.stub(Division, 'create', function() {
                return Promise.reject(new Error('test-error'));
            });
            underTest.saveCreateDivision = Promise.promisify(underTest.saveCreateDivision);
            done();
        });
        afterEach(function(done) {
            divisionCreateStub.restore();
            done();
        });

        it('should failed create division - check req', function(done) {
            var req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Data divisi yang Anda masukkan salah');
                        done();
                    } catch (err) {}
                },
                body: {
                    division_name: 'Marketing'
                }
            };
            var res = {
                locals: {
                    company: '0'
                },
                redirect: function(args) {
                    try {
                        assert.equal(args, '/division/create');
                    } catch (err) {}
                }
            };
            underTest.saveCreateDivision(req, res, function() {});
        });
        it('should failed create division - check res', function(done) {
            var req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Data divisi yang Anda masukkan salah');
                    } catch (err) {}
                },
                body: {
                    division_name: 'Marketing'
                }
            };
            var res = {
                locals: {
                    company: '0'
                },
                redirect: function(args) {
                    try {
                        assert.equal(args, '/division/create');
                        done();
                    } catch (err) {}
                }
            };
            underTest.saveCreateDivision(req, res, function() {});
        });
        it('should redirect to create division page on existing division', function(done) {
            divisionCreateStub.restore();
            divisionCreateStub = sinon.stub(Division, 'create', function() {
                return Promise.reject(new Sequelize.UniqueConstraintError());
            });
            var req = {
                body: {},
                flash: function(key, val) {}
            };
            var res = {
                locals: {
                    company: 1
                },
                send: function(args) {},
                redirect: function(args) {
                    try {
                        assert.equal(args, '/division/create');
                        done();
                    } catch (err) {}
                }
            };
            underTest.saveCreateDivision(req, res, function() {});
        });
    });
    describe('on get division JSON', function(done) {
        beforeEach(function() {
            divisionFindStub = sinon.stub(Division, 'findAll', function() {
                return Promise.resolve({ result: [{ division: 'Board of Management' }, { division: 'Marketing' }] });
            });
            underTest.getDivision = Promise.promisify(underTest.getDivision);
        });

        afterEach(function() {
            divisionFindStub.restore();
        });
        it('should return JSON file', function(done) {
            var req = {};
            var res = {
                json: function(args) {
                    assert.deepEqual(args, { result: [{ division: 'Board of Management' }, { division: 'Marketing' }] });
                    done();
                },
                locals: {
                    company: 1
                },
            };
            underTest.getDivision(req, res, function() {});
        });
    });
    describe('on error when get division JSON', function(done) {
        beforeEach(function() {
            divisionFindStub = sinon.stub(Division, 'findAll', function() {
                return Promise.reject(new Error('test-error'));
            });
            underTest.getDivision = Promise.promisify(underTest.getDivision);
        });

        afterEach(function() {
            divisionFindStub.restore();
        });
        it('should return JSON file', function(done) {
            var req = {};
            var res = {
                send: function(args) {
                    assert.equal(args, 'Error: test-error');
                    done();
                },
                locals: {
                    company: 1
                },
            };
            underTest.getDivision(req, res, function() {});
        });
    });
});