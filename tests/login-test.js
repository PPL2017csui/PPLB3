require('dotenv').config();
var assert = require('chai').assert;
var sinon = require('sinon');
var rewire = require('rewire');
var underTest = rewire('../handlers/login');
var Login = require('../models').Login;
var Employee = require('../models').Employee;
var Promise = require('bluebird');
var Sequelize = require('sequelize');
var password = require('password-hash-and-salt');

describe('login', function() {
    var loginStub;
    var emailFindStub;
    describe('on viewing login page', function(done) {
        describe('should show render view', function(done) {
            it('flash should contain message', function(done) {
                var req = {
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                            done();
                        } catch (err) {}
                    }
                };
                var res = {
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'login');
                        } catch (err) {}
                    }
                };
                underTest.viewLogin(req, res, function() {});
            });
            it('res should render login page', function(done) {
                var req = {
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                            done();
                        } catch (err) {}
                    }
                };
                var res = {
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'login');
                        } catch (err) {}
                    }
                };
                underTest.viewLogin(req, res, function() {});
            });
        });
    });
    describe('on login employee', function(done) {
        beforeEach(function(done) {
            emailFindStub = sinon.stub(Employee, 'findOne', function() {
                return Promise.resolve({
                    id: 7,
                    employee_no: '7',
                    password: 'pplcermaticeria',
                    email: 'muthyafi@gmail.com',
                    name: 'Muthy Afifah',
                    join_date: '2016 - 10 - 04 T00: 00: 00.000 Z',
                    place_of_birth: 'Jakarta',
                    date_of_birth: '1996 - 10 - 14 T00: 00: 00.000 Z',
                    phone: null,
                    gender: 'female',
                    is_admin: true,
                    salary: null,
                    address: null,
                    nik: '155465657474',
                    retire_date: null
                });
            });
            underTest.authenticate = Promise.promisify(underTest.authenticate);
            done();
        });
        afterEach(function(done) {
            emailFindStub.restore();
            done();
        });
        it('should success find employee email, redirect to employee view', function(done) {
            var verifyAgainst = underTest.__set__('password', function() {
                return {
                    verifyAgainst(pass, cb) {
                        cb({}, true);
                    }
                }
            });
            var req = {
                session: {},
                body: {
                    email: 'muthyafi@gmail.com',
                    password: 'pplcermaticeria'
                },
                flash: (args, argss) => {console.log(args, argss)}
            };
            var res = {
                redirect: function(args) {
                    assert.equal(args, '/dashboard');
                    done();
                    verifyAgainst();
                }
            };
            underTest.authenticate(req, res, function() {});
        });
        it('on wrong password, should redirect to login - check req', function(done) {
            var req = {
                flash: function(key, val) {
                    assert.equal(key, 'message');
                    assert.equal(val, 'Password salah');
                    done();
                },
                body: {
                    email: 'muthyafi@gmail.com',
                    password: 'pplcermaticeriaaaa'
                },
            };
            var res = {
                redirect: function(args) {
                    assert.equal(args, '/login');
                }
            };
            underTest.authenticate(req, res, function() {});
        });
        it('on wrong password, should redirect to login - check res', function(done) {
            var req = {
                flash: function(key, val) {
                    assert.equal(key, 'message');
                    assert.equal(val, 'Password salah');
                },
                body: {
                    email: 'muthyafi@gmail.com',
                    password: 'pplcermaticeriaaaa'
                },
            };
            var res = {
                redirect: function(args) {
                    assert.equal(args, '/login');
                    done();
                }
            };
            underTest.authenticate(req, res, function() {});
        });
    });
    describe('on invalid email', function(done) {
        beforeEach(function(done) {
            emailFindStub = sinon.stub(Employee, 'findOne', function(done) {
                return Promise.reject(new Error('test-error'));
            });
            underTest.authenticate = Promise.promisify(underTest.authenticate);
            done();
        });
        afterEach(function(done) {
            emailFindStub.restore();
            done();
        });
        it('should show error message - check req', function(done) {
            var req = {
                body: {
                    email: "muthy@gmail.com"
                },
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, "Email salah");
                        done();
                    } catch (err) {}
                }
            };
            var res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/login');
                    } catch (err) {}
                }
            };
            underTest.authenticate(req, res, function() {});
        });
        it('should show error message - check res', function(done) {
            var req = {
                body: {
                    email: "muthy@gmail.com"
                },
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, "Email salah");
                    } catch (err) {}
                }
            };
            var res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/login');
                        done();
                    } catch (err) {}
                }
            };
            underTest.authenticate(req, res, function() {});
        });
    });
});