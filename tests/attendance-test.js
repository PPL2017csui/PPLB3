require('dotenv').config();
require('../models/relations')();
let assert = require('chai').assert;
let rewire = require('rewire');
let sinon = require('sinon');
let underTest = rewire('../handlers/attendance');
let model = require('../models');
let Promise = require('bluebird');
let fs = require('fs');
var Sequelize = require('sequelize');

describe('attendance', function() {
    describe('on view attendance', function() {
        it('should gather attendance data with employee name, job title, and division', function(done) {
            let attendance = sinon.stub(model.Attendance, 'findAll', function(args) {
                assert.deepEqual(args, {
                    include: [{
                        association: model.Attendance.Employee,
                        include: [{
                            association: model.Employee.Job,
                            include: [{
                                association: model.Job.Division,
                                where: {
                                    id_company: 1
                                },
                                include: [{
                                    as: 'company',
                                    model: model.Company,
                                }]
                            }]
                        }]
                    }]
                });
                done();
                return Promise.resolve([]);
            });
            let res = {
                render: () => {},
                locals: {
                    company: 1
                }
            };
            underTest.viewAttendance({ flash: function() {} }, res);
            attendance.restore();
        });
        it('should hydrate attendance data', function(done) {
            attendance = sinon.stub(model.Attendance, 'findAll').returns(Promise.resolve([]));
            underTest.__set__('hydrateCalled', false);
            revert = underTest.__set__('hydrateAttendance', function() {
                underTest.__set__('hydrateCalled', true);
                assert.isTrue(underTest.__get__('hydrateCalled'));
                done();
            });
            let res = {
                render: () => {},
                locals: {
                    company: 1
                }
            };
            underTest.viewAttendance({ flash: function() {} }, res);
            attendance.restore();
            revert();
        });
        it('should render employee attendance handlebar', function(done) {
            let attendance = sinon.stub(model.Attendance, 'findAll').returns(Promise.resolve([]));

            let res = {
                render: (view, args) => {
                    assert.equal(view, 'employee-attendance');
                    assert.deepEqual(args.attendances, []);
                    done();
                },
                locals: {
                    company: 1
                }
            };
            underTest.viewAttendance({ flash: function() {} }, res);
            attendance.restore();
            revert();
        });
    });
    describe('on uploading csv', function() {
        let req = {
            flash: function(args) {
                if (args === 'formatError' || args == 'dataError') return ['true'];

            },
            file: {
                path: 'test',
                mimetype: 'text/csv',
                originalname: 'template.csv'
            }
        };
        let res = {
            redirect: () => {},
            locals: {
                company: 1
            }
        };
        let createReadStream;
        let saveCsvData;

        before(() => {
            createReadStream = sinon.stub(fs, 'createReadStream', () => {
                return 1;
            });
            saveCsvData = underTest.__set__('saveCsvData', () => {
                return 0;
            });
        });
        after(() => {
            createReadStream.restore();
            saveCsvData();
        });
        it('should retrieve employee no and ID', function(done) {
            let findAll = sinon.stub(model.Employee, 'findAll', function(args) {
                assert.deepEqual(args, {
                    attributes: ['id', 'employee_no'],
                    include: [{
                        association: model.Employee.Job,
                        include: [{
                            association: model.Job.Division,
                            where: {
                                id_company: 1
                            },
                            include: [{
                                as: 'company',
                                model: model.Company,
                            }]
                        }]
                    }]
                });
                done();
                return Promise.resolve([]);
            });
            underTest.saveAttendance(req, res);
            findAll.restore();
        });
        it('should reject on receiving non-csv file', function(done) {
            let req = {
                flash: function(msg, args) {
                    assert.equal(args, 'Sistem hanya dapat menerima file CSV');
                },
                file: {
                    path: 'test',
                    mimetype: 'text/cs',
                    originalname: 'template.txt'
                }
            };
            let res = {
                redirect: (args) => {
                    assert.equal(args, '/attendance');
                    done();
                }
            };
            underTest.saveAttendance(req, res);
        });
        it('should create dictionary with employee no as key and id as value', function(done) {
            let findAll = sinon.stub(model.Employee, 'findAll').returns(Promise.resolve([]));
            dictionarize = underTest.__set__('dictionarizeEmployee', () => {
                underTest.__set__('called', true);
            });
            underTest.saveAttendance(req, res)
                .then(() => {
                    assert.isTrue(underTest.__get__('called'));
                    done();
                });
            dictionarize();
            findAll.restore();
        });
        it('should redirect upon invalid csv data', function(done) {
            let findAll = sinon.stub(model.Employee, 'findAll').returns(Promise.resolve([]));
            let req = {
                flash: function(msg, args) {
                    if (msg === 'formatError') return ['true'];
                    else {
                        assert.equal(args, 'Pastikan format file yang Anda upload sudah benar');
                    }
                    done();
                },
                file: {
                    path: 'test',
                    mimetype: 'text/csv',
                    originalname: 'template.csv'
                }
            };
            underTest.saveAttendance(req, res);
            findAll.restore();
        });
        it('should redirect upon duplication data', function(done) {
            let findAll = sinon.stub(model.Employee, 'findAll').returns(Promise.resolve([]));
            let req = {
                flash: function(msg, args) {
                    if (msg === 'dataError') return ['true'];
                    else if (msg === 'formatError') return ['false'];
                    else {
                        assert.equal(args, 'Terdapat duplikasi data');
                    }
                    done();
                },
                file: {
                    path: 'test',
                    mimetype: 'text/csv',
                    originalname: 'template.csv'
                }
            };
            underTest.saveAttendance(req, res);
            findAll.restore();
        });
        it('should flash success message on valid data', function(done) {
            let findAll = sinon.stub(model.Employee, 'findAll').returns(Promise.resolve([]));
            let req = {
                flash: function(msg, args) {
                    if (msg === 'dataError' || msg === 'formatError') return ['false'];
                    else {
                        assert.equal(args, 'Data berhasil disimpan');
                    }
                    done();
                },
                file: {
                    path: 'test',
                    mimetype: 'text/csv',
                    originalname: 'template.csv'
                }
            };
            underTest.saveAttendance(req, res);
            findAll.restore();
        });
    });
    describe('on hydrate attendance', function() {
        it('should create array with attendance object', function() {
            attendances = [{
                    Employee: {
                        employee_no: 1,
                        name: 'Gentur Waskito',
                        Job: {
                            job_title: 'Software Engineer',
                            Division: {
                                division_name: 'Marketing'
                            }
                        }
                    },
                    in: '08:00:00',
                    out: '18:00:00',
                    absent_date: new Date('2017-04-12T06:15:16Z')
                },
                {
                    Employee: {
                        employee_no: 2,
                        name: 'Brigita Maria',
                        Job: {
                            job_title: 'Public Relation',
                            Division: {
                                division_name: 'Board of Manager'
                            }
                        }
                    },
                    in: '08:00:00',
                    out: '18:00:00',
                    absent_date: new Date('2017-05-12T06:15:16Z')
                },
            ];
            expected = [{
                    id: 1,
                    name: 'Gentur Waskito',
                    job: 'Software Engineer',
                    division: 'Marketing',
                    in: '08:00:00',
                    out: '18:00:00',
                    absent_date: '12-04-2017'
                },
                {
                    id: 2,
                    name: 'Brigita Maria',
                    job: 'Public Relation',
                    division: 'Board of Manager',
                    in: '08:00:00',
                    out: '18:00:00',
                    absent_date: '12-05-2017'
                },
            ];
            hydrate = underTest.__get__('hydrateAttendance');
            result = hydrate(attendances);
            assert.deepEqual(expected, result);
        });
    });
    describe('on dictionarizing employee', function() {
        it('should return a dictionary', function() {
            employees = [{
                    employee_no: 'IT001',
                    id: 1
                },
                {
                    employee_no: 'IT002',
                    id: 2
                },
                {
                    employee_no: 'IT003',
                    id: 3
                }
            ];
            expected = {
                'IT001': 1,
                'IT002': 2,
                'IT003': 3
            };
            dictionarize = underTest.__get__('dictionarizeEmployee');
            result = dictionarize(employees);
            assert.deepEqual(expected, result);
        });
    });
    describe('on saving csv data', function() {
        before(() => {
            streamObj = {
                on(msg, func) {
                    if (msg === 'data') {
                        func({
                            tanggal: '17-05-1996',
                            employee_no: 1,
                            jam_masuk: '08.00',
                            jam_keluar: '16.00'
                        });
                    } else {
                        func();
                    }
                    return this;
                }
            };
            csvFile = {
                pipe(args) {
                    return streamObj;
                }
            }
        });

        beforeEach((done) => {
            createAttendance = sinon.stub(model.Attendance, 'bulkCreate', () => {
                return Promise.resolve([]);
            });
            saveCsvData = underTest.__get__('saveCsvData');
            unlink = sinon.stub(fs, 'unlink');
            done();
        });

        afterEach((done) => {
            createAttendance.restore();
            unlink.restore();
            done();
        });

        it('should create attendance', () => {
            saveCsvData({ flash() {}, file: { path: 'a' } }, {}, {}, csvFile, {});
            assert.isTrue(createAttendance.called);
        });
        it('should catch on receiving invalid data', () => {
            let streamObj = {
                on(msg, func) {
                    if (msg === 'data') {
                        func({});
                    } else {
                        func();
                    }
                    return this;
                }
            };
            let csvFile = {
                pipe(args) {
                    return streamObj;
                }
            };
            let req = {
                flash: sinon.spy(),
                file: { path: 'a' }
            };
            saveCsvData(req, {}, {}, csvFile, {});
            assert.isTrue(req.flash.called);
        });
        it('should delete file upon finish', () => {
            saveCsvData({ flash() {}, file: { path: 'a' } }, {}, {}, csvFile, {});
            assert.isTrue(unlink.called);
        });
        it('should catch on unique constraint error', (done) => {
            createAttendance.restore();
            createAttendance = sinon.stub(model.Attendance, 'bulkCreate', function() {
                return Promise.reject(new Sequelize.UniqueConstraintError);
            });
            let req = {
                flash: sinon.spy(),
                file: { path: 'a' }
            };
            saveCsvData(req, {}, {}, csvFile, {}).then(() => {
                assert.isTrue(req.flash.called);
                done();
            });
        });
        it('should catch on query error', (done) => {
            createAttendance.restore();
            createAttendance = sinon.stub(model.Attendance, 'bulkCreate', function() {
                return Promise.resolve().throw('error');
            });
            let req = {
                flash: sinon.spy(),
                file: { path: 'a' }
            };
            saveCsvData(req, {}, {}, csvFile, {}).then(() => {
                assert.isTrue(req.flash.called);
                done();
            });
        });
    });
    describe('on download csv', function(done) {
        it('should download csv template', function(done) {
            let req = {}
            let res = {
                sendFile: function(view, args) {
                    assert.equal(view, 'template.csv');
                    done();
                }
            }
            underTest.downloadCSV(req, res, function() {});
        })
    })
});