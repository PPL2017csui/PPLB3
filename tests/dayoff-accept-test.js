require('dotenv').config();
var assert = require('chai').assert;
var sinon = require('sinon');
var rewire = require('rewire');
var underTest = rewire('../handlers/dayoff-accept');
var Permit_history = require('../models').Permit_history;
var Employee = require('../models').Employee;
var Promise = require('bluebird');
var Sequelize = require('sequelize');
var DateUtils = require('../utils/date');
let app = rewire('../handlers/dayoff-accept');

describe('dayoff-accept', function() {
    // var loginStub;
    var permitFindStub;
    var employeeFindStub;
    var permitUpdateStub;
    var permitCreateStub;
    var acceptWithNoteStub;
    var acceptWithoutNoteStub;
    var rejectWithNoteStub;
    var acceptWithNote = app.__get__('acceptWithNote');
    var acceptWithoutNote = app.__get__('acceptWithoutNote');
    var rejectWithNote = app.__get__('rejectWithNote');
    var dateStub;
    describe('on view list of dayoff accept page', function(done) {
        describe('should show list of dayoff request', function(done) {
            beforeEach(function(done) {
                permitFindStub = sinon.stub(Permit_history, 'findAll', function() {
                    return Promise.resolve({
                        "id": 2,
                        "start_date": '2017 - 06 - 15 T00: 00: 00.000 Z',
                        "end_date": '2017 - 06 - 15 T00: 00: 00.000 Z',
                        "id_employee": 2,
                        "approval_status": "disetujui",
                        "approval_date": null,
                        "superior_note": null,
                        "request_date": "2017-04-28T00:00:00.000Z",
                        "employee_note": null,
                        "expire_date": "2017-06-21T00:00:00.000Z",
                        "dayoff_type": "sakit",
                        "value": -1,
                        "startoff_date": "21 Juni 2017",
                        "endoff_date": "21 Juni 2017"
                    }, {
                        "id": 2,
                        "start_date": '2017 - 06 - 15 T00: 00: 00.000 Z',
                        "end_date": '2017 - 06 - 15 T00: 00: 00.000 Z',
                        "id_employee": 2,
                        "approval_status": "disetujui",
                        "approval_date": null,
                        "superior_note": null,
                        "request_date": "2017-04-28T00:00:00.000Z",
                        "employee_note": null,
                        "expire_date": "2017-06-21T00:00:00.000Z",
                        "dayoff_type": "sakit",
                        "value": -1,
                        "startoff_date": "21 Juni 2017",
                        "endoff_date": "21 Juni 2017"
                    });
                });

                underTest.viewListDayoff = Promise.promisify(underTest.viewListDayoff);
                done();
            });
            afterEach(function(done) {
                permitFindStub.restore();
                done();
            });
            it('res should render dayoff accept view', function(done) {
                var req = {
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                        } catch (err) {}
                    }
                };
                var res = {
                    locals: {
                        company: '1',
                        user: {
                            id: '1'
                        }
                    },
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'dayoff-accept');
                            done();
                        } catch (err) {}
                    }
                };
                underTest.viewListDayoff(req, res, function() {});
            });
        });
        describe('on data not available, should show error', function(done) {
            beforeEach(function(done) {
                permitFindStub = sinon.stub(Permit_history, 'findAll', function() {
                    return Promise.reject(new Error('test-error'));
                });
                underTest.viewListDayoff = Promise.promisify(underTest.viewListDayoff);
                done();
            })
            afterEach(function(done) {
                permitFindStub.restore();
                done();
            })
            it('req check for flash', function(done) {
                var req = {
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                            done();
                        } catch (err) {}
                    }
                };
                var res = {
                    locals: {
                        company: '1',
                        user: {
                            id: '1'
                        }
                    },
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/dayoff/view');
                        } catch (err) {}
                    }
                };
                underTest.viewListDayoff(req, res, function() {});
            });
            it('res should redirect to dayoff accept page', function(done) {
                var req = {
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                        } catch (err) {}
                    }
                };
                var res = {
                    locals: {
                        company: '1',
                        user: {
                            id: '1'
                        }
                    },
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/dayoff/view');
                            done();
                        } catch (err) {}
                    }
                };
                underTest.viewListDayoff(req, res, function() {});
            });
        });
    });
    describe('on change dayoff request', function(done) {
        beforeEach(function(done) {
            permitFindStub = sinon.stub(Permit_history, 'findOne', function(done) {
                return Promise.resolve({
                    id: 3,
                    day_off_date: '2017 - 06 - 15 T00: 00: 00.000 Z',
                    id_employee: 3,
                    approval_status: 'menunggu persetujuan',
                    approval_date: null,
                    superior_note: null,
                    request_date: '2017 - 04 - 28 T00: 00: 00.000 Z',
                    employee_note: null,
                    expire_date: '2017 - 06 - 18 T00: 00: 00.000 Z',
                    type: 'sakit',
                    value: 9
                });
            });
            underTest.changeDayoff = Promise.promisify(underTest.changeDayoff);
            done();
        });
        afterEach(function(done) {
            permitFindStub.restore();
            done();
        });
        it('should accept dayoff with superior_note', function(done) {
            acceptWithNoteStub = sinon.spy(acceptWithNote);
            let req = {
                flash: function(key, val) {},
                body: {
                    accept: "1",
                    superior_note: "oke"
                }
            };
            let res = {
                redirect: function() {}
            };
            underTest.changeDayoff(req, res, function() {});
            done();
        });
        it('should accept dayoff without superior_note', function(done) {
            acceptWithoutNoteStub = sinon.spy(acceptWithoutNote);
            let req = {
                flash: function(key, val) {},
                body: {
                    accept: "1"
                }
            };
            let res = {
                redirect: function() {}
            };
            underTest.changeDayoff(req, res, function() {});
            done();
        });
        it('or reject day off request', function(done) {
            rejectWithNoteStub = sinon.spy(rejectWithNoteStub);
            let req = {
                flash: function(key, val) {},
                body: {
                    accept: "",
                }
            };
            let res = {
                redirect: function() {}
            };
            underTest.changeDayoff(req, res, function() {});
            done();
        });
    });
    describe('on success viewing detail dayoff page', function() {
        beforeEach(function(done) {
            permitStub = sinon.stub(Permit_history, 'findOne', function() {
                return Promise.resolve({ "id": 1, "start_date": "2017 - 06 - 15 T00: 00: 00.000 Z", "end_date": "2017 - 06 - 15 T00: 00: 00.000 Z", "id_employee": 1, "approval_status": "disetujui", "approval_date": null, "superior_note": null, "request_date": "2017-04-28", "employee_note": null, "expire_date": "2017-06-11", "dayoff_type": "sakit", "value": 10, "Employee": { "id": 1, "employee_no": "1", "password": "pbkdf2$10000$6a5079b2272ecabe695148f738a1dca224dfc39c6400bda4c9a6076db941ee60af1959a60fc0021f0f61ca3e1d6ab1987eb392d5afc7dabee4e5d8568312e055$a170adfc75120f1f8b2643a366b63c4007652563788cf89fc4bca99512b0290f0e2eec305a8b029c0fa699ee3f5f335a3197214559739d4d9759c1dc9eb4be88", "email": "genturwt@gmail.com", "name": "Gentur W.", "join_date": "2016-06-03", "place_of_birth": "Palembang", "date_of_birth": "1996-05-17", "is_admin": true, "retire_date": null, "phone_no": "08918182311", "telp_no": "0218112239", "gender": "male", "address": "Jl. Saharjo no 2, Jakarta Selatan.", "nik": "114375683344", "id_job_title": 1, "salary": 5000000, "token": null, "Job": { "id": 1, "job_title": "Owner", "supervisor": null, "id_division": 1 } } });
            });
            dateStub = sinon.stub(DateUtils, 'dateToUser', function() {
                return Promise.resolve();
            })
            done();
        });

        afterEach(function(done) {
            permitStub.restore();
            dateStub();
            done();
        });
        it('should return detail dayoff page', function(done) {
            var req = {
                params: {
                    id: 2
                }
            };
            var res = {
                locals: {
                    user: {
                        id: '1'
                    },
                    division: '1'
                },
                render: function(view, args) {
                    try {
                        assert.equal(view, 'dayoff-detail');
                        done();
                    } catch (err) {}
                }
            };
            underTest.viewDetailDayoff(req, res, function() {});
        });
    });
    describe('on failed viewing detail dayoff page', function() {
        beforeEach(function(done) {
            permitStub = sinon.stub(Permit_history, 'findOne', function() {
                return Promise.resolve(null);
            });
            done();
        });

        afterEach(function(done) {
            permitStub.restore();
            done();
        });
        it('should return to dayoff accept page', function(done) {
            var req = {
                params: {
                    id: 2
                },
                flash: function(args) {
                    return args;
                }
            };
            var res = {
                locals: {
                    user: {
                        id: '1'
                    }
                },
                redirect: function(args) {
                    try {
                        assert.equal(args, '/dayoff/view');
                        done();
                    } catch (err) {}
                }
            };
            underTest.viewDetailDayoff(req, res, function() {});
        });
    });
    describe('on success update approval with supervisor note', function(done) {
        it('should update its permit - check req', function(done) {
            let permitid = '1';
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Berhasil Diterima');
                        done();
                    } catch (err) {}
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/dayoff/detail/' + permitid);
                    } catch (err) {}
                }
            };
            let permit = {
                id: 3,
                day_off_date: '2017 - 06 - 15 T00: 00: 00.000 Z',
                id_employee: 3,
                approval_status: 'menunggu persetujuan',
                approval_date: null,
                superior_note: null,
                request_date: '2017 - 04 - 28 T00: 00: 00.000 Z',
                employee_note: null,
                expire_date: '2017 - 06 - 18 T00: 00: 00.000 Z',
                type: 'sakit',
                value: 9,
                update: function(args) {
                    return Promise.resolve();
                }
            }
            let sup_note = "oke";
            acceptWithNote(permit, permitid, sup_note, req, res, function() {});
        });
        it('should update its permit - check res', function(done) {
            let permitid = '1';
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Berhasil Diterima');
                    } catch (err) {

                    }
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/dayoff/detail/' + permitid);
                        done();
                    } catch (err) {

                    }
                }
            };
            let permit = {
                id: 3,
                day_off_date: '2017 - 06 - 15 T00: 00: 00.000 Z',
                id_employee: 3,
                approval_status: 'menunggu persetujuan',
                approval_date: null,
                superior_note: null,
                request_date: '2017 - 04 - 28 T00: 00: 00.000 Z',
                employee_note: null,
                expire_date: '2017 - 06 - 18 T00: 00: 00.000 Z',
                type: 'sakit',
                value: 9,
                update: function(args) {
                    return Promise.resolve();
                }
            }
            let sup_note = "oke";
            acceptWithNote(permit, permitid, sup_note, req, res, function() {});
        });
    });
    describe('on success update approval without supervisor note', function(done) {
        it('should update its permit - check req', function(done) {
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Berhasil Diterima');
                        done();
                    } catch (err) {}
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/dayoff/view');
                    } catch (err) {}
                }
            };
            let permit = {
                id: 3,
                day_off_date: '2017 - 06 - 15 T00: 00: 00.000 Z',
                id_employee: 3,
                approval_status: 'menunggu persetujuan',
                approval_date: null,
                superior_note: null,
                request_date: '2017 - 04 - 28 T00: 00: 00.000 Z',
                employee_note: null,
                expire_date: '2017 - 06 - 18 T00: 00: 00.000 Z',
                type: 'sakit',
                value: 9,
                update: function(args) {
                    return Promise.resolve();
                }
            }
            let sup_note = "oke";
            acceptWithoutNote(permit, req, res, function() {});
        });
        it('should update its permit - check res', function(done) {
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Berhasil Diterima');
                    } catch (err) {

                    }
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/dayoff/view');
                        done();
                    } catch (err) {

                    }
                }
            };
            let permit = {
                id: 3,
                day_off_date: '2017 - 06 - 15 T00: 00: 00.000 Z',
                id_employee: 3,
                approval_status: 'menunggu persetujuan',
                approval_date: null,
                superior_note: null,
                request_date: '2017 - 04 - 28 T00: 00: 00.000 Z',
                employee_note: null,
                expire_date: '2017 - 06 - 18 T00: 00: 00.000 Z',
                type: 'sakit',
                value: 9,
                update: function(args) {
                    return Promise.resolve();
                }
            }
            acceptWithoutNote(permit, req, res, function() {});
        });
    });
    describe('on success update reject approval status with supervisor note', function(done) {
        it('should update its permit - check req', function(done) {
            let permitid = '1';
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Berhasil Ditolak');
                        done();
                    } catch (err) {}
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/dayoff/detail/' + permitid);
                    } catch (err) {}
                }
            };
            let permit = {
                id: 3,
                day_off_date: '2017 - 06 - 15 T00: 00: 00.000 Z',
                id_employee: 3,
                approval_status: 'menunggu persetujuan',
                approval_date: null,
                superior_note: null,
                request_date: '2017 - 04 - 28 T00: 00: 00.000 Z',
                employee_note: null,
                expire_date: '2017 - 06 - 18 T00: 00: 00.000 Z',
                type: 'sakit',
                value: 9,
                update: function(args) {
                    return Promise.resolve();
                }
            }
            let sup_note = "oke";
            rejectWithNote(permit, permitid, sup_note, req, res, function() {});
        });
        it('should update its permit - check res', function(done) {
            let permitid = '1';
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Berhasil Ditolak');
                    } catch (err) {

                    }
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/dayoff/detail/' + permitid);
                        done();
                    } catch (err) {

                    }
                }
            };
            let permit = {
                id: 3,
                day_off_date: '2017 - 06 - 15 T00: 00: 00.000 Z',
                id_employee: 3,
                approval_status: 'menunggu persetujuan',
                approval_date: null,
                superior_note: null,
                request_date: '2017 - 04 - 28 T00: 00: 00.000 Z',
                employee_note: null,
                expire_date: '2017 - 06 - 18 T00: 00: 00.000 Z',
                type: 'sakit',
                value: 9,
                update: function(args) {
                    return Promise.resolve();
                }
            }
            let sup_note = "oke";
            rejectWithNote(permit, permitid, sup_note, req, res, function() {});
        });
    });
    describe('on viewing add annual dayoff page', function(done) {
        describe('should show button annual dayoff', function(done) {
            it('flash should contain message', function(done) {
                var req = {
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                            done();
                        } catch (err) {}
                    }
                };
                var res = {
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'dayoff-add');
                        } catch (err) {}
                    }
                };
                underTest.viewAddAnnualDayoff(req, res, function() {});
            });
            it('res should render login page', function(done) {
                var req = {
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                            done();
                        } catch (err) {}
                    }
                };
                var res = {
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'dayoff-add');
                        } catch (err) {}
                    }
                };
                underTest.viewAddAnnualDayoff(req, res, function() {});
            });
        });
    });
    describe('on creating annual dayoff', function(done) {
        beforeEach(function(done) {
            employeeFindStub = sinon.stub(Employee, 'findAll', function() {
                var employees = [{ id: 1 }, { id: 2 }];
                return Promise.resolve(employees);
            })
            permitCreateStub = sinon.stub(Permit_history, 'create', function() {
                return Promise.resolve();
            });
            underTest.addAnnualDayoff = Promise.promisify(underTest.addAnnualDayoff);
            done();
        })
        afterEach(function(done) {
            employeeFindStub.restore();
            permitCreateStub.restore();
            done();
        })
        it('should create dayoff - check req', function(done) {
            var req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Pengajuan cuti dikirim');
                    } catch (err) {}
                }
            };
            var res = {
                locals: {
                    company: '1'
                },
                redirect: function(args) {
                    try {
                        assert.equal(args, '/dayoff/view');
                        done();
                    } catch (err) {}
                }
            };
            underTest.addAnnualDayoff(req, res, function() {});
        });
    });

    describe('on failed creating annual dayoff', function(done) {
        beforeEach(function(done) {
            employeeFindStub = sinon.stub(Employee, 'findAll', function() {
                return Promise.reject(new Error('test-error'));
            })
            permitCreateStub = sinon.stub(Permit_history, 'create', function() {
                return Promise.reject(new Error('test-error'));
            });
            underTest.addAnnualDayoff = Promise.promisify(underTest.addAnnualDayoff);
            done();
        })
        afterEach(function(done) {
            employeeFindStub.restore();
            permitCreateStub.restore();
            done();
        })
        it('should create dayoff - check req', function(done) {
            var req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'test-error');
                    } catch (err) {}
                }
            };
            var res = {
                locals: {
                    company: '1'
                },
                redirect: function(args) {
                    try {
                        assert.equal(args, '/dayoff/add');
                        done();
                    } catch (err) {}
                }
            };
            underTest.addAnnualDayoff(req, res, function() {});
        });
    });

});