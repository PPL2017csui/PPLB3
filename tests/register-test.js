require('dotenv').config();
let assert = require('chai').assert;
var sinon = require('sinon');
var rewire = require('rewire');
var underTest = rewire('../handlers/register');
var expect = require('chai').expect;
var Company = require('../models').Company;
var Division = require('../models').Division;
var Job = require('../models').Job;
var Employee = require('../models').Employee;
var Promise = require('bluebird');
var Sequelize = require('sequelize');

describe('register', function() {

    describe('on viewing register-page', function(done) {
        describe('should show render view', function(done) {
            it('flash should contain message', function(done) {
                var req = {
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                            done();
                        } catch (err) {}
                    }
                };
                var res = {
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'register');
                        } catch (err) {}
                    }
                };
                underTest.viewRegister(req, res, function() {});
            });
            it('res should render register page', function(done) {
                var req = {
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                        } catch (err) {}
                    }
                };
                var res = {
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'register');
                            done();
                        } catch (err) {}
                    }
                };
                underTest.viewRegister(req, res, function() {});
            });
        });

        describe('on save registration data', function(done) {
            var this_email;
            var new_password;
            var id;
            beforeEach(function(done) {
                companyCreateStub = sinon.stub(Company, 'create', function(done) {
                    return Promise.resolve({ id: 1 });
                });
                divisionCreateStub = sinon.stub(Division, 'create', function(done) {
                    return Promise.resolve({ id: 1 });
                });
                jobCreateStub = sinon.stub(Job, 'create', function(done) {
                    return Promise.resolve({ id: 1 });
                });
                employeeCStub = sinon.stub(Employee, 'create', function(done) {
                    return Promise.resolve();
                });
                done();
            });
            afterEach(function(done) {
                companyCreateStub.restore();
                divisionCreateStub.restore();
                jobCreateStub.restore();
                employeeCStub.restore();
                done();
            });

            it('should successfully create company', function(done) {
                var req = {
                    body: {
                        company_name: "nama perusahaan",
                        company_address: "jakarta",
                        company_description: "asd",
                        division_name: "Marketing",
                        job_title: "staff",
                        employee_no: "5",
                        name: "Riscel",
                        email: "risceleliel@yahoo.com",
                        phone_no: "123",
                        telp_no: "01239102",
                        nik: "1234123",
                        address: "menpul",
                        gender: "Wanita",
                        place_of_birth: "jakarta",
                        date_of_birth: "23-08-1996",
                        join_date: "1-10-2009",
                        is_admin: false,
                        salary: "40000000",
                    },
                    flash: function(key, val) {}
                };
                var res = {
                    redirect: function(view) {
                        assert.equal(view, '/login');
                        done();
                    }

                };
                underTest.submitRegister(req, res, function() {});
            });
        });

        describe('on invalid input', function() {
            beforeEach(function(done) {
                companyCreateStub = sinon.stub(Company, 'create', function(done) {
                    return Promise.resolve({ id: 1 });
                });
                divisionCreateStub = sinon.stub(Division, 'create', function(done) {
                    return Promise.resolve({ id: 1 });
                });
                jobCreateStub = sinon.stub(Job, 'create', function(done) {
                    return Promise.resolve({ id: 1 });
                });
                done();
            });
            afterEach(function(done) {
                companyCreateStub.restore();
                divisionCreateStub.restore();
                jobCreateStub.restore();
                done();
            });
            it('should redirect to register page on existing email', function(done) {
                employeeCStub.restore();
                employeeCStub = sinon.stub(Employee, 'create', function() {
                    return Promise.reject(new Sequelize.UniqueConstraintError({
                        fields: {
                            email: 'genturwt@gmail.com'
                        }
                    }));
                });
                var req = {
                    body: {
                        company_name: "nama perusahaan",
                        company_address: "jakarta",
                        company_description: "asd",
                        division_name: "Marketing",
                        job_title: "staff",
                        employee_no: "5",
                        name: "Riscel",
                        email: "risceleliel@yahoo.com",
                        phone_no: "123",
                        telp_no: "01239102",
                        nik: "1234123",
                        address: "menpul",
                        gender: "Wanita",
                        place_of_birth: "jakarta",
                        date_of_birth: "23-08-1996",
                        join_date: "1-10-2009",
                        is_admin: false,
                        salary: "40000000",
                    },
                    flash: function(key, val) {}
                };
                var res = {
                    send: function(args) {},
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/register');
                            done();
                        } catch (err) {

                        }
                    }
                };
                underTest.submitRegister(req, res, function() {});

            });
            it('should redirect to register page on existing nik', function(done) {
                employeeCStub.restore();
                employeeCStub = sinon.stub(Employee, 'create', function() {
                    return Promise.reject(new Sequelize.UniqueConstraintError({
                        fields: {
                            nik: '1406527620'
                        }
                    }));
                });
                var req = {
                    body: {
                        company_name: "nama perusahaan",
                        company_address: "jakarta",
                        company_description: "asd",
                        division_name: "Marketing",
                        job_title: "staff",
                        employee_no: "5",
                        name: "Riscel",
                        email: "risceleliel@yahoo.com",
                        phone_no: "123",
                        telp_no: "01239102",
                        nik: "1234123",
                        address: "menpul",
                        gender: "Wanita",
                        place_of_birth: "jakarta",
                        date_of_birth: "23-08-1996",
                        join_date: "1-10-2009",
                        is_admin: false,
                        salary: "40000000",
                    },
                    flash: function(key, val) {}
                };
                var res = {
                    send: function(args) {},
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/register');
                            done();
                        } catch (err) {

                        }
                    }
                };
                underTest.submitRegister(req, res, function() {});

            });
            it('should redirect to register page on existing employee_no', function(done) {
                employeeCStub.restore();
                employeeCStub = sinon.stub(Employee, 'create', function() {
                    return Promise.reject(new Sequelize.UniqueConstraintError({
                        fields: {
                            employee_no: '123412320'
                        }
                    }));
                });
                var req = {
                    body: {

                        company_name: "nama perusahaan",
                        company_address: "jakarta",
                        company_description: "asd",
                        division_name: "Marketing",
                        job_title: "staff",
                        employee_no: "5",
                        name: "Riscel",
                        email: "risceleliel@yahoo.com",
                        phone_no: "123",
                        telp_no: "01239102",
                        nik: "1234123",
                        address: "menpul",
                        gender: "Wanita",
                        place_of_birth: "jakarta",
                        date_of_birth: "23-08-1996",
                        join_date: "1-10-2009",
                        is_admin: false,
                        salary: "40000000",
                    },
                    flash: function(key, val) {}
                };
                var res = {
                    send: function(args) {},
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/register');
                            done();
                        } catch (err) {

                        }
                    }
                };
                underTest.submitRegister(req, res, function() {});

            });
            it('should redirect to register page on existing phone', function(done) {
                employeeCStub.restore();
                employeeCStub = sinon.stub(Employee, 'create', function() {
                    return Promise.reject(new Sequelize.UniqueConstraintError({
                        fields: {
                            phone: '087835520315'
                        }
                    }));
                });
                var req = {
                    body: {
                        company_name: "nama perusahaan",
                        company_address: "jakarta",
                        company_description: "asd",
                        division_name: "Marketing",
                        job_title: "staff",
                        employee_no: "5",
                        name: "Riscel",
                        email: "risceleliel@yahoo.com",
                        phone_no: "123",
                        telp_no: "01239102",
                        nik: "1234123",
                        address: "menpul",
                        gender: "Wanita",
                        place_of_birth: "jakarta",
                        date_of_birth: "23-08-1996",
                        join_date: "1-10-2009",
                        is_admin: false,
                        salary: "40000000",
                    },
                    flash: function(key, val) {}
                };
                var res = {
                    send: function(args) {},
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/register');
                            done();
                        } catch (err) {

                        }
                    }
                };
                underTest.submitRegister(req, res, function() {});

            });
            it('should redirect to register page on existing company name', function(done) {
                companyCreateStub.restore();
                companyCreateStub = sinon.stub(Company, 'create', function() {
                    return Promise.reject(new Sequelize.UniqueConstraintError({
                        fields: {
                            company_name: '087835520315'
                        }
                    }));
                });
                var req = {
                    body: {
                        company_name: "nama perusahaan",
                        company_address: "jakarta",
                        company_description: "asd",
                        division_name: "Marketing",
                        job_title: "staff",
                        employee_no: "5",
                        name: "Riscel",
                        email: "risceleliel@yahoo.com",
                        phone_no: "123",
                        telp_no: "01239102",
                        nik: "1234123",
                        address: "menpul",
                        gender: "Wanita",
                        place_of_birth: "jakarta",
                        date_of_birth: "23-08-1996",
                        join_date: "1-10-2009",
                        is_admin: false,
                        salary: "40000000",
                    },
                    flash: function(key, val) {}
                };
                var res = {
                    send: function(args) {},
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/register');
                            done();
                        } catch (err) {

                        }
                    }
                };
                underTest.submitRegister(req, res, function() {});

            });
            it('should redirect to register page on missing value', function(done) {
                employeeCStub.restore();
                employeeCStub = sinon.stub(Employee, 'create', function() {
                    return Promise.resolve(5).throw(new Error());
                });
                var req = {
                    body: {
                        company_name: "nama perusahaan",
                        company_address: "jakarta",
                        company_description: "asd",
                        division_name: "Marketing",
                        job_title: "staff",
                        employee_no: "5",
                        name: "Riscel",
                        email: "risceleliel@yahoo.com",
                        phone_no: "123",
                        telp_no: "01239102",
                        nik: "1234123",
                        address: "menpul",
                        gender: "Wanita",
                        place_of_birth: "jakarta",
                        date_of_birth: "23-08-1996",
                        join_date: "1-10-2009",
                        is_admin: false,
                        salary: "40000000",
                    },
                    flash: function(key, val) {}
                };
                var res = {
                    send: function(args) {},
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/register');
                            done();
                        } catch (err) {

                        }
                    }
                };
                underTest.submitRegister(req, res, function() {});
            });

        });
    });
});