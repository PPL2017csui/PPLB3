require('dotenv').config();
let assert = require('chai').assert;
let expect = require('chai').expect;
let sinon = require('sinon');
let rewire = require('rewire');
let underTest = require('../handlers/job');
let Job = require('../models').Job;
let Division = require('../models').Division;
let Promise = require('bluebird');
let Sequelize = require('sequelize');
let app = rewire('../handlers/job');


describe('job', function() {
    let findSupervisorId = app.__get__('findSupervisorId');
    let createJob = app.__get__('createJob');
    let createJobWithoutSp = app.__get__('createJobWithoutSp');

    let divisionFindStub;
    let jobCreateStub;
    let jobFindStub;
    let findSupervisorStub;
    let createJobStub;

    before(function() {
        consoles = sinon.stub(console, 'error');
    });

    after(function() {
        consoles.restore();
    });

    describe('on view create jobs', function(done) {
        describe('should show render view', function(done) {
            it('flash should contain message', function(done) {
                let req = {
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                            done();
                        } catch (err) {}
                    }
                };
                let res = {
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'job-create')
                        } catch (err) {}
                    }
                };
                underTest.viewCreateJob(req, res, function() {});
            });
            it('res should render job-create view', function(done) {
                let req = {
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                            done();
                        } catch (err) {}
                    }
                };
                let res = {
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'job-create')
                        } catch (err) {}
                    }
                };
                underTest.viewCreateJob(req, res, function() {});
            });
        });
    });

    describe('on view list jobs', function(done) {
        describe('should show list jobs', function(done) {
            beforeEach(function(done) {
                jobFindStub = sinon.stub(Job, 'findAll', function() {
                    return Promise.resolve();
                });
                underTest.viewListJob = Promise.promisify(underTest.viewListJob);
                done();
            })
            afterEach(function(done) {
                jobFindStub.restore();
                done();
            })
            it('res should render job-view with object jobs', function(done) {
                let req = {};
                let res = {
                    render: function(view) {
                        try {
                            assert.equal(view, 'job-view');
                            done();
                        } catch (err) {}
                    },
                    locals: {
                        company: 1
                    }
                };
                underTest.viewListJob(req, res, function() {});
            });
        });
        describe('on data not available, should show error', function(done) {
            beforeEach(function(done) {
                jobFindStub = sinon.stub(Job, 'findAll', function() {
                    return Promise.reject(new Error('test-error'));
                });
                underTest.viewListJob = Promise.promisify(underTest.viewListJob);
                done();
            })
            afterEach(function(done) {
                jobFindStub.restore();
                done();
            })
            it('res should render err', function(done) {
                let req = {};
                let res = {
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'job-view');
                            done();
                        } catch (err) {}
                    },
                    locals: {
                        company: 1
                    },
                };
                underTest.viewListJob(req, res, function() {});
            });
        });
    });

    describe('on get supervisor JSON', function(done) {
        beforeEach(function(done) {
            jobFindStub = sinon.stub(Job, 'findAll', function() {
                return Promise.resolve({ result: [{ job_title: 'CEO' }, { job_title: 'CTO' }] });
            });
            underTest.getJobTitle = Promise.promisify(underTest.getJobTitle);
            done();
        });

        afterEach(function(done) {
            jobFindStub.restore();
            done();
        });
        it('should return JSON file', function(done) {
            let req = { query: {} };
            let res = {
                json: function(args) {
                    try {
                        assert.deepEqual(args, { result: [{ job_title: 'CEO' }, { job_title: 'CTO' }] });
                        done();
                    } catch (err) {}
                },
                locals: {
                    company: 1
                }
            };
            underTest.getJobTitle(req, res, function() {});
        });
    });

    describe('on error when get supervisor JSON', function(done) {
        beforeEach(function(done) {
            jobFindStub = sinon.stub(Job, 'findAll', function() {
                return Promise.reject(new Error('test-error'));
            });
            underTest.getJobTitle = Promise.promisify(underTest.getJobTitle);
            done();
        });

        afterEach(function(done) {
            jobFindStub.restore();
            done();
        });
        it('should return error message', function(done) {
            let req = { query: {} };
            let res = {
                send: function(args) {
                    try {
                        assert.equal(args, 'Error: test-error');
                        done();
                    } catch (err) {}
                },
                locals: {
                    company: 1
                }
            };
            underTest.getJobTitle(req, res, function() {});
        });
    });

    describe('on function saveCreateJob called', function(done) {
        beforeEach(function(done) {
            divisionFindStub = sinon.stub(Division, 'findAll', function(done) {
                return Promise.resolve();
            });
            underTest.saveCreateJob = Promise.promisify(underTest.saveCreateJob);
            done();
        });
        afterEach(function(done) {
            divisionFindStub.restore();
            done();
        });
        it('should find id of supervisor if supervisor provided', function(done) {
            findSupervisorStub = sinon.spy(findSupervisorId);
            let req = {
                flash: function(key, val) {},
                body: {
                    job_title: "Manager",
                    supervisor: "CEO"
                }
            };
            let res = {
                redirect: function() {}
            };
            underTest.saveCreateJob(req, res, function() {});
            done();
        });
        it('or create job without supervisor if not provided', function(done) {
            createJobStub = sinon.spy(createJobWithoutSp);
            let req = {
                flash: function(key, val) {},
                body: {
                    job_title: "CEO",
                }
            };
            let res = {
                redirect: function() {}
            };
            underTest.saveCreateJob(req, res, function() {});
            done();
        });
        it('if job_title not provided, set as null', function(done) {
            createJobStub = sinon.spy(createJobWithoutSp);
            let req = {
                flash: function(key, val) {},
                body: {
                    job_title: '',
                }
            };
            let res = {
                redirect: function() {}
            };
            underTest.saveCreateJob(req, res, function() {});
            done();
        });
    });

    describe('on success finding supervisor id', function(done) {
        beforeEach(function(done) {
            jobFindStub = sinon.stub(Job, 'findAll', function(done) {
                return Promise.resolve();
            });
            findSupervisorId = Promise.promisify(findSupervisorId);
            done();
        });
        afterEach(function(done) {
            jobFindStub.restore();
            done();
        });
        it('should call createJob', function(done) {
            let job_title = "Manager";
            let division_id = 1;
            let id = 1;
            let req = {
                flash: function(key, val) {},
            }
            let res = {
                redirect: function() {}
            };
            findSupervisorId(division_id, job_title, id, req, res, function() {});
            done();
        });
    });

    describe('on failed finding supervisor id', function(done) {
        beforeEach(function(done) {
            jobFindStub = sinon.stub(Job, 'findAll', function(done) {
                return Promise.reject(new Error('test-error'));
            });
            findSupervisorId = Promise.promisify(findSupervisorId);
            done();
        });
        afterEach(function(done) {
            jobFindStub.restore();
            done();
        });
        it('should show error message - req check', function(done) {
            let job_title = "Manager";
            let division_id = 1;
            let id = 1;
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Atasan tidak ditemukan');
                        done();
                    } catch (err) {}
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/job/create');
                    } catch (err) {}
                }
            };
            findSupervisorId(division_id, job_title, id, req, res, function() {});
        });
        it('should show error message - res check', function(done) {
            let job_title = "Manager";
            let division_id = 1;
            let id = 1;
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Atasan tidak ditemukan');
                    } catch (err) {}
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/job/create');
                        done();
                    } catch (err) {}
                }
            };
            findSupervisorId(division_id, job_title, id, req, res, function() {});
        });
    });

    describe('on success creating job with supervisor', function(done) {
        beforeEach(function(done) {
            jobCreateStub = sinon.stub(Job, 'create', function() {
                return Promise.resolve();
            });
            createJob = Promise.promisify(createJob);
            done();
        });
        afterEach(function(done) {
            jobCreateStub.restore();
            done();
        });
        it('should create job with provided supervisor - check req', function(done) {
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Jabatan dengan atasan berhasil dibuat');
                        done();
                    } catch (err) {}
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/job/create');
                    } catch (err) {}
                }
            };
            let job_title = "CEO";
            let supervisor_id = 1;
            let division_id = 1;
            createJob(division_id, job_title, supervisor_id, req, res, function() {});
        });
        it('should create job with provided supervisor - check res', function(done) {
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Jabatan dengan atasan berhasil dibuat');
                    } catch (err) {}
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/job/create');
                        done();
                    } catch (err) {}
                }
            };
            let job_title = "CEO";
            let supervisor_id = 1;
            let division_id = 1;
            createJob(division_id, job_title, supervisor_id, req, res, function() {});
        });
    });

    describe('on failed creating job with supervisor - error in data', function(done) {
        beforeEach(function(done) {
            jobCreateStub = sinon.stub(Job, 'create', function() {
                return Promise.reject(new Error('test-error'));
            });
            createJob = Promise.promisify(createJob);
            done();
        });
        afterEach(function(done) {
            jobCreateStub.restore();
            done();
        });
        it('should failed create job with provided supervisor - check req', function(done) {
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Pastikan data atasan yang Anda masukkan sudah benar');
                        done();
                    } catch (err) {}
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/job/create');
                    } catch (err) {}
                }
            };
            let job_title = "CEO";
            let supervisor_id = -1;
            let division_id = 1;
            createJob(division_id, job_title, supervisor_id, req, res, function() {});
        });
        it('should failed create job with provided supervisor - check res', function(done) {
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Pastikan data atasan yang Anda masukkan sudah benar');
                    } catch (err) {}
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/job/create');
                        done();
                    } catch (err) {}
                }
            };
            let job_title = "CEO";
            let supervisor_id = -1;
            let division_id = 1;
            createJob(division_id, job_title, supervisor_id, req, res, function() {});
        });
    });

    describe('on failed creating job with supervisor - error in job', function(done) {
        beforeEach(function(done) {
            jobCreateStub = sinon.stub(Job, 'create', function() {
                return Promise.reject(new Sequelize.UniqueConstraintError());
            });
            createJob = Promise.promisify(createJob);
            done();
        });
        afterEach(function(done) {
            jobCreateStub.restore();
            done();
        });
        it('should failed create job with provided supervisor - check req', function(done) {
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Nama jabatan sudah pernah dipakai');
                        done();
                    } catch (err) {}
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/job/create');
                    } catch (err) {}
                }
            };
            let job_title = "CEO";
            let supervisor_id = -1;
            let division_id = 1;
            createJob(division_id, job_title, supervisor_id, req, res, function() {});
        });
        it('should failed create job with provided supervisor - check res', function(done) {
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Nama jabatan sudah pernah dipakai');
                    } catch (err) {}
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/job/create');
                        done();
                    } catch (err) {}
                }
            };
            let job_title = "CEO";
            let supervisor_id = -1;
            let division_id = 1;
            createJob(division_id, job_title, supervisor_id, req, res, function() {});
        });
    });

    describe('on success creating job without supervisor', function(done) {
        beforeEach(function(done) {
            jobCreateStub = sinon.stub(Job, 'create', function() {
                return Promise.resolve();
            });
            createJobWithoutSp = Promise.promisify(createJobWithoutSp);
            done();
        });
        afterEach(function(done) {
            jobCreateStub.restore();
            done();
        });
        it('should create job - check req', function(done) {
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Jabatan berhasil dibuat');
                        done();
                    } catch (err) {}
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/job/create');
                    } catch (err) {}
                }
            };
            let job_title = "CTO";
            let division_id = 1;
            createJobWithoutSp(division_id, job_title, req, res, function() {});
        });
        it('should create job with provided supervisor - check res', function(done) {
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Jabatan berhasil dibuat');
                    } catch (err) {

                    }
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/job/create');
                        done();
                    } catch (err) {

                    }
                }
            };
            let job_title = "CTO";
            let division_id = 1;
            createJobWithoutSp(division_id, job_title, req, res, function() {});
        });
    });

    describe('on failed creating job without supervisor - error in data', function(done) {
        beforeEach(function(done) {
            jobCreateStub = sinon.stub(Job, 'create', function() {
                return Promise.reject(new Error('test-error'));
            });
            createJobWithoutSp = Promise.promisify(createJobWithoutSp);
            done();
        });
        afterEach(function(done) {
            jobCreateStub.restore();
            done();
        });
        it('should failed create job - check req', function(done) {
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Pastikan data atasan yang Anda masukkan sudah benar');
                        done();
                    } catch (err) {

                    }
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/job/create');
                    } catch (err) {

                    }
                }
            };
            let job_title = "CTO";
            let division_id = 1;
            createJobWithoutSp(division_id, job_title, req, res, function() {});
        });
        it('should failed create job - check res', function(done) {
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Pastikan data atasan yang Anda masukkan sudah benar');
                    } catch (err) {

                    }
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/job/create');
                        done();
                    } catch (err) {

                    }
                }
            };
            let job_title = "CTO";
            let division_id = 1;
            createJobWithoutSp(division_id, job_title, req, res, function() {});
        });
    });
    describe('on failed creating job without supervisor - error in job', function(done) {
        beforeEach(function(done) {
            jobCreateStub = sinon.stub(Job, 'create', function() {
                return Promise.reject(new Sequelize.UniqueConstraintError());
            });
            createJobWithoutSp = Promise.promisify(createJobWithoutSp);
            done();
        });
        afterEach(function(done) {
            jobCreateStub.restore();
            done();
        });
        it('should failed create job - check req', function(done) {
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Nama jabatan sudah pernah dipakai');
                        done();
                    } catch (err) {}
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/job/create');
                    } catch (err) {}
                }
            };
            let job_title = "CTO";
            let division_id = 1;
            createJobWithoutSp(division_id, job_title, req, res, function() {});
        });
        it('should failed create job - check res', function(done) {
            let req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Nama jabatan sudah pernah dipakai');
                    } catch (err) {

                    }
                }
            };
            let res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/job/create');
                        done();
                    } catch (err) {

                    }
                }
            };
            let job_title = "CTO";
            let division_id = 1;
            createJobWithoutSp(division_id, job_title, req, res, function() {});
        });
    });
});

describe('job', function() {
    let jobFindStub;

    describe('on get supervisor JSON', function(done) {
        beforeEach(function() {
            jobFindStub = sinon.stub(Job, 'findAll', function() {
                return Promise.resolve({ result: [{ job_title: 'CEO' }, { job_title: 'CTO' }] });
            });
            underTest.getJobTitle = Promise.promisify(underTest.getJobTitle);
        });

        afterEach(function() {
            jobFindStub.restore();
        });

        it('should return JSON file', function(done) {
            let req = {
                query: {
                    selectedDivision: 'test'
                }
            };
            let res = {
                json: function(args) {
                    assert.deepEqual(args, { result: [{ job_title: 'CEO' }, { job_title: 'CTO' }] });
                    done();
                },
                locals: {
                    company: 1
                }
            };
            underTest.getJobTitle(req, res, function() {});
        });
    });
    describe('on error when get supervisor JSON', function(done) {
        beforeEach(function() {
            jobFindStub = sinon.stub(Job, 'findAll', function() {
                return Promise.reject(new Error('test-error'));
            });
            underTest.getJobTitle = Promise.promisify(underTest.getJobTitle);
        });

        afterEach(function() {
            jobFindStub.restore();
        });
        it('should return JSON file', function(done) {
            let req = {
                query: {
                    selectedDivision: 'test'
                }
            };
            let res = {
                send: function(args) {
                    assert.equal(args, 'Error: test-error');
                    done();
                },
                locals: {
                    company: 1
                }

            };
            underTest.getJobTitle(req, res, function() {});
        });
    });
    describe('on function called without query', () => {
        beforeEach(function() {
            jobFindStub = sinon.stub(Job, 'findAll', function() {
                return Promise.resolve({ result: [{ job_title: 'CEO' }, { job_title: 'CTO' }] });
            });
            underTest.getJobTitle = Promise.promisify(underTest.getJobTitle);
        });

        afterEach(function() {
            jobFindStub.restore();
        });

        it('should return all job title', (done) => {
            let req = { query: {} };
            let res = {
                json: function(args) {
                    assert.deepEqual(args, { result: [{ job_title: 'CEO' }, { job_title: 'CTO' }] });
                    done();
                },
                locals: {
                    company: 1
                }
            };
            underTest.getJobTitle(req, res, function() {});
        });
    });
    describe('on error without query', function(done) {
        beforeEach(function() {
            jobFindStub = sinon.stub(Job, 'findAll', function() {
                return Promise.reject(new Error('test-error'));
            });
            underTest.getJobTitle = Promise.promisify(underTest.getJobTitle);
        });

        afterEach(function() {
            jobFindStub.restore();
        });
        it('should return JSON file', function(done) {
            let req = {
                query: {}
            };
            let res = {
                send: function(args) {
                    assert.equal(args, 'Error: test-error');
                    done();
                },
                locals: {
                    company: 1
                }
            };
            underTest.getJobTitle(req, res, function() {});
        });
    });
});