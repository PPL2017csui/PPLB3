require('dotenv').config();
let assert = require('chai').assert;
var sinon = require('sinon');
var Employee = require('../models').Employee;
var rewire = require('rewire');
var Promise = require('bluebird');
var underTest = rewire('../handlers/password');

describe('set password', function() {
    var passwordCreateStub;
    var tokenCreateStub;
    let redirectStub;
    var employeeStub;
    describe('on token not valid', function(done) {
        beforeEach(function(done) {
            tokenCreateStub = sinon.stub(Employee, 'findOne', function(done) {
                return Promise.resolve(null);
            })
            underTest.viewSetPassword = Promise.promisify(underTest.viewSetPassword);
            done();
        });
        afterEach(function(done) {
            tokenCreateStub.restore();
            done();
        });

        it('should show flash message', function(done) {
            var req = {
                query: {
                    token: "1238bshajnf83yd7"
                },
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Silahkan login');
                        done();
                    } catch (err) {}
                }
            };
            var res = {
                redirect: (args) => {
                    assert.equal(args, '/login');
                },
            };
            underTest.viewSetPassword(req, res, function() {});
        });

        it('should redirect to login page on token not exist', function(done) {
            var req = {
                query: {
                    token: "1238bshajnf83yd7"
                },
                flash: function(args) {
                    try {
                        assert.equal(args, 'message');
                    } catch (err) {}
                }
            };
            var res = {
                redirect: (args) => {
                    assert.equal(args, '/login');
                },
            };
            redirectStub = sinon.spy(res.redirect);
            underTest.viewSetPassword(req, res, function() {});
            done();
        });
        describe('on token valid', function(done) {
            beforeEach(function(done) {
                tokenCreateStub.restore();
                tokenCreateStub = sinon.stub(Employee, 'findOne', function(done) {
                    return Promise.resolve({
                        id: 7,
                        employee_no: '7',
                        password: 'pplcermaticeria',
                        email: 'muthyafi@gmail.com',
                        name: 'Muthy Afifah',
                        join_date: '2016 - 10 - 04 T00: 00: 00.000 Z',
                        place_of_birth: 'Jakarta',
                        date_of_birth: '1996 - 10 - 14 T00: 00: 00.000 Z',
                        phone: null,
                        gender: 'female',
                        is_admin: true,
                        salary: null,
                        address: null,
                        nik: '155465657474',
                        retire_date: null
                    });
                })
                underTest.viewSetPassword = Promise.promisify(underTest.viewSetPassword);
                done();
            });
            afterEach(function(done) {
                tokenCreateStub.restore();
                done();
            });

            it('should render set-password handlebar', function(done) {
                var req = {
                    query: {
                        token: "1238bshajnf83yd7"
                    },
                    flash: function(msg, args) {}
                };
                var res = {
                    render: function(view, args) {
                        assert.equal(view, 'set-password');
                        done();
                    }
                };
                underTest.viewSetPassword(req, res, function() {});
            });
            it('should show flash message', function(done) {
                var req = {
                    query: {
                        token: "1238bshajnf83yd7"
                    },
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                            done();
                        } catch (err) {}
                    }
                };
                var res = {
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'set-password');
                        } catch (err) {}
                    }
                };
                underTest.viewSetPassword(req, res, function() {});
            });

        });
    });
    describe('on change password', function(done) {
        var this_email;
        var new_password;
        beforeEach(function(done) {
            passwordCreateStub = sinon.stub(Employee, 'find', function(done) {
                return Promise.resolve({
                    updateAttributes: () => {}
                });
            })
            underTest.savePassword = Promise.promisify(underTest.savePassword);
            done();
        });
        afterEach(function(done) {
            passwordCreateStub.restore();
            done();
        });

        it('should show flash message on success', function(done) {
            let req = {
                body: {
                    new_password: 'password',
                },
                flash: function(msg, args) {
                    assert.equal(args, 'Password berhasil diubah');
                    done();
                },
                query: {
                    token: "1238bshajnf83yd7"
                }
            };
            let res = {
                redirect: (args) => {
                    assert.equal(args, '/login');
                }
            };
            underTest.savePassword(req, res, function() {});
        });
        it('should redirect to login page on success', function(done) {
            let req = {
                body: {
                    new_password: 'password',
                },
                flash: function(msg, args) {
                    assert.equal(args, 'Password berhasil diubah');
                },
                query: {
                    token: "1238bshajnf83yd7"
                }
            };
            let res = {
                redirect: (args) => {
                    assert.equal(args, '/login');
                    done();
                }
            };
            underTest.savePassword(req, res, function() {});
        });
    });


    describe('on failed set password', function(done) {
        var id_company;
        var division_name;
        beforeEach(function(done) {
            passwordCreateStub = sinon.stub(Employee, 'find', function() {
                return Promise.reject(
                    new Error('test-error'));
            });
            underTest.savePassword = Promise.promisify(underTest.savePassword);
            done();
        });
        afterEach(function(done) {
            passwordCreateStub.restore();
            done();
        });

        it('should show flash message on failed', function(done) {
            let req = {
                body: {
                    new_password: 'password',
                },
                flash: function(msg, args) {
                    assert.equal(args, 'Pengubahan password gagal');
                    done();
                },
                query: {
                    token: "1238bshajnf83yd7"
                }
            };
            let res = {
                redirect: (args) => {
                    assert.equal(args, '/set-password');
                }
            };
            underTest.savePassword(req, res, function() {});
        });
        it('should redirect to set-password page on failed', function(done) {
            let req = {
                body: {
                    new_password: 'password',
                },
                flash: function(msg, args) {
                    assert.equal(args, 'Pengubahan password gagal');
                },
                query: {
                    token: "1238bshajnf83yd7"
                }
            };
            let res = {
                redirect: (args) => {
                    assert.equal(args, '/set-password');
                    done();
                }
            };
            underTest.savePassword(req, res, function() {});
        });
    });
});