require('dotenv').config();
var assert = require('chai').assert;
var sinon = require('sinon');
var rewire = require('rewire');
var underTest = rewire('../handlers/dayoff');
var Permit_history = require('../models').Permit_history;
var Promise = require('bluebird');
var Sequelize = require('sequelize');
var sequelize = new Sequelize();

var app = rewire('../handlers/dayoff');

describe('dayoff', function() {
    var getValue = app.__get__('getValue');
    var dayOffFindStub;
    var dayOffCreateStub;
    var getValueStub;

    describe('on view create dayoff', function(done) {
        describe('should show render view when value exists', function(done) {
            beforeEach(function(done) {
                getValue = underTest.__set__('getValue', () => {
                    return Promise.resolve(5);
                });
                done();
            });
            afterEach(function(done) {
                getValue();
                done();
            });
            it('flash should contain message', function(done) {
                var req = {
                    session: {
                        user: {
                            id: '2'
                        }
                    },
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                            done();
                        } catch (err) {}
                    }
                };
                var res = {
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'employee-request-day-off');
                        } catch (err) {}
                    }
                };
                underTest.viewCreateDayOff(req, res, function() {});
            });
            it('res should render request dayoff view', function(done) {
                var req = {
                    session: {
                        user: {
                            id: '2'
                        }
                    },
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                            done();
                        } catch (err) {}
                    }
                };
                var res = {
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'employee-request-day-off');
                        } catch (err) {}
                    }
                };
                underTest.viewCreateDayOff(req, res, function() {});
            });
        });
        describe('should show render view when value not exist', function(done) {
            beforeEach(function(done) {
                getValue = underTest.__set__('getValue', () => {
                    return Promise.resolve(0);
                });
                done();
            });
            afterEach(function(done) {
                getValue();
                done();
            });
            it('res should render request dayoff view', function(done) {
                var req = {
                    session: {
                        user: {
                            id: '2'
                        }
                    },
                };
                var res = {
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'employee-request-day-off');
                            done();
                        } catch (err) {}
                    }
                };
                underTest.viewCreateDayOff(req, res, function() {});
            });
        });
    });

    describe('on view dayoff status', function(done) {
        describe('should show list of dayoff status', function(done) {
            beforeEach(function(done) {
                dayOffFindStub = sinon.stub(Permit_history, 'findAll', function() {
                    return Promise.resolve();
                });
                underTest.viewDayOffStatus = Promise.promisify(underTest.viewDayOffStatus);
                done();
            })
            afterEach(function(done) {
                dayOffFindStub.restore();
                done();
            })
            it('res should render dayoff status view', function(done) {
                var req = {
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                        } catch (err) {}
                    }
                };
                var res = {
                    locals: {
                        user: {
                            id: '2'
                        }
                    },
                    render: function(view, args) {
                        try {
                            assert.equal(view, 'employee-view-day-off');
                            done();
                        } catch (err) {}
                    }
                };
                underTest.viewDayOffStatus(req, res, function() {});
            });
        });
        describe('on data not available, should show error', function(done) {
            beforeEach(function(done) {
                dayOffFindStub = sinon.stub(Permit_history, 'findAll', function() {
                    return Promise.reject(new Error('test-error'));
                });
                underTest.viewDayOffStatus = Promise.promisify(underTest.viewDayOffStatus);
                done();
            })
            afterEach(function(done) {
                dayOffFindStub.restore();
                done();
            })
            it('check for flash', function(done) {
                var req = {
                    flash: function(args) {
                        return ['bri'];
                    }
                };
                var res = {
                    locals: {
                        user: {
                            id: '2'
                        }
                    },
                    render: function(args, opts) {
                        try {
                            assert.equal(args, 'employee-view-day-off');
                            assert.deepEqual(opts, { message: ['bri'], cuti: true });
                            done();
                        } catch (err) {}
                    }
                };
                underTest.viewDayOffStatus(req, res, function() {});
            });
            it('res should redirect to dayoff', function(done) {
                var req = {
                    flash: function(args) {
                        try {
                            assert.equal(args, 'message');
                        } catch (err) {}
                    }
                };
                var res = {
                    locals: {
                        user: {
                            id: '2'
                        }
                    },
                    render: function(args) {
                        try {
                            assert.equal(args, 'employee-view-day-off');
                            done();
                        } catch (err) {}
                    }
                };
                underTest.viewDayOffStatus(req, res, function() {});
            });
        });
    });
    describe('on get dates JSON', function(done) {
        beforeEach(function() {
            dayOffFindStub = sinon.stub(Permit_history, 'findAll', function() {
                return Promise.resolve({});
            });
            underTest.getDates = Promise.promisify(underTest.getDates);
        });

        afterEach(function() {
            dayOffFindStub.restore();
        });
        it('should return JSON file', function(done) {
            var req = {
                session: {
                    user: {
                        id: '2'
                    }
                },
            };
            var res = {
                json: function(args) {
                    assert.deepEqual(args, {});
                    done();
                }
            };
            underTest.getDates(req, res, function() {});
        });
    });

    describe('on error when get dates JSON', function(done) {
        beforeEach(function() {
            dayOffFindStub = sinon.stub(Permit_history, 'findAll', function() {
                return Promise.reject(new Error('test-error'));
            });
            underTest.getDates = Promise.promisify(underTest.getDates);
        });

        afterEach(function() {
            dayOffFindStub.restore();
        });
        it('should return JSON file', function(done) {
            var req = {
                session: {
                    user: {
                        id: '2'
                    }
                },
            };
            var res = {
                send: function(args) {
                    assert.equal(args, 'Error: test-error');
                    done();
                }
            };
            underTest.getDates(req, res, function() {});
        });
    });

    describe('on success creating dayoff', function() {
        beforeEach(function(done) {
            dayOffCreateStub = sinon.stub(Permit_history, 'create', function(done) {
                return Promise.resolve();
            })
            getValue = underTest.__set__('getValue', () => {
                return Promise.resolve(5);
            });
            dayOffFindStub = sinon.stub(Permit_history, 'findAll', function(done) {
                return Promise.resolve([]);
            })
            underTest.saveCreateDayOff = Promise.promisify(underTest.saveCreateDayOff);
            done();
        });
        afterEach(function(done) {
            dayOffCreateStub.restore();
            dayOffFindStub.restore();
            getValue();
            done();
        });
        it('should create dayoff - check req', function(done) {
            var req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Pengajuan cuti dikirim');
                        done();
                    } catch (err) {}
                },
                session: {
                    user: {
                        id: '2'
                    }
                },
                body: {
                    day_off_date: '07/07/2017',
                    end_date: '07/08/2017',
                    request_type: 'izin',
                    employee_note: 'Mau libur aja'
                }
            };
            var res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/dayoff/create');
                    } catch (err) {}
                }
            };
            underTest.saveCreateDayOff(req, res, function() {});
        });
        it('should create dayoff - check res', function(done) {
            var req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Pengajuan cuti dikirim');
                    } catch (err) {}
                },
                session: {
                    user: {
                        id: '2'
                    }
                },
                body: {
                    day_off_date: '07/07/2017',
                    end_date: '07/08/2017',
                    request_type: 'izin',
                    employee_note: 'Mau libur aja'
                }
            };
            var res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/dayoff/create');
                        done();
                    } catch (err) {}
                }
            };
            underTest.saveCreateDayOff(req, res, function() {});
        });
    });

    describe('on failed create dayoff if the same request exists', function(done) {
        beforeEach(function(done) {
            dayOffCreateStub = sinon.stub(Permit_history, 'create', function() {
                return Promise.reject(new Error('test-error'));
            });
            getValue = underTest.__set__('getValue', () => {
                return Promise.resolve(5);
            });
            dayOffFindStub = sinon.stub(Permit_history, 'findAll', function(done) {
                return Promise.resolve({ "id": 2, "start_date": "2017-07-12T00:00:00.000Z", "end_date": "2017-07-12T00:00:00.000Z", "id_employee": 2, "approval_status": "disetujui", "approval_date": null, "superior_note": null, "request_date": "2017-04-28T00:00:00.000Z", "employee_note": null, "expire_date": "2017-06-21T00:00:00.000Z", "dayoff_type": "sakit", "value": -1 });
            })
            underTest.saveCreateDayOff = Promise.promisify(underTest.saveCreateDayOff);
            done();
        });
        afterEach(function(done) {
            dayOffCreateStub.restore();
            getValue();
            dayOffFindStub.restore();
            done();
        });
        it('should failed create dayoff - check permit', function(done) {
            var req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Jadwal pengajuan yang diajukan beririsan dengan pengajuan Anda yang lain');
                    } catch (err) {}
                },
                session: {
                    user: {
                        id: '2'
                    }
                },
                body: {
                    day_off_date: '2017-07-12',
                    end_date: '2017-07-12'
                },
            };
            var res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/dayoff/create');
                        done();
                    } catch (err) {}
                }
            };
            underTest.saveCreateDayOff(req, res, function() {});
        });
    });

    describe('on failed create dayoff if exceeding dayoff limit', function(done) {
        beforeEach(function(done) {
            dayOffCreateStub = sinon.stub(Permit_history, 'create', function() {});
            getValue = underTest.__set__('getValue', () => {
                return Promise.resolve(5);
            });
            dayOffFindStub = sinon.stub(Permit_history, 'findAll', function(done) {})
            underTest.saveCreateDayOff = Promise.promisify(underTest.saveCreateDayOff);
            done();
        });
        afterEach(function(done) {
            dayOffCreateStub.restore();
            getValue();
            dayOffFindStub.restore();
            done();
        });
        it('should failed request dayoff - check req', function(done) {
            var req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Maaf, jatah cuti Anda tidak mencukupi');
                        done();
                    } catch (err) {}
                },
                session: {
                    user: {
                        id: '2'
                    }
                },
                body: {
                    day_off_date: '2017-12-31',
                    end_date: '2019-12-31'
                }
            };
            var res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/dayoff/create');
                    } catch (err) {}
                }
            };
            underTest.saveCreateDayOff(req, res, function() {});
        });
        it('should failed request dayoff - check res', function(done) {
            var req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Maaf, jatah cuti Anda tidak mencukupi');
                    } catch (err) {}
                },
                session: {
                    user: {
                        id: '2'
                    }
                },
                body: {
                    day_off_date: '2017-12-31',
                    end_date: '2019-12-31'
                }
            };
            var res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/dayoff/create');
                        done();
                    } catch (err) {}
                }
            };
            underTest.saveCreateDayOff(req, res, function() {});
        });
    });

    describe('on failed create dayoff if date input is empty', function(done) {
        beforeEach(function(done) {
            dayOffCreateStub = sinon.stub(Permit_history, 'create', function() {});
            getValue = underTest.__set__('getValue', () => {
                return Promise.resolve(5);
            });
            underTest.saveCreateDayOff = Promise.promisify(underTest.saveCreateDayOff);
            done();
        });
        afterEach(function(done) {
            dayOffCreateStub.restore();
            getValue();
            done();
        });
        it('should failed request dayoff - check req', function(done) {
            var req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Pastikan data yang Anda masukkan benar');
                        done();
                    } catch (err) {}
                },
                session: {
                    user: {
                        id: '2'
                    }
                },
                body: {
                    day_off_date: '',
                    end_date: ''
                }
            };
            var res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/dayoff/create');
                    } catch (err) {}
                }
            };
            underTest.saveCreateDayOff(req, res, function() {});
        });
        it('should failed request dayoff - check res', function(done) {
            var req = {
                flash: function(key, val) {
                    try {
                        assert.equal(key, 'message');
                        assert.equal(val, 'Pastikan data yang Anda masukkan benar');
                    } catch (err) {}
                },
                session: {
                    user: {
                        id: '2'
                    }
                },
                body: {
                    day_off_date: '',
                    end_date: ''
                }
            };
            var res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/dayoff/create');
                        done();
                    } catch (err) {}
                }
            };
            underTest.saveCreateDayOff(req, res, function() {});
        });
    });

    describe('on success getting value', function(done) {
        beforeEach(function(done) {
            getValueStub = sinon.stub(Permit_history, 'sum', function(done) {
                return Promise.resolve();
            });
            getValue = Promise.promisify(getValue);
            done();
        });
        afterEach(function(done) {
            getValueStub.restore();
            done();
        });
        it('should return sum', function(done) {
            var req = {}
            var res = {};
            var id_employee = 2;
            getValue(req, res, id_employee, function() {});
            done();
        });
    });
});