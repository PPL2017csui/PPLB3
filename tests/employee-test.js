require('dotenv').config();
var assert = require('chai').assert;
var sinon = require('sinon');
var rewire = require('rewire');
var underTest = rewire('../handlers/employee');
var Employee = require('../models').Employee;
var Promise = require('bluebird');
var Sequelize = require('sequelize');
describe('employee', function() {

    before(function() {
        consoles = sinon.stub(console, 'error');
    });

    after(function() {
        consoles.restore();
    });

    beforeEach(function() {
        employeeCStub = sinon.stub(Employee, 'create', function() {
            return Promise.resolve(5);
        });
        underTest.saveCreateEmployee = Promise.promisify(underTest.saveCreateEmployee);

        mailer = underTest.__set__('mailer', {
            send: () => {}
        })
    });

    afterEach(function() {
        employeeCStub.restore();
        mailer();
    });

    describe('on creating employee', function() {
        it('should success on valid input', function(done) {
            var send = sinon.spy(function(args) {
                try {
                    assert.equal('/employee', args);
                    done();
                } catch (err) {

                }
            });
            var req = {
                body: {},
                flash: function(args) {}
            };
            var res = {
                redirect: send
            };
            underTest.saveCreateEmployee(req, res, function() {});
        });
        describe('on invalid input', function() {
            it('should redirect to create page on existing email', function(done) {
                employeeCStub.restore();
                employeeCStub = sinon.stub(Employee, 'create', function() {
                    return Promise.reject(new Sequelize.UniqueConstraintError({
                        fields: {
                            email: 'genturwt@gmail.com'
                        }
                    }));
                });
                var req = {
                    body: {},
                    flash: function(key, val) {}
                };
                var res = {
                    send: function(args) {},
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/employee/create');
                            done();
                        } catch (err) {

                        }
                    }
                };
                underTest.saveCreateEmployee(req, res, function() {});

            });
            it('should redirect to create page on existing nik', function(done) {
                employeeCStub.restore();
                employeeCStub = sinon.stub(Employee, 'create', function() {
                    return Promise.reject(new Sequelize.UniqueConstraintError({
                        fields: {
                            nik: '1406527620'
                        }
                    }));
                });
                var req = {
                    body: {},
                    flash: function(key, val) {}
                };
                var res = {
                    send: function(args) {},
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/employee/create');
                            done();
                        } catch (err) {

                        }
                    }
                };
                underTest.saveCreateEmployee(req, res, function() {});

            });
            it('should redirect to create page on existing email', function(done) {
                employeeCStub.restore();
                employeeCStub = sinon.stub(Employee, 'create', function() {
                    return Promise.reject(new Sequelize.UniqueConstraintError({
                        fields: {
                            employee_no: 'genturwt@gmail.com'
                        }
                    }));
                });
                var req = {
                    body: {},
                    flash: function(key, val) {}
                };
                var res = {
                    send: function(args) {},
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/employee/create');
                            done();
                        } catch (err) {

                        }
                    }
                };
                underTest.saveCreateEmployee(req, res, function() {});

            });
            it('should redirect to create page on existing nik', function(done) {
                employeeCStub.restore();
                employeeCStub = sinon.stub(Employee, 'create', function() {
                    return Promise.reject(new Sequelize.UniqueConstraintError({
                        fields: {
                            phone: '087835520315'
                        }
                    }));
                });
                var req = {
                    body: {},
                    flash: function(key, val) {}
                };
                var res = {
                    send: function(args) {},
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/employee/create');
                            done();
                        } catch (err) {

                        }
                    }
                };
                underTest.saveCreateEmployee(req, res, function() {});

            });
            it('should redirect to create page on missing value', function(done) {
                employeeCStub.restore();
                employeeCStub = sinon.stub(Employee, 'create', function() {
                    return Promise.resolve(5).throw(new Error());
                });
                var req = {
                    body: {},
                    flash: function(key, val) {}
                };
                var res = {
                    send: function(args) {},
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/employee/create');
                            done();
                        } catch (err) {

                        }
                    }
                };
                underTest.saveCreateEmployee(req, res, function() {});
            });

        });
    });
    describe('on viewing create employee page', function() {
        it('should return create employee page', function(done) {
            var req = {
                flash: function(args) {
                    return args;
                },
                body: {}
            };
            var res = {
                render: function(view, args) {
                    try {
                        assert.equal(view, 'employee-create');
                        assert.deepEqual(args, { message: 'message', karyawan: true });
                        done();
                    } catch (err) {

                    }
                }
            };
            underTest.viewCreateEmployee(req, res, function() {});
        });
    });
    describe('on viewing list employee page', function() {
        it('should return list employee page', function(done) {
            employeeCStub.restore();
            employeeCStub = sinon.stub(Employee, 'findAll', function() {
                return Promise.resolve([{
                    name: 'Gentur'
                }]);
            });
            var req = {
                body: {},
                flash: function(args) {
                    return args;
                }
            };
            var res = {
                locals: {
                    company: 1
                },
                render: function(view, args) {
                    try {
                        assert.equal(view, 'employee-list');
                        assert.deepEqual(args, {
                            employees: [{
                                name: 'Gentur'
                            }],
                            message: 'message',
                            karyawan: true
                        });
                        done();
                    } catch (err) {

                    }
                }
            };
            underTest.viewListEmployee(req, res, function() {});
        });
    });
});