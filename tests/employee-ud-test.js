require('dotenv').config();
var assert = require('chai').assert;
var sinon = require('sinon');
var underTest = require('../handlers/employee');
var Employee = require('../models').Employee;
var Promise = require('bluebird');
var Sequelize = require('sequelize');

describe('employee', function() {
    var employeeStub;
    var throwStub;

    before(function() {
        consoles = sinon.stub(console, 'error');
    });

    after(function() {
        consoles.restore();
    });

    describe('on success viewing detail employee page', function() {
        beforeEach(function(done) {
            employeeStub = sinon.stub(Employee, 'findOne', function() {
                return Promise.resolve([{
                    name: 'Riscel'
                }]);
            });
            done();
        });

        afterEach(function(done) {
            employeeStub.restore();
            done();
        });
        it('should return detail employee page', function(done) {
            var req = {
                params: {
                    id: 6
                }
            };
            var res = {
                render: function(view, args) {
                    try {
                        assert.equal(view, 'employee-detail');
                        done();
                    } catch (err) {}
                },
                locals: {
                    user: {
                        is_admin: 1
                    }
                }
            };
            underTest.viewDetailEmployee(req, res, function() {});
        });
        it('should return employee list page if not admin', function(done) {
            var req = {
                params: {
                    id: 6
                },
                flash: function(args) {
                    return args;
                }
            };
            var res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/employee');
                        done();
                    } catch (err) {}
                },
                locals: {
                    user: {
                        is_admin: 0
                    }
                }
            };
            underTest.viewDetailEmployee(req, res, function() {});
        });
    });
    describe('on failed viewing detail employee page', function() {
        beforeEach(function(done) {
            employeeStub = sinon.stub(Employee, 'findOne', function() {
                return Promise.resolve(null);
            });
            done();
        });

        afterEach(function(done) {
            employeeStub.restore();
            done();
        });
        it('should return list employee page', function(done) {
            var req = {
                params: {
                    id: 6
                },
                flash: function(args) {
                    return args;
                }
            };
            var res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/employee');
                        done();
                    } catch (err) {}
                },
                locals: {
                    user: {
                        is_admin: 1
                    }
                }
            };
            underTest.viewDetailEmployee(req, res, function() {});
        });
    });

    describe('on success viewing update employee page', function() {
        beforeEach(function(done) {
            employeeStub = sinon.stub(Employee, 'findOne', function() {
                return Promise.resolve([{
                    name: 'Riscel'
                }]);
            });
            done();
        });

        afterEach(function(done) {
            employeeStub.restore();
            done();
        });
        it('should return update employee page', function(done) {
            var req = {
                params: {
                    id: 6
                }
            };
            var res = {
                render: function(view, args) {
                    try {
                        assert.equal(view, 'employee-edit');
                        done();
                    } catch (err) {}
                },
                locals: {
                    user: {
                        is_admin: 1
                    }
                }
            };
            underTest.viewUpdateEmployee(req, res, function() {});
        });
        it('should return employee list page if not admin', function(done) {
            var req = {
                params: {
                    id: 6
                },
                flash: function(args) {
                    return args;
                }
            };
            var res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/employee');
                        done();
                    } catch (err) {}
                },
                locals: {
                    user: {
                        is_admin: 0
                    }
                }
            };
            underTest.viewUpdateEmployee(req, res, function() {});
        });
    });
    describe('on failed viewing update employee page', function() {
        beforeEach(function(done) {
            employeeStub = sinon.stub(Employee, 'findOne', function() {
                return Promise.resolve(null);
            });
            done();
        });

        afterEach(function(done) {
            employeeStub.restore();
            done();
        });
        it('should return list employee page', function(done) {
            var req = {
                params: {
                    id: 6
                },
                flash: function(args) {
                    return args;
                }
            };
            var res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/employee');
                        done();
                    } catch (err) {}
                },
                locals: {
                    user: {
                        is_admin: 1
                    }
                }
            };
            underTest.viewUpdateEmployee(req, res, function() {});
        });
    });

    describe('on updating employee', function() {
        it('should success on valid input', function(done) {
            employeeStub.restore();
            employeeStub = sinon.stub(Employee, 'update', function() {
                return Promise.resolve();
            });
            var send = sinon.spy(function(args) {
                try {
                    assert.equal('/employee/detail/6', args);
                    done();
                } catch (err) {

                }
            });

            var req = {
                body: {},
                params: {
                    id: 6
                },
                flash: function(args) {}
            };
            var res = {
                redirect: send,
                locals: {
                    user: {
                        is_admin: 1
                    }
                }
            };

            underTest.saveUpdateEmployee(req, res, function() {});
        });
        it('should failed if not admin', function(done) {
            var send = sinon.spy(function(args) {
                try {
                    assert.equal('/employee', args);
                    done();
                } catch (err) {

                }
            });

            var req = {
                body: {},
                params: {
                    id: 6
                },
                flash: function(args) {}
            };
            var res = {
                redirect: send,
                locals: {
                    user: {
                        is_admin: 0
                    }
                }
            };
            underTest.saveUpdateEmployee(req, res, function() {});
        });
        describe('on invalid input', function() {
            it('should redirect to create page on existing email', function(done) {
                employeeStub.restore();
                employeeStub = sinon.stub(Employee, 'update', function() {
                    return Promise.reject(new Sequelize.UniqueConstraintError({
                        fields: {
                            email: 'genturwt@gmail.com'
                        }
                    }));
                });
                var date_of_birth = {};
                var req = {
                    body: {},
                    params: {
                        id: 6
                    },
                    flash: function(key, val) {}
                };
                var res = {
                    send: function(args) {},
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/employee/edit/6');
                            done();
                        } catch (err) {

                        }
                    },
                    locals: {
                        user: {
                            is_admin: 1
                        }
                    }
                };
                underTest.saveUpdateEmployee(req, res, function() {});

            });
            it('should redirect to create page on existing nik', function(done) {
                employeeStub.restore();
                employeeStub = sinon.stub(Employee, 'update', function() {
                    return Promise.reject(new Sequelize.UniqueConstraintError({
                        fields: {
                            nik: '1406527620'
                        }
                    }));
                });
                var req = {
                    body: {},
                    params: {
                        id: 6
                    },
                    flash: function(key, val) {}
                };
                var res = {
                    send: function(args) {},
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/employee/edit/6');
                            done();
                        } catch (err) {

                        }
                    },
                    locals: {
                        user: {
                            is_admin: 1
                        }
                    }
                };
                underTest.saveUpdateEmployee(req, res, function() {});

            });
            it('should redirect to create page on existing employee no', function(done) {
                employeeStub.restore();
                employeeStub = sinon.stub(Employee, 'update', function() {
                    return Promise.reject(new Sequelize.UniqueConstraintError({
                        fields: {
                            employee_no: 'genturwt@gmail.com'
                        }
                    }));
                });
                var req = {
                    body: {},
                    params: {
                        id: 6
                    },
                    flash: function(key, val) {}
                };
                var res = {
                    send: function(args) {},
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/employee/edit/6');
                            done();
                        } catch (err) {

                        }
                    },
                    locals: {
                        user: {
                            is_admin: 1
                        }
                    }
                };
                underTest.saveUpdateEmployee(req, res, function() {});

            });
            it('should redirect to create page on existing phone', function(done) {
                employeeStub.restore();
                employeeStub = sinon.stub(Employee, 'update', function() {
                    return Promise.reject(new Sequelize.UniqueConstraintError({
                        fields: {
                            phone: '087835520315'
                        }
                    }));
                });
                var req = {
                    body: {},
                    params: {
                        id: 6
                    },
                    flash: function(key, val) {}
                };
                var res = {
                    send: function(args) {},
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/employee/edit/6');
                            done();
                        } catch (err) {

                        }
                    },
                    locals: {
                        user: {
                            is_admin: 1
                        }
                    }
                };
                underTest.saveUpdateEmployee(req, res, function() {});

            });
            it('should redirect to create page on missing value', function(done) {
                employeeStub.restore();
                employeeStub = sinon.stub(Employee, 'update', function() {
                    return Promise.resolve(5).throw(new Error());
                });
                var req = {
                    body: {},
                    params: {
                        id: 6
                    },
                    flash: function(key, val) {}
                };
                var res = {
                    send: function(args) {},
                    redirect: function(args) {
                        try {
                            assert.equal(args, '/employee');
                            done();
                        } catch (err) {

                        }
                    },
                    locals: {
                        user: {
                            is_admin: 1
                        }
                    }
                };
                underTest.saveUpdateEmployee(req, res, function() {});
            });
        });
    });

    describe('on success delete employee', function() {
        beforeEach(function(done) {
            employeeStub = sinon.stub(Employee, 'destroy', function() {
                return Promise.resolve(1);
            });
            done();
        });

        afterEach(function(done) {
            employeeStub.restore();
            done();
        });
        it('should return to list employee', function(done) {
            var req = {
                params: {
                    id: 6
                },
                flash: function(args) {
                    return args;
                }
            };
            var res = {
                redirect: function(view) {
                    try {
                        assert.equal(view, '/employee');
                        done();
                    } catch (err) {}
                },
                locals: {
                    user: {
                        is_admin: 1
                    }
                }
            };
            underTest.deleteEmployee(req, res, function() {});
        });
        it('should return to list employee if not admin', function(done) {
            var req = {
                params: {
                    id: 6
                },
                flash: function(args) {
                    return args;
                }
            };
            var res = {
                redirect: function(view) {
                    try {
                        assert.equal(view, '/employee');
                        done();
                    } catch (err) {}
                },
                locals: {
                    user: {
                        is_admin: 0
                    }
                }
            };
            underTest.deleteEmployee(req, res, function() {});
        });
    });
    describe('on failed delete employee', function() {
        beforeEach(function(done) {
            employeeStub = sinon.stub(Employee, 'destroy', function() {
                return Promise.resolve(0);
            });
            done();
        });

        afterEach(function(done) {
            employeeStub.restore();
            done();
        });
        it('should return to list employee', function(done) {
            var req = {
                params: {
                    id: 6
                },
                flash: function(args) {
                    return args;
                }
            };
            var res = {
                redirect: function(args) {
                    try {
                        assert.equal(args, '/employee');
                        done();
                    } catch (err) {}
                },
                locals: {
                    user: {
                        is_admin: 1
                    }
                }
            };
            underTest.deleteEmployee(req, res, function() {});
        });
    });
});