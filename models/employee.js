module.exports = function(sequelize, DataTypes) {
    return sequelize.define("Employee", {
        employee_no: DataTypes.STRING,
        password: DataTypes.STRING,
        email: DataTypes.STRING,
        name: DataTypes.STRING,
        join_date: DataTypes.DATE,
        place_of_birth: DataTypes.TEXT,
        date_of_birth: DataTypes.DATEONLY,
        is_admin: DataTypes.BOOLEAN,
        retire_date: DataTypes.DATE,
        phone_no: DataTypes.STRING,
        telp_no: DataTypes.STRING,
        gender: DataTypes.ENUM('male', 'female'),
        address: DataTypes.STRING,
        nik: DataTypes.STRING,
        id_job_title: DataTypes.INTEGER,
        telp_no: DataTypes.STRING,
        salary: DataTypes.INTEGER,
        token: DataTypes.TEXT
    }, {
        tableName: 'employees',
        timestamps: false,
        indexes: [{
            unique: true,
            fields: ['email', 'id']
        }]
    });
};