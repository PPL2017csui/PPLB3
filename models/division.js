module.exports = function(sequelize, DataTypes) {
    jobModel = sequelize.define("Division", {
        id_company: DataTypes.INTEGER,
        division_name: DataTypes.STRING
    }, {
        timestamps: false,
        tableName: 'divisions',
        classMethods: {
            associate: function(models) {
                jobModel.belongsTo(models.Company, {
                    as: 'company',
                    onDelete: "CASCADE",
                    foreignKey: 'id_company'
                });
            }
        }
    });
    return jobModel;
};