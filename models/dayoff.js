module.exports = function(sequelize, DataTypes) {
    return sequelize.define("Permit_history", {
        start_date: DataTypes.DATEONLY,
        end_date: DataTypes.DATEONLY,
        id_employee: DataTypes.STRING,
        approval_status: DataTypes.ENUM('disetujui', 'menunggu persetujuan', 'ditolak'),
        approval_date: DataTypes.DATEONLY,
        superior_note: DataTypes.STRING,
        request_date: DataTypes.DATEONLY,
        employee_note: DataTypes.STRING,
        expire_date: DataTypes.DATEONLY,
        dayoff_type: DataTypes.ENUM('sakit', 'izin', 'cuti'),
        value: DataTypes.INTEGER
    }, {
        tableName: 'permit_histories',
        timestamps: false,
        indexes: [{
            unique: true,
            fields: ['start_date', 'id_employee']
        }],
        classMethods: {
            associate: function(models) {
                models.Permit_history.belongsTo(models.Employee, {
                    as: 'employee',
                    onDelete: "CASCADE",
                    foreignKey: 'id_employee'
                });
            }
        }
    });
};