module.exports = function(sequelize, DataTypes) {
    var jobModel = sequelize.define("Job", {
        job_title: DataTypes.STRING,
        supervisor: DataTypes.INTEGER,
        id_division: DataTypes.INTEGER
    }, {
        tableName: 'jobs',
        timestamps: false,
        classMethods: {
            associate: function(models) {
                jobModel.belongsTo(models.Job, {
                    as: 'supervised',
                    onDelete: "CASCADE",
                    foreignKey: 'supervisor'
                });
            }
        }
    });
    return jobModel;
};