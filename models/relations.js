var model = require('./index');

module.exports = function() {
    Attendance = model.Attendance;
    Employee = model.Employee;
    Division = model.Division;
    Job = model.Job;
    Permit_history = model.Permit_history;
    Attendance.Employee = Attendance.belongsTo(Employee, {
        foreignKey: 'id_employee',
        targetKey: 'id'
    });
    Permit_history.Employee = Permit_history.belongsTo(Employee, {
        foreignKey: 'id_employee',
        targetKey: 'id'
    });
    Employee.Job = Employee.belongsTo(Job, {
        foreignKey: 'id_job_title',
        targetKey: 'id'
    });
    Job.Division = Job.belongsTo(Division, {
        foreignKey: 'id_division',
        targetKey: 'id'
    });
};