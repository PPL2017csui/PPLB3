module.exports = function(sequelize, DataTypes) {
    return sequelize.define("Attendance", {
        absent_date: {
            type: DataTypes.DATEONLY,
            primaryKey: true
        },
        id_employee: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        in: DataTypes.TIME,
        out: DataTypes.TIME
    }, {
        timestamps: false,
        tableName: 'attendances'
    });
};
