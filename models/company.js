module.exports = function(sequelize, DataTypes) {
    return sequelize.define("Company", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        company_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        address: {
            type: DataTypes.STRING,
            allowNull: true
        },
        description: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {
        tableName: 'companies',
        timestamps: false,
        indexes: [{
            unique: true,
            fields: ['company_name', 'id']
        }]
    });
};