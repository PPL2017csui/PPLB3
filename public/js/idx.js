$(document).ready(function() {
    $('.page').scroll(function() {
        if ($(this).scrollTop() > 70) {
            $('.nav').css("background-color", "rgba(15, 163, 177, 1)");
            $('.button-nav').css("color", "#fff");
            $('.nav').css("border-bottom", "none");
        } else {
            $('.nav').css("background-color", "transparent");
            $('.button-nav').css("color", "#939393");
            $('.button-nav:hover').css("color", "#000");
            $('.nav').css("border-bottom", "none");
        }
    });

    $('#scrollup').on('click', function() {
        $('.page').animate({ scrollTop: 0 }, 800);
    });

    $('#tologin').on('click', function(e) {
        window.location.href = '/login';
    });

    $('#tologin-2').on('click', function(e) {
        window.location.href = '/login';
    });

    $('.page').animate({ scrollTop: 1000 }, 0).animate({ scrollTop: 0 }, 1000);
});

function jump(h) {
    var container = $('.page'),
        scrollTo = $('#' + h);
    container.animate({ scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop() - 70 }, 800);
}