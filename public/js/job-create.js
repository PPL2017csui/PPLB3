var dataList = document.getElementById('supervisors');
var dataListDiv = document.getElementById('divisions');
var job_title = document.getElementById('job_title');
var url_json = '/job/api/list-job-title';
var url_json_div = '/division/api/list-division';
const spaceCHar = 32;

$.ajax({
    type: 'GET',
    url: url_json,
    success: function(response) {
        $.each(response, function(key, value) {
            var option = document.createElement('option');
            option.value = Object.values(value);
            dataList.appendChild(option);
        });
    }
});

$.ajax({
    type: 'GET',
    url: url_json_div,
    success: function(response) {
        $.each(response, function(key, value) {
            var option_div = document.createElement('option');
            option_div.value = Object.values(value);
            dataListDiv.appendChild(option_div);
        });
    }
});

job_title.onkeydown = function(e) {
    var val = job_title.value.length;
    if (val == 0) {
        return e.which !== spaceChar;
    }
}