$(document).ready(function() {
    $('#permitTable').DataTable();
    $('#gentable').DataTable();
});

var table = document.getElementById("permitTable");
for (var i = 1, row; row = table.rows[i]; i++) {
    var status = table.rows[i].cells.namedItem("approvalStatus").innerHTML;
    var idPermit = table.rows[i].cells.namedItem("idPermit").innerHTML;
    if (status == "menunggu persetujuan") {
        table.rows[i].cells.namedItem("approvalStatus").innerHTML = '<input type="hidden" name="accept" value="1"><button type="submit" name="permitID" value="' + idPermit + '" class="btn btn-default">Terima</button><a href="/dayoff/detail/' + idPermit + '"><button type="button" class="btn btn-default">Tolak</button>';
    }

}
$(function() {
    $('.permit_entry').click(function() {
        var idpermit = $(this).data('idpermit');
        window.location.href = '/dayoff/detail/' + idpermit;
    });
})