$(function() {

    jQuery(document).on('change', ':file', function() {
        var input = $(this);
        numFiles = input.get(0).files ? input.get(0).files.length : 1;
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });

    jQuery(document).ready(function($) {
        showAllDiv();
        showAllJob();
        $(':file').on('fileselect', function(event, numFiles, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if (input.length) {
                input.val(log);
            } else {
                if (log) alert(log);
            }
        });
    });
});

var table = $('#tableAbsent').DataTable({
    "aoColumns": [
        null,
        null,
        null,
        null,
        { "sType": "date-uk" },
        null,
        null
    ]
});

$(document).ready(function() {
    $('#tableAbsent tfoot th').each(function() {
        var title = $(this).text();
        if (title == 'Divisi') {
            $(this).html('<input type="text" placeholder="Search ' + title + '" /><datalist id="list' + title + '"></datalist>');

        } else if (title == 'Jabatan') {
            $(this).html('<input type="text" placeholder="Search ' + title + '" /><datalist id="list' + title + '"></datalist>');

        } else {
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');
        }
    });
    // DataTable
    // Apply the search
    table.columns().every(function() {
        var that = this;
        $('input', this.footer()).on('keyup change', function() {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        });
    });

    //change date format sorting
    jQuery.extend(jQuery.fn.dataTableExt.oSort, {
        "date-uk-pre": function(a) {
            var ukDatea = a.split('-');
            console.log(a);
            return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
        },

        "date-uk-asc": function(a, b) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },

        "date-uk-desc": function(a, b) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        }
    });


    $("input:file").change(function() {
        $("button").prop('disabled', false);
    });
});

//# sourceURL=pen.js
function showAllJob() {
    var dataList = document.getElementById('filterJobTitle');
    var url_json = '/job/api/list-job-title';
    $.ajax({
        type: 'GET',
        url: url_json,
        success: function(response) {
            $.each(response, function(key, value) {
                var option = document.createElement('option');
                option.value = value.id;
                option.innerHTML = value.job_title;
                dataList.appendChild(option);
            });
        }
    });
}

function changeJobOption(thisDivsion) {
    filterBy('job');
    if (thisDivsion === "All Division") {
        $("#filterJobTitle").children('option').show();
    } else {
        var url_json = '/job/api/list-job-title';
        $("#filterJobTitle").children('option').hide();
        $('#filterJobTitle').val('All Job');
        $("#filterJobTitle select").val('All Job');
        $("#filterJobTitle option[value=\"All Job\"]").show();
        filterJob = document.getElementById("filterJobTitle").value;
        $.ajax({
            type: 'GET',
            url: url_json,
            data: "selectedDivision=" + thisDivsion,
            success: function(response) {
                $.each(response, function(key, value) {
                    $("#filterJobTitle option[value=\"" + value.id + "\"]").show();
                });
            }
        });
    }
}


function showAllDiv() {
    var dataList = document.getElementById('filterDivision');
    var url_json = '/attendance/api/list-division';
    $.ajax({
        type: 'GET',
        url: url_json,
        success: function(response) {
            $.each(response, function(key, value) {
                var option = document.createElement('option');
                option.value = Object.values(value);
                option.innerHTML = Object.values(value);
                dataList.appendChild(option);
            });
        }
    });

}

function filterBy(thisFilter) {
    var input, filter, tr, td, i, filterColumn;

    if (thisFilter == "job") {
        filterJob = document.getElementById("filterJobTitle").value;
        $.fn.dataTable.ext.search.push(
            function(settings, data, dataIndex) {
                var dataJob = data[3]; // use data for the job column
                if (filterJob == "All Job") {
                    return true;
                } else {
                    if (dataJob == filterJob) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        );
        table.draw();
    } else if (thisFilter == "division") {
        filterDivision = document.getElementById("filterDivision").value;
        changeJobOption(filterDivision);
        $.fn.dataTable.ext.search.push(
            function(settings, data, dataIndex) {
                var dataDivision = data[2]; // use data for the division column
                if (filterDivision == "All Division") {
                    return true;
                } else {
                    if (dataDivision == filterDivision) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        );
        table.draw();
    } else if (thisFilter == "date") {}
}