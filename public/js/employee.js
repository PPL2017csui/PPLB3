function showAllJob() {
    var dataList = document.getElementById('jobList');
    var url_json = '/job/api/list-job-title';
    $.ajax({
        type: 'GET',
        url: url_json,
        success: function(response) {
            $.each(response, function(key, value) {
                var option = document.createElement('option');
                option.value = value.id;
                option.innerHTML = value.job_title;
                dataList.appendChild(option);
            });
        }
    });
}

showAllJob();