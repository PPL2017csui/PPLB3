var division_name = document.getElementById('division_name');
var id_company = document.getElementById('id_company');
const spaceChar = 32;

division_name.onkeydown = function(e) {
    var val = division_name.value.length;
    if (val == 0) {
        return e.which !== spaceChar;
    }
}

id_company.onkeydown = function(e) {
    var val = id_company.value.length;
    if (val == 0) {
        return e.which !== spaceChar;
    }
}