var url_json = '/dayoff/api/list-dates';
var restrictedDates = [];

$.ajax({
    type: 'GET',
    url: url_json,
    success: function(response) {
        $.each(response, function(key, value) {
            var start_end = Object.values(value);
            var dirty_start_date = start_end[0].split('T');
            var dirty_end_date = start_end[1].split('T');
            var start_date = new Date(dirty_start_date[0]);
            var end_date = new Date(dirty_end_date[0]);
            var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
            var range = Math.round(Math.abs((end_date.getTime() - start_date.getTime()) / oneDay));
            if (range === 0) {
                var temp = new Date(start_date.getTime());
                restrictedDates.push((temp.getMonth() + 1) + '-' + temp.getDate() + '-' + +temp.getFullYear());
            } else {
                for (var i = 0; i <= range; i++) {
                    var temp = new Date(start_date.getTime() + oneDay * i);
                    begin = (temp.getMonth() + 1) + '-' + temp.getDate() + '-' + temp.getFullYear();
                    restrictedDates.push(begin);
                }
            }
        });
    }
}).done(function(data) {
    for (var i = 0; i < restrictedDates.length; i++) {}
    return restrictedDates;
});

function DisableRestrictedDates(date) {
    var m = date.getMonth();
    var d = date.getDate();
    var y = date.getFullYear();
    var currentdate = (m + 1) + '-' + d + '-' + y;
    for (var i = 0; i < restrictedDates.length; i++) {
        if ($.inArray(currentdate, restrictedDates) != -1) {
            return [false];
        }
    }
    var weekenddate = $.datepicker.noWeekends(date);
    return weekenddate;
}

$(function() {
    $('input[type="chosenDate"]').datepicker();
});

$('#day_off_date').datepicker({
    beforeShowDay: DisableRestrictedDates,
    minDate: new Date(),
    autoclose: true,
    onSelect: function(selected) {
        $('#end_date').datepicker('option', 'minDate', selected);
    }
});

$('#end_date').datepicker({
    beforeShowDay: DisableRestrictedDates,
    minDate: new Date(),
    autoclose: true,
    onSelect: function(selected) {
        $('#day_off_date').datepicker('option', 'maxDate', selected)
    }
});