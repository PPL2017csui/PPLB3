$(window).load(function() {
    $('.table_entry').click(function() {
        var req_date = $(this).data('reqdate');
        var type = $(this).data('type');
        var approval_status = $(this).data('approval');
        var day_off_date = $(this).data('dayoffdate');
        var end_date = $(this).data('enddate');
        var employee_note = $(this).data('empnote');
        var superior_note = $(this).data('supnote');
        var appendThis;

        if (approval_status === 'disetujui') {
            appendThis = '<kbd class="label label-success">Disetujui</kbd>';
        } else if (approval_status === 'menunggu persetujuan') {
            appendThis = '<kbd class="label label-warning">Menunggu Persetujuan</kbd>';
        } else {
            appendThis = '<kbd class="label label-danger">Ditolak</kbd>';
        }

        if (employee_note.length === 0) {
            employee_note = '-';
        }

        if (superior_note.length === 0) {
            superior_note = '-';
        }

        var date = req_date.split(' ');
        req_date = date[2] + ' ' + toMonth(date[1]) + ' ' + date[3];

        date = day_off_date.split(' ');
        day_off_date = date[2] + ' ' + toMonth(date[1]) + ' ' + date[3];

        date = end_date.split(' ');
        end_date = date[2] + ' ' + toMonth(date[1]) + ' ' + date[3];

        $('.container').append('<div id="dayoffModal" class="modal fade" role="dialog" aria-labelledby="dayOffModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeModal()">x</button><h3>Detail Pengajuan</h3></div><div class="modal-body"><p><b>Tanggal Pengajuan: </b>' + req_date + '</p><p><b>Tipe: </b>' + type + '</p><p><b>Tanggal Mulai Cuti: </b>' + day_off_date + '</p><p><b>Tanggal Selesai Cuti: </b>' + end_date + '</p><p><b>Keterangan Pengajuan: </b>' + employee_note + '</p><p><b>Status Pengajuan: </b>' + appendThis + '</p><b>Catatan Atasan/HR: </b>' + superior_note + '</p></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="closeModal()">Tutup</button></div></div></div></div>');
    });
});

function toMonth(mo) {
    console.log(mo);
    if (mo === 'Jan') {
        mo = 'Januari'
    } else if (mo === 'Feb') {
        mo = 'Februari'
    } else if (mo === 'Mar') {
        mo = 'Maret'
    } else if (mo === 'Apr') {
        mo = 'April'
    } else if (mo === 'May') {
        mo = 'Mei'
    } else if (mo === 'Jun') {
        mo = 'Juni'
    } else if (mo === 'Jul') {
        mo = 'Juli'
    } else if (mo === 'Aug') {
        mo = 'Agustus'
    } else if (mo === 'Sep') {
        mo = 'September'
    } else if (mo === 'Oct') {
        mo = 'Oktober'
    } else if (mo === 'Nov') {
        mo = 'November'
    } else {
        mo = 'Desember'
    }
    return mo;
}

function closeModal() {
    document.getElementById('dayoffModal').remove();
}