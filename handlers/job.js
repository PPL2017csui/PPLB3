let model = require('../models');
let Job = model.Job;
let Sequelize = require('sequelize');
let sequelize = new Sequelize('auto-personalia', 'root', '');

module.exports = {
    viewCreateJob: function(req, res) {
        res.render('job-create', {
            message: req.flash('message'),
            perusahaan: true
        });
    },
    viewListJob: function(req, res) {
        model.Job.findAll({
                attributes: ['id', 'job_title'],
                include: [{
                    as: 'supervised',
                    model: Job,
                    attributes: [
                        ['job_title', 'supervisor']
                    ]
                }, {
                    association: model.Job.Division,
                    include: [{
                        model: model.Company,
                        as: 'company',
                        where: {
                            id: res.locals.company
                        }
                    }]
                }]
            })
            .then(function(jobs) {
                res.render('job-view', {
                    jobs: jobs,
                    perusahaan: true
                });
            }).catch(function(err) {
                res.render('job-view', {
                    message: err,
                    perusahaan: true
                });
            })
    },
    getJobTitle: function(req, res) {
        if (!req.query.selectedDivision) {
            model.Job.findAll({
                attributes: ['job_title', 'id'],
                include: [{
                    association: model.Job.Division,
                    include: [{
                        model: model.Company,
                        as: 'company',
                        where: {
                            id: res.locals.company
                        }
                    }]
                }]
            }).then(function(job_titles) {
                res.json(job_titles);
            }).catch(function(err) {
                res.send(err);
            });
        } else {
            model.Job.findAll({
                attributes: ['job_title', 'id'],
                include: [{
                    model: model.Division,
                    where: {
                        division_name: req.query.selectedDivision
                    }
                }]
            }).then(function(job_titles) {
                res.json(job_titles);
            }).catch(function(err) {
                res.send(err);
            });
        }
    },
    saveCreateJob: function(req, res) {
        let job_title = req.body.job_title;
        let division = req.body.division;
        if (job_title === '') {
            job_title = null;
        }
        model.Division.findAll({
            attributes: ['id'],
            where: {
                division_name: division
            }
        }).then(function(division_id) {
            if (req.body.supervisor) {
                findSupervisorId(division_id[0].dataValues.id, job_title, req.body.supervisor, req, res);
            } else {
                createJobWithoutSp(division_id[0].dataValues.id, job_title, req, res);
            }
        }).catch(function(err) {
            console.error('Error #608 : Divisi tidak ditemukan');
            req.flash('message', 'Divisi tidak ditemukan');
            res.redirect('/job/create');
        })
    },
};

function findSupervisorId(division_id, job_title, id, req, res) {
    model.Job.findAll({
        attributes: ['id'],
        where: {
            job_title: id
        }
    }).then(function(jobs) {
        createJob(division_id, job_title, jobs[0].dataValues.id, req, res);
    }).catch(function(err) {
        console.error('Error #601 : Atasan tidak ditemukan');
        req.flash('message', 'Atasan tidak ditemukan');
        res.redirect('/job/create');
    })
};

function createJob(division_id, job_title, supervisor_id, req, res) {
    model.Job.create({
        job_title: job_title,
        supervisor: supervisor_id,
        id_division: division_id
    }).then(function() {
        req.flash('message', 'Jabatan dengan atasan berhasil dibuat');
        res.redirect('/job/create');
    }).catch(Sequelize.UniqueConstraintError, function() {
        console.error('Error #603 : Nama jabatan sudah pernah dipakai');
        req.flash('message', 'Nama jabatan sudah pernah dipakai');
        res.redirect('/job/create');
    }).catch(function(err) {
        console.error('Error #602 : Pastikan data atasan yang Anda masukkan sudah benar');
        req.flash('message', 'Pastikan data atasan yang Anda masukkan sudah benar');
        res.redirect('/job/create');
    });
};

function createJobWithoutSp(division_id, job_title, req, res) {
    model.Job.create({
        job_title: job_title,
        id_division: division_id
    }).then(function() {
        req.flash('message', 'Jabatan berhasil dibuat');
        res.redirect('/job/create');
    }).catch(Sequelize.UniqueConstraintError, function() {
        console.error('Error #603 : Nama jabatan sudah pernah dipakai');
        req.flash('message', 'Nama jabatan sudah pernah dipakai');
        res.redirect('/job/create');
    }).catch(function(err) {
        console.error('Error #602 : Pastikan data atasan yang Anda masukkan sudah benar');
        req.flash('message', 'Pastikan data atasan yang Anda masukkan sudah benar');
        res.redirect('/job/create');
    });
};