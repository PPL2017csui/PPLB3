let model = require('../models');
let Job = model.Job;
let Sequelize = require('sequelize');
let sequelize = new Sequelize('auto-personalia', 'root', '');
var password = require('password-hash-and-salt');

module.exports = {
    viewSetPassword: function(req, res) {
        var token = req.query.token;
        model.Employee.findOne({
            where: {
                token: token
            }
        }).then(function(user) {
            if (user != null) {
                res.render('set-password', {
                    message: req.flash('message')
                });
            } else {
                console.error('Error #621: token salah');
                req.flash('message', 'Silahkan login');
                res.redirect('/login');
            }
        });
    },
    savePassword: function(req, res) {
        var token = req.query.token;
        var new_password = req.body.new_password;
        password(new_password).hash(function(error, pw_hash) {
            model.Employee.find({
                where: {
                    token: token
                }
            }).then(function(Emp) {
                Emp.updateAttributes({
                    password: pw_hash,
                    token: null
                });
            }).then(function() {
                req.flash('message', 'Password berhasil diubah');
                res.redirect('/login');
            }).catch(function(err) {
                console.error('Error #622 : Pengubahan password gagal');
                req.flash('message', 'Pengubahan password gagal');
                res.redirect('/set-password');
            });
        });
    },
}