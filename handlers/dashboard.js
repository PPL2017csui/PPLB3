var model = require('../models');
var dateFormatter = require('../utils/date');

module.exports = {
    renderAdminDashboard(req, res) {
        cur_date = new Date();
        date = cur_date.getFullYear() + '-' + (cur_date.getMonth() + 1) + '-' + cur_date.getDate();

        attendances = [0, 0, 0];
        model.sequelize.query('select attendances.in, attendances.out, employees.id from attendances '+
            'right join employees on employees.id = attendances.id_employee '+
            'and attendances.absent_date = "'+ date +'"').then((attd) => {
            attd = attd[0];
            attd.forEach((att) => {
                if (att.in == null) attendances[0]++;
                else if (att.in > '08:00:00') attendances[2]++;
                else attendances[1]++;
            });
            return {attendances: attendances};
        }).then(data => {
            return model.Employee.findAll({
                attributes: ['name', 'date_of_birth']
            }).then(employees => {
                new_emp = [];
                employees.forEach(emp => {
                    emp.birthday = dateFormatter.getBirthdayDate(emp.date_of_birth);
                    new_emp.push(emp);
                });
                data.employees = new_emp;
                return data;
            });
        }).then(data => {
            return model.Permit_history.findAll({
                attributes: ['start_date', 'end_date', 'id'],
                where: {
                    approval_status: 'disetujui'
                },
                include: [
                    {
                        as: 'employee',
                        model: model.Employee
                    }
                ]
            }).then(day_offs => {
                day_offs.forEach(day_off => {
                    day_off.start_date = dateFormatter.retrieveDateReverse(day_off.start_date);
                    day_off.end_date = dateFormatter.retrieveDateReverse(day_off.end_date);
                    day_off.url = process.env.BASE_URL + '/dayoff/detail/' + day_off.id;
                });
                data.day_offs = day_offs;
                return data;
            });
        }).then(data => {
            res.render('admin-dashboard', data);
        });
    },
    renderNonAdminDashboard(req, res) {
        cur_date = new Date();
        date = cur_date.getFullYear() + '-' + (cur_date.getMonth() + 1) + '-' + cur_date.getDate();
        firstDay = new Date(cur_date.getFullYear(), cur_date.getMonth(), 1);
        lastDay = new Date(cur_date.getFullYear(), cur_date.getMonth() + 1, 0);
        numDate = dateFormatter.getWeekdaysInMonth(cur_date.getMonth(), cur_date.getFullYear());
        attendances = [0, 0, 0];
        model.sequelize.query('select attendances.in, attendances.out, employees.id from attendances '+
            'join employees on employees.id = attendances.id_employee '+
            'where attendances.absent_date between "'+ firstDay +'" and "'+ lastDay +'"' +
            'and employees.id = ' + req.user.id).then((attd) => {
            attd = attd[0];
            attd.forEach((att) => {
                if (att.in > '08:00:00') attendances[2]++;
                else attendances[1]++;
            });
            attendances[0] = numDate - attendances[2] - attendances[1];
            return {attendances: attendances};
        }).then(data => {

            return model.Employee.findOne({
                where: {
                    id: req.user.id
                }
            }).then(emp => {
                return model.Job.findOne({
                    where: {
                        id: emp.id_job_title
                    }
                });
            }).then(job => {
                return model.sequelize.query('select employees.date_of_birth, employees.name, employees.id ' +
                    'from employees ' +
                    'join jobs on jobs.id = employees.id_job_title ' +
                    'where jobs.id_division = ' + job.id_division);
            }).then(employees => {
                employees = employees[0];
                ids = [];
                employees.forEach(emp => {
                    emp.birthday = dateFormatter.getBirthdayDate(emp.date_of_birth);
                    ids.push(emp.id);
                });
                data.employees = employees;
                data.ids = ids;
                return data;
            });
        }).then(data => {
            return model.Permit_history.findAll({
                attributes: ['start_date', 'end_date', 'id'],
                where: {
                    approval_status: 'disetujui',
                    id_employee: {
                        $in: data.ids
                    }
                },
                include: [
                    {
                        as: 'employee',
                        model: model.Employee
                    }
                ]
            }).then(day_offs => {
                day_offs.forEach(day_off => {
                    day_off.start_date = dateFormatter.retrieveDateReverse(day_off.start_date);
                    day_off.end_date = dateFormatter.retrieveDateReverse(day_off.end_date);
                    day_off.url = process.env.BASE_URL + '/dayoff/detail/' + day_off.id;
                });
                data.day_offs = day_offs;
                return data;
            })
        }).then(data => {
            res.render('non-admin-dashboard', data);
        });
    }
};