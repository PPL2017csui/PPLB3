/**
 * Module handler untuk employee
 * @module handlers/employee
 */

/**
 * Menggunakan utils/date untuk parsing tanggal
 */

var date = require('../utils/date');
/**
 * Menggunakan model dalam kasus ini menggunakan Employee model
 */
var model = require('../models');
/**
 * Sequelize untuk memanggil fungsi model yang menggunakan sequelize
 */
var Sequelize = require('sequelize');
var mailer = require('../utils/mailer');
var randomstring = require("randomstring");

let mailOptions = {
    from: '"Autopersonalia" <autopersonaliapplb3@gmail.com>', // sender address
    subject: 'Selamat bergabung di Autopersonalia' // Subject line
};

/**
 * fungsi yang di export
 */
module.exports = {
    /**
     * Menampilkan halaman create employee
     * @param {Object} req - Akan berisi request dari router
     * @param {Object} res - Akan berisi result dari request
     */
    viewCreateEmployee: function(req, res) {
        res.render('employee-create', {
            message: req.flash('message'),
            karyawan: true
        });
    },
    /**
     * Menampilkan halaman list employee
     * Menggunakan model Employee untuk query semua employee yang ada
     * @param {Object} req - Akan berisi request dari router
     * @param {Object} res - Akan berisi result dari request
     */
    viewListEmployee: function(req, res) {
        model.Employee.findAll({
                include: [{
                    association: model.Employee.Job,
                    include: [{
                        association: model.Job.Division,
                        where: {
                            id_company: res.locals.company
                        },
                        include: [{
                            as: 'company',
                            model: model.Company,
                        }]
                    }]
                }]
            })
            .then(function(employees) {
                console.log(employees.Job)
                res.render('employee-list', {
                    employees: employees,
                    message: req.flash('message'),
                    karyawan: true
                });
            });
    },
    viewDetailEmployee: function(req, res) {
        if (res.locals.user.id == req.params.id || res.locals.user.is_admin) {
            model.Employee.findOne({
                    where: {
                        id: req.params.id
                    },
                    include: [{
                        association: model.Employee.Job,
                        include: [{
                            association: model.Job.Division,
                            where: {
                                id_company: res.locals.company
                            },
                            include: [{
                                as: 'company',
                                model: model.Company,
                            }]
                        }]
                    }]
                })
                .then(function(employee) {
                    if (employee) {
                        res.render('employee-detail', {
                            employee: employee,
                            join_date: date.dateToUser(employee.join_date),
                            date_of_birth: date.dateToUser(employee.date_of_birth),
                            retire_date: date.dateToUser(employee.retire_date),
                            can_delete: req.params.id != res.locals.user.id,
                            karyawan: true
                        });
                    } else {
                        console.error('Error #617: Tidak ada karyawan dengan id ' + req.params.id);
                        req.flash('message', 'Tidak ada karyawan dengan id ' + req.params.id);
                        res.redirect('/employee');
                    }
                })
        } else {
            console.error('Error #618: Tidak bisa melihat data karyawan');
            req.flash('message', 'Tidak bisa melihat data karyawan');
            res.redirect('/employee');
        }
    },
    saveCreateEmployee: function(req, res) {
        var date_of_birth = new Date(req.body.date_of_birth);
        var join_date = new Date(req.body.join_date);
        var token = randomstring.generate();
        model.Employee.create({
            employee_no: req.body.employee_no,
            name: req.body.name,
            email: req.body.email,
            // sementara diset default untuk dilakuakn reset password
            password: null,
            phone_no: req.body.phone_no,
            telp_no: req.body.telp_no,
            nik: req.body.nik,
            address: req.body.address,
            gender: req.body.gender,
            join_date: join_date,
            place_of_birth: req.body.place_of_birth,
            date_of_birth: date_of_birth,
            is_admin: req.body.is_admin,
            salary: req.body.salary,
            token: token,
            retire_date: null,
            id_job_title: req.body.job_id
        }).then(function() {
            mailOptions.to = req.body.email;
            url = process.env.BASE_URL + '/set-password?token=' + token;
            mailOptions.html = "Silakan akses link <a href=\"" + url + "\">ini</a>.";
            mailer.send(mailOptions);
            req.flash('message', 'Data employee berhasil ditambahkan');
            res.redirect('/employee');
        }).catch(Sequelize.UniqueConstraintError, function(err) {
            if ('nik' in err.fields) {
                field = 'NIK';
            } else if ('email' in err.fields) {
                field = 'Email';
            } else if ('phone' in err.fields) {
                field = 'Nomor HP';
            } else {
                field = 'Employee ID';
            }
            console.error('Error #604: ' + field + ' sudah pernah dipakai');
            req.flash('message', field + ' sudah pernah dipakai');
            res.redirect('/employee/create');
        }).catch(function(err) {
            console.error(err);
            console.error('Error #605: Pastikan data yang Anda masukkan sudah benar');
            req.flash('message', 'Pastikan data yang Anda masukkan sudah benar');
            res.redirect('/employee/create');
        });
    },
    viewUpdateEmployee: function(req, res) {
        if (res.locals.user.id == req.params.id || res.locals.user.is_admin) {
            model.Employee.findOne({
                    where: {
                        id: req.params.id
                    },
                    include: [{
                        association: model.Employee.Job
                    }]
                })
                .then(function(employee) {
                    if (employee) {
                        res.render('employee-edit', {
                            employee: employee,
                            join_date: date.dateToCalendar(employee.join_date),
                            date_of_birth: date.dateToCalendar(employee.date_of_birth),
                            retire_date: date.dateToCalendar(employee.retire_date),
                            gender: employee.gender == 'male' ? true : false,
                            karyawan: true
                        });
                    } else {
                        console.error('Error #617: Tidak ada karyawan dengan id ' + req.params.id);
                        req.flash('message', 'Tidak ada karyawan dengan id ' + req.params.id);
                        res.redirect('/employee');
                    }
                })
        } else {
            console.error('Error #619: Tidak bisa mengubah karyawan');
            req.flash('message', 'Tidak bisa mengubah karyawan');
            res.redirect('/employee');
        }
    },
    saveUpdateEmployee: function(req, res) {
        if (res.locals.user.id == req.params.id || res.locals.user.is_admin) {
            var date_of_birth = new Date(req.body.date_of_birth);
            var join_date = new Date(req.body.join_date);
            model.Employee.update({
                name: req.body.name,
                email: req.body.email,
                phone_no: req.body.phone_no,
                telp_no: req.body.telp_no,
                nik: req.body.nik,
                address: req.body.address,
                gender: req.body.gender,
                join_date: join_date,
                place_of_birth: req.body.place_of_birth,
                date_of_birth: date_of_birth,
                is_admin: req.body.is_admin,
                salary: req.body.salary,
                id_job_title: req.body.job_id
            }, {
                where: {
                    id: req.params.id
                }
            }).then(function() {
                req.flash('message', 'Data karyawan berhasil diubah');
                res.redirect('/employee/detail/' + req.params.id);
            }).catch(Sequelize.UniqueConstraintError, function(err) {
                if ('nik' in err.fields) {
                    field = 'NIK';
                } else if ('email' in err.fields) {
                    field = 'Email';
                } else if ('phone' in err.fields) {
                    field = 'Nomor HP';
                } else {
                    field = 'Employee ID';
                }
                console.error('Error #604: ' + field + ' sudah pernah dipakai');
                req.flash('message', field + ' sudah pernah dipakai');
                res.redirect('/employee/edit/' + req.params.id);
            }).catch(function(err) {
                console.error('Error #605: Pastikan data yang Anda masukkan sudah benar');
                req.flash('message', 'Pastikan data yang Anda masukkan sudah benar');
                res.redirect('/employee');
            });
        } else {
            console.error('Error #619: Tidak bisa mengubah karyawan');
            req.flash('message', 'Tidak bisa mengubah karyawan');
            res.redirect('/employee');
        }
    },
    deleteEmployee: function(req, res) {
        if (res.locals.user.is_admin && req.params.id != res.locals.user.id) {
            model.Employee.destroy({
                where: {
                    id: req.params.id
                }
            }).then(function(result) {
                if (result) {
                    req.flash('message', 'Data karyawan berhasil dihapus');
                    res.redirect('/employee');
                } else {
                    console.error('Error #617: Tidak ada karyawan dengan id ' + req.params.id);
                    req.flash('message', 'Tidak ada karyawan dengan id ' + req.params.id);
                    res.redirect('/employee');
                }
            })
        } else {
            console.error('Error #620: Tidak bisa menghapus karyawan');
            req.flash('message', 'Tidak bisa menghapus karyawan');
            res.redirect('/employee');
        }
    }
};