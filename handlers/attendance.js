let model = require('../models');
let csv = require('csv-stream');
let fs = require('fs');
let dateFormatter = require('../utils/date');
let Promise = require('bluebird');
let Sequelize = require('sequelize');

let hydrateAttendance = function hydrateAttendance(attendances) {
    attd = [];
    attendances.forEach((attendance) => {
        attd.push({
            id: attendance.Employee.employee_no,
            name: attendance.Employee.name,
            in: attendance.in,
            job: attendance.Employee.Job.job_title,
            division: attendance.Employee.Job.Division.division_name,
            out: attendance.out,
            absent_date: dateFormatter.retrieveDate(attendance.absent_date)
        });
    });
    return attd;
};

let dictionarizeEmployee = function dictionarizeEmployee(employees) {
    employee_dict = {};
    employees.forEach((employee) => {
        employee_dict[employee.employee_no] = employee.id;
    });
    return employee_dict;
};

let saveCsvData = Promise.promisify(function saveCsvData(req, res, employee_dict, csvFile, streamer, cb) {
    attendances = [];
    csvFile.pipe(streamer).on('data', (data) => {
        try {
            field_list = ['tanggal', 'no_employee', 'jam_masuk', 'jam_keluar'];
            field_list.forEach(x => {
                if (!(x in data)) {
                    throw new Error('error');
                }
            });
            attendance = {
                absent_date: dateFormatter.toMysqlFormat(data.tanggal),
                id_employee: employee_dict[data.no_employee],
                in: data.jam_masuk,
                out: data.jam_keluar
            };
            dates = data.tanggal.split('-');
            newDates = [];
            dates.forEach(x => {
                newDates.push(parseInt(x));
            });
            if (dateFormatter.isWeekday(newDates[2], newDates[1] - 1, newDates[0])) {
                attendances.push(attendance);
            }
        } catch (err) {
            req.flash('formatError', 'true');
        }
    }).on('end', () => {
        fs.unlink(req.file.path);
        model.Attendance.bulkCreate(attendances)
            .then(() => {
                cb();
            }).catch(Sequelize.UniqueConstraintError, (err) => {
                req.flash('dataError', 'true');
                cb();
            }).catch((err) => {
                req.flash('formatError', 'true');
                cb();
            });
    });
});

module.exports = {
    viewAttendance(req, res) {
        model.Attendance.findAll({
                include: [{
                    association: model.Attendance.Employee,
                    include: [{
                        association: model.Employee.Job,
                        include: [{
                            association: model.Job.Division,
                            where: {
                                id_company: res.locals.company
                            },
                            include: [{
                                as: 'company',
                                model: model.Company,
                            }]
                        }]
                    }]
                }]
            })
            .then(hydrateAttendance)
            .then((attendances) => {
                res.render('employee-attendance', {
                    attendances: attendances,
                    message: req.flash('message'),
                    kehadiran: true
                });
            });
    },
    saveAttendance(req, res) {
        csvFile = fs.createReadStream(req.file.path);
        streamer = csv.createStream();
        fileSplit = req.file.originalname.split(".");
        if (fileSplit[fileSplit.length - 1] !== 'csv') {
            fs.unlink(req.file.path);
            console.error('Error #610 : Sistem hanya dapat menerima file CSV');
            req.flash('message', 'Sistem hanya dapat menerima file CSV');
            res.redirect('/attendance');
            return Promise.resolve();
        } else {
            return model.Employee.findAll({
                    attributes: ['id', 'employee_no'],
                    include: [{
                        association: model.Employee.Job,
                        include: [{
                            association: model.Job.Division,
                            where: {
                                id_company: res.locals.company
                            },
                            include: [{
                                as: 'company',
                                model: model.Company,
                            }]
                        }]
                    }]
                })
                .then(dictionarizeEmployee)
                .then((data) => {
                    return saveCsvData(req, res, data, csvFile, streamer);
                })
                .then(() => {
                    if (req.flash('formatError')[0] === 'true') {
                        req.flash('message', 'Pastikan format file yang Anda upload sudah benar');
                    } else if (req.flash('dataError')[0] === 'true') {
                        console.error('Error #611 : Terdapat duplikasi data')
                        req.flash('message', 'Terdapat duplikasi data');
                    } else {
                        req.flash('message', 'Data berhasil disimpan');
                    }
                    res.redirect('/attendance');
                });
        }
    },
    downloadCSV(req, res) {
        res.sendFile('template.csv', {
            root: 'static/',
            headers: {
                'Content-Disposition': 'attachment; filename=template.csv'
            }
        });
    }
};