var model = require('../models');
var Sequelize = require('sequelize');
var sequelize = new Sequelize('auto-personalia', 'root', '');
var session = require('express-session');

module.exports = {
    viewCreateDivision: function(req, res) {
        res.render('division-create', {
            message: req.flash('message'),
            perusahaan: true
        });
    },
    viewListDivision: function(req, res) {
        var id_company = res.locals.company;
        model.Division.findAll({
            where: {
                id_company: id_company
            }
        }).then(function(divisions) {
            res.render('division-view', {
                message: req.flash('message'),
                divisions: divisions,
                perusahaan: true
            });
        }).catch(function(err) {
            req.flash('message', 'Tidak ada data');
            res.render('division-view', {
                message: req.flash('message'),
                perusahaan: true
            });
        })
    },
    saveCreateDivision: function(req, res) {
        var id_company = res.locals.company;
        var division_name = req.body.division_name;
        model.Division.create({
            division_name: division_name,
            id_company: id_company
        }).then(function() {
            req.flash('message', 'Divisi berhasil dibuat');
            res.redirect('/division/create');
        }).catch(Sequelize.UniqueConstraintError, function(err) {
            console.error('Error #609: Nama divisi sudah pernah dipakai');
            req.flash('message', 'Nama divisi sudah pernah dipakai');
            res.redirect('/division/create');
        }).catch(function(err) {
            console.error('Error #607 : Data divisi yang Anda masukkan salah');
            req.flash('message', 'Data divisi yang Anda masukkan salah');
            res.redirect('/division/create');
        });
    },
    getDivision: function(req, res) {
        var id_company = res.locals.company;
        model.Division.findAll({
                attributes: ['division_name'],
                where: {
                    id_company: id_company
                }
            })
            .then(function(division) {
                res.json(division);
            }).catch(function(err) {
                res.send(err);
            })
    }
};