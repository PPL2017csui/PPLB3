var model = require('../models');
var Sequelize = require('sequelize');
var Promise = require('bluebird');
var date = require('../utils/date');
/**
 * function for day off
 * @module dayoff
 */
module.exports = {
    /**
     * to view the page of list day off
     */
    viewListDayoff: function(req, res) {
        var id_company = res.locals.company;
        var id_employee = res.locals.user.id;

        model.Permit_history.findAll({
            include: [{
                association: model.Permit_history.Employee,
                where: {
                    id: {
                        $ne: id_employee
                    }
                },
                include: [{
                    association: model.Employee.Job,
                    include: [{
                        association: model.Job.Division,
                        where: {
                            id_company: id_company
                        }
                    }]
                }]
            }]
        }).then(function(permits) {
            var startoff_date;
            var endoff_date;
            for (var i = 0; i < permits.length; i++) {
                permits[i].startoff_date = date.dateToUser(permits[i].start_date);
                permits[i].endoff_date = date.dateToUser(permits[i].end_date);
            }
            res.render('dayoff-accept', {
                permits: permits,
                message: req.flash('message'),
                cuti: true
            });
        }).catch(function(err) {
            console.error('Error #612: Tidak ada data cuti');
            req.flash('message', 'Tidak ada data cuti');
            res.redirect('/dayoff/view');
        })
    },
    /**
     * function to accept employee's day off request
     */
    changeDayoff: function(req, res) {
        model.Permit_history.findOne({
            where: { id: req.body.permitID }
        }).then(function(permit) {
            if (req.body.accept) {
                if (req.body.superior_note) {
                    acceptWithNote(permit, req.body.permitID, req.body.superior_note, req, res);
                } else {
                    acceptWithoutNote(permit, req, res);
                }
            } else {
                rejectWithNote(permit, req.body.permitID, req.body.superior_note, req, res);
            }
        }).catch(function(err) {
            console.error('Error #613: Terjadi kesalahan dalam sistem');
            req.flash('message', "Terjadi kesalahan dalam sistem");
            res.redirect('/dayoff/view');
        });
    },
    /**
     * function to get detaif of employee's day off request
     */
    viewDetailDayoff: function(req, res) {
        var id_employee = res.locals.user.id;
        var divisi = res.locals.division;
        model.Permit_history.findOne({
                where: { id: req.params.id },
                include: [{
                    association: model.Permit_history.Employee,
                    where: {
                        id: {
                            $ne: id_employee
                        }
                    },
                    include: [{
                        association: model.Employee.Job,
                    }]
                }]
            })
            .then(function(permit) {
                if (permit) {
                    res.render('dayoff-detail', {
                        permit: permit,
                        startoff_date: date.dateToUser(permit.start_date),
                        endoff_date: date.dateToUser(permit.end_date),
                        oneDivision: divisi == permit.Employee.Job.id_division ? true : false,
                        cuti: true
                    });
                } else {
                    console.error('Error #614: Tidak ada pengajuan dengan id ' + req.params.id);
                    req.flash('message', 'Tidak ada pengajuan dengan id ' + req.params.id);
                    res.redirect('/dayoff/view');
                }
            });
    },
    viewAddAnnualDayoff: function(req, res) {
        res.render('dayoff-add', {
            message: req.flash('message'),
            cuti: true
        });
    },
    addAnnualDayoff: function(req, res) {
        var id_company = res.locals.company;
        model.Employee.findAll({
            include: [{
                association: model.Employee.Job,
                include: [{
                    association: model.Job.Division,
                    where: {
                        id_company: id_company
                    }
                }]
            }]
        }).then(function(employees) {
            var listPromise = [];
            for (var i = 0; i < employees.length; i++) {
                var datetime = new Date();
                var oneyear = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
                var newPromise = model.Permit_history.create({
                    start_date: datetime,
                    end_date: datetime,
                    id_employee: employees[i].id,
                    approval_status: 'disetujui',
                    approval_date: datetime,
                    superior_note: 'Penambahan cuti tahunan',
                    request_date: datetime,
                    expire_date: oneyear,
                    dayoff_type: 'cuti',
                    value: 12
                })
                listPromise.push(newPromise);
            }
            return listPromise;
        }).then(function(listPromise) {
            Promise.all(listPromise).then(function() {
                req.flash('message', 'Cuti tahunan berhasil ditambahkan');
                res.redirect('/dayoff/view');
            });
        }).catch(function(err) {
            req.flash('message', err);
            res.redirect('/dayoff/add');
        })
    }
};

function acceptWithNote(permit, permitid, sup_note, req, res) {
    var datetime = new Date();
    permit.update({
        approval_status: 'disetujui',
        approval_date: datetime,
        superior_note: sup_note
    }).then(function() {
        req.flash('message', 'Berhasil Diterima');
        res.redirect('/dayoff/detail/' + permitid);
    });
};

function acceptWithoutNote(permit, req, res) {
    var datetime = new Date();
    permit.update({
        approval_status: 'disetujui',
        approval_date: datetime
    }).then(function() {
        req.flash('message', 'Berhasil Diterima');
        res.redirect('/dayoff/view');
    });
};

function rejectWithNote(permit, permitid, sup_note, req, res) {
    var datetime = new Date();
    permit.update({
        approval_status: 'ditolak',
        approval_date: datetime,
        superior_note: sup_note
    }).then(function() {
        req.flash('message', 'Berhasil Ditolak');
        res.redirect('/dayoff/detail/' + permitid);
    });
};