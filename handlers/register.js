let model = require('../models');
let Job = model.Job;
let Sequelize = require('sequelize');
var password = require('password-hash-and-salt');
var mailer = require('../utils/mailer');
var randomstring = require("randomstring");

let mailOptions = {
    from: '"Autopersonalia" <autopersonaliapplb3@gmail.com>', // sender address
    subject: 'Selamat bergabung di Autopersonalia' // Subject line
};


module.exports = {
    viewRegister: function(req, res) {
        res.render('register', {
            message: req.flash('message')
        });
    },
    submitRegister: function(req, res) {
        var date_of_birth = new Date(req.body.date_of_birth);
        var join_date = new Date(req.body.join_date);
        var token = randomstring.generate();
        model.Company.create({
            company_name: req.body.company_name,
            address: req.body.company_address,
            description: req.body.company_description
        }).then(function(company) {
            return model.Division.create({
                division_name: req.body.division_name,
                id_company: company.id
            });
        }).then(function(division) {
            return model.Job.create({
                job_title: req.body.job_title,
                supervisor: null,
                id_division: division.id
            });
        }).then(function(job) {
            var date_of_birth = new Date(req.body.date_of_birth);
            var join_date = new Date(req.body.join_date);
            return model.Employee.create({
                employee_no: req.body.employee_no,
                name: req.body.name,
                email: req.body.email,
                // sementara diset default untuk dilakukan reset password
                password: null,
                phone_no: req.body.phone_no,
                telp_no: req.body.telp_no,
                nik: req.body.nik,
                address: req.body.address,
                gender: req.body.gender,
                join_date: join_date,
                place_of_birth: req.body.place_of_birth,
                date_of_birth: date_of_birth,
                is_admin: req.body.is_admin,
                salary: req.body.salary,
                token: token,
                retire_date: null,
                id_job_title: job.id
            });
        }).then(function() {
            mailOptions.to = req.body.email;
            url = process.env.BASE_URL + '/set-password?token=' + token;
            mailOptions.html = "Silakan akses link <a href=\"" + url + "\">ini</a>.";
            mailer.send(mailOptions);
        }).then(function() {
            req.flash('message', 'Registrasi berhasil. Silahkan cek email Anda');
            res.redirect('/login');
        }).catch(Sequelize.UniqueConstraintError, function(err) {
            deleteCompany(req, res);
            if ('nik' in err.fields) {
                field = 'NIK';
            } else if ('email' in err.fields) {
                field = 'Email';
            } else if ('phone' in err.fields) {
                field = 'Nomor HP';
            } else if ('employee_no' in err.fields) {
                field = 'Employee ID';
            } else {
                field = 'Nama Perusahaan'
            }
            console.error('Error #604: ' + field + ' sudah pernah dipakai');
            req.flash('message', field + ' sudah pernah dipakai');
            res.redirect('/register');
        }).catch(function(err) {
            res.redirect('/register');
        });
    }
}

function deleteCompany(req, res) {
    model.Company.destroy({
        where: {
            company_name: req.body.company_name
        }
    })
};