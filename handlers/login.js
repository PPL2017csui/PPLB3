var model = require('../models');
var password = require('password-hash-and-salt');
var Promise = require('bluebird');

module.exports = {
    viewLogin: function(req, res) {
        res.render('login', {
            message: req.flash('message')
        });
    },
    authenticate: function(req, res) {
        model.Employee.findOne({ where: { email: req.body.email } })
            .then(function(user) {
                password(req.body.password).verifyAgainst(user.password, function(error, verified) {
                    if (verified) {
                        req.session.user = user;
                        res.redirect('/dashboard');
                    } else {
                        req.flash('message', 'Password salah');
                        res.redirect('/login');
                    }
                });
            }).catch(function() {
                req.flash('message', 'Email salah');
                res.redirect('/login');
            });
    },

};