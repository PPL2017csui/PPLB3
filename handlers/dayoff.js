var model = require('../models');
var Sequelize = require('sequelize');
var sequelize = new Sequelize('auto-personalia', 'root', '');
var session = require('express-session');
var Promise = require('bluebird');
var date = require('../utils/date');

/**
 * function for day off
 * @module dayoff
 */
module.exports = {
    /**
     * to view the page of create day off
     */
    viewCreateDayOff: function(req, res) {
        getValue(req, res, req.session.user.id).then(function(day_off_limit) {
            if (day_off_limit > 0) {
                res.render('employee-request-day-off', {
                    accessDO: 'true',
                    message: req.flash('message'),
                    cuti: true
                });
            } else {
                res.render('employee-request-day-off', {
                    accessDO: null,
                    cuti: true
                });
            }
        })

    },
    /**
     * to view the page of dayoff status
     */
    viewDayOffStatus: function(req, res) {
        model.Permit_history.findAll({
            where: {
                id_employee: res.locals.user.id
            }
        }).then(function(permit_histories) {
            var reqoff_date;
            var startoff_date;
            var endoff_date;
            for (var i = 0; i < permit_histories.length; i++) {
                permit_histories[i].reqoff_date = date.dateToUser(permit_histories[i].request_date);
                permit_histories[i].startoff_date = date.dateToUser(permit_histories[i].start_date);
                permit_histories[i].endoff_date = date.dateToUser(permit_histories[i].end_date);
            }
            res.render('employee-view-day-off', {
                message: req.flash('message'),
                permit_histories: permit_histories,
                cuti: true
            });
        }).catch(function(err) {
            console.error('Error #612: Tidak ada data cuti');
            req.flash('message', 'Tidak ada data');
            res.render('employee-view-day-off', {
                message: req.flash('message'),
                cuti: true,
            });
        })
    },
    /**
     * to generate json of dayoff dates
     */
    getDates: function(req, res) {
        var id_employee = req.session.user.id;
        model.Permit_history.findAll({
            attributes: ['start_date', 'end_date'],
            where: {
                id_employee: id_employee
            }
        }).then(function(dates) {
            res.json(dates);
        }).catch(function(err) {
            res.send(err);
        });
    },
    /**
     * to save new day off request
     */
    saveCreateDayOff: function(req, res) {
        var start_date_temp = new Date(req.body.day_off_date);
        var end_date_temp = new Date(req.body.end_date);
        var id_employee = req.session.user.id;
        var approval_status = 'menunggu persetujuan';
        var approval_date = null;
        var superior_note = null;
        var request_date = new Date();
        var employee_note = req.body.employee_note;
        var request_type = req.body.request_type;
        var this_year = request_date.getFullYear();
        var expire_date = this_year + 1 + '-12-31';
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds

        getValue(req, res, id_employee).then(function(day_off_limit) {
            var num_of_days = Math.round(Math.abs((end_date_temp.getTime() - start_date_temp.getTime()) / oneDay));
            var value = num_of_days + 1;
            for (i = 0; i < num_of_days; i++) {
                var day = new Date(start_date_temp.getTime() + oneDay * i);
                if (day.getDay() == 0 || day.getDay() == 6) {
                    value = value - 1;
                }
            }
            if (day_off_limit - value > 0) {
                model.Permit_history.findAll({
                    where: {
                        id_employee: id_employee,
                        $or: [{
                            start_date: {
                                $gt: start_date_temp
                            },
                            end_date: {
                                $gte: start_date_temp,
                                $lte: end_date_temp
                            }
                        }, {
                            start_date: {
                                $lt: start_date_temp
                            },
                            end_date: {
                                $gte: start_date_temp,
                                $lte: end_date_temp
                            }
                        }, {
                            start_date: {
                                $gte: start_date_temp
                            },
                            end_date: {
                                $lte: end_date_temp
                            }
                        }, {
                            start_date: {
                                $lte: start_date_temp
                            },
                            end_date: {
                                $gte: end_date_temp
                            }
                        }, ]
                    }
                }).then(function(results) {
                    var permit = results[0];
                    if (permit) {
                        console.error('Error #615: Jadwal pengajuan yang diajukan beririsan dengan pengajuan Anda yang lain');
                        req.flash('message', 'Jadwal pengajuan yang diajukan beririsan dengan pengajuan Anda yang lain');
                        res.redirect('/dayoff/create');
                    } else {
                        model.Permit_history.create({
                            start_date: new Date(start_date_temp.getTime() + oneDay),
                            end_date: new Date(end_date_temp.getTime() + oneDay),
                            id_employee: id_employee,
                            approval_status: approval_status,
                            approval_date: approval_date,
                            superior_note: superior_note,
                            request_date: request_date,
                            employee_note: employee_note,
                            expire_date: expire_date,
                            dayoff_type: request_type,
                            value: -1 * value
                        }).then(function() {
                            req.flash('message', 'Pengajuan cuti dikirim');
                            res.redirect('/dayoff/create');
                        }).catch(function(err) {
                            console.error('Error #616: Data cuti yang dimasukkan tidak benar');
                            req.flash('message', 'Pastikan data yang Anda masukkan benar');
                            res.redirect('/dayoff/create');
                        });
                    }
                });
            } else {
                if (isNaN(num_of_days)) {
                    console.error('Error #616: Data cuti yang dimasukkan tidak benar');
                    req.flash('message', 'Pastikan data yang Anda masukkan benar');
                    res.redirect('/dayoff/create');
                } else {
                    req.flash('message', 'Maaf, jatah cuti Anda tidak mencukupi');
                    res.redirect('/dayoff/create');
                }
            }
        })
    }
};
/**
 * to get the employees' value of dayoff
 */
function getValue(req, res, id_employee) {
    return model.Permit_history.sum('value', {
        where: {
            id_employee: id_employee,
            approval_status: 'disetujui',
            expire_date: {
                $gt: new Date()
            }
        }
    }).then(function(sum) {
        return sum;
    })
};